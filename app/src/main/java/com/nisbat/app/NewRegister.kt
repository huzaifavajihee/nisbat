package com.nisbat.app

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.LinearGradient
import android.graphics.Matrix
import android.graphics.Shader
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NewRegister : AppCompatActivity() {

    var booleanMaleChecked: Boolean = false
    var booleanfemaleChecked: Boolean = false
    var strprofilefor1: String = ""
    var strlivingin: String = ""
    var strcountrycode: String = "00"
    var booleanpasswordvisible: Boolean = false
    var strfirstname: String = ""
    var strlastname: String = ""
    var strdateofbirth: String = ""
    var stremail: String = ""
    var strmobilenumber: String = ""
    var stritscardnumber: String = ""
    var strpassword: String = ""
    var countriees = ArrayList<String>()
    var booleancountryselected = false

    var FCMToken=""

    lateinit var ITS_Image_File: File

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject

    val stringArrayCountry = ArrayList<String>()
    var jsonArrayCountry = JSONArray()
    val strarrayCCode = ArrayList<String>()
    var jsonArrayCCode = JSONArray()


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_register_2)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = resources.getColor(R.color.color_white)
        }

        if (!checkPermissionForWriteExtertalStorage()) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
        }

        sharedPrefencesLogin =
            this@NewRegister.getSharedPreferences("pref_login_user", 0)
//        jsonUserData= JSONObject(sharedPrefencesLogin.getString("User_Data",""))

        // Declaring Objects
        val tvhometogether = findViewById<TextView>(R.id.tvregistertogether)
        val lineardateofbirth = findViewById<LinearLayout>(R.id.linearregisterdob)
        val frameDOB = findViewById<FrameLayout>(R.id.frameregisterdatepicker)
        val tvDOB = findViewById<TextView>(R.id.tvregisterdateofbirth)
        val btndobcancel = findViewById<Button>(R.id.btnregisterdatepickercancel)
        val btndobdone = findViewById<Button>(R.id.btnregisterdatepickerok)
        val ivmalecheckboxout = findViewById<ImageView>(R.id.ivregistermaleoutside)
        val ivfemalecheckboxout = findViewById<ImageView>(R.id.ivregisterfemaleoutside)
        val ivmalecheckinside = findViewById<ImageView>(R.id.ivregistermaleinside)
        val ivfemalecheckinside = findViewById<ImageView>(R.id.ivregisterfemaleinside)
        val datepicker = findViewById<DatePicker>(R.id.datepickerregister)
        val etfirstname = findViewById<EditText>(R.id.etregisterfirstname)
        val etlastname = findViewById<EditText>(R.id.etregisterlastname)
        val etemail = findViewById<EditText>(R.id.etregisteremail)
        val etmobile = findViewById<EditText>(R.id.etregistercontactnumber)
        val etITSnumber = findViewById<EditText>(R.id.etregisteritscardnumber)
        val ivpasswordvisible = findViewById<ImageView>(R.id.ivregisterpasswordvisible)
        val etpassword = findViewById<EditText>(R.id.etregisterpassword)
        val frameuploadpic = findViewById<FrameLayout>(R.id.frameuploadITScardphoto)
        val btnthakyouok = findViewById<Button>(R.id.btnthankyouscreenok)
        val framethankyou = findViewById<FrameLayout>(R.id.frameregisterthankyou)
//        val spinnerlivingin = findViewById<Spinner>(R.id.spinnerregisterlivingin)
        val linearmale = findViewById<LinearLayout>(R.id.linearregistermale)
        val linearfemale = findViewById<LinearLayout>(R.id.linearregisterfemale)
        val btnsignup = findViewById<Button>(R.id.btnsignup)
        var spinnercountrycode =
            findViewById<Spinner>(R.id.spinnerregistercountrycode)
        ITS_Image_File = File("")
        val ACLivingIn = findViewById<AutoCompleteTextView>(R.id.AutoCompleteRegisterLivingin)

        //test value
//        etfirstname.setText("test")
//        etlastname.setText("test")
//        etemail.setText("demo@gmail.com")
//        etpassword.setText("Demo123")
//        etmobile.setText("12345678")
//        etITSnumber.setText("77777777")
//        tvDOB.setText("01-01-2000")

        GetFCMToken()

        //jsonArrayCountry = JSONArray(loadCountryJSONFromAsset())
        val db = CSC_Database_Helper(this)
        jsonArrayCountry = db.GetCountry()!!
//        var locale = resources.configuration.locale.displayCountry
        val tm = this.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        val locale = tm.getNetworkCountryIso().toUpperCase()
//        var locale=Locale.getDefault().getISO3Country()
        var currentcountrypos = 0
        for (i in 0 until jsonArrayCountry.length()) {
            var Cname = jsonArrayCountry.getJSONObject(i).getString("name")
            countriees.add(Cname)
            try {

                if (jsonArrayCountry.getJSONObject(i).getString(
                        "iso2"
                    ).equals(locale)
                ) {
                    currentcountrypos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
//        if (spinnerlivingin != null) {
//            val adapter =
//                Adapter_Spinner_CSC(
//                    (this@NewRegister),
//                    jsonArrayCountry,
//                    null
//                )
//            spinnerlivingin.adapter = adapter
//            spinnerlivingin.setSelection(currentcountrypos)
//        }
        val adapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, countriees)
        ACLivingIn.setAdapter(adapter)
        if (currentcountrypos >= 0) {
            ACLivingIn.setText(countriees.get(currentcountrypos))
            strlivingin = jsonArrayCountry.getJSONObject(currentcountrypos).getString("id")
            booleancountryselected = true
        }
//        if (ACLivingIn!=null) {
//            val adapter =
//               Adapter_Spinner_CSC(this,  jsonArrayCountry,null)
//            ACLivingIn.setAdapter(adapter)
//            if (currentcountrypos>=0)
//            {
//                ACLivingIn.setText(jsonArrayCountry.getJSONObject(currentcountrypos).getString("name"))
//            }
//        }


        var currentcountrycodeposition: Int = 0
        for (i in 0 until jsonArrayCountry.length()) {
            var jsonobj = jsonArrayCountry.getJSONObject(i)
            var code = jsonobj.getString("phonecode")
            var countryname = jsonobj.getString("iso2")
            var phonecode = code.split("-")

            var finalcode =
                "+" + phonecode.get(0).replace("+", "") + " " + countryname
            if (!strarrayCCode.contains(finalcode)) {
                if (!finalcode.equals("+")) {
                    strarrayCCode.add(finalcode)
                }
            }
            if (countryname.equals(locale)) {
                currentcountrycodeposition = i
            }
        }
        if (spinnercountrycode != null) {

            val adapter =
                Adapter_CountryCode(
                    this@NewRegister,
                    strarrayCCode,
                    null
                )
            spinnercountrycode.adapter = adapter
            spinnercountrycode.setSelection(currentcountrycodeposition)
        }


        //Making Multicolor Text
        val paint = tvhometogether.paint
        val shader = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            LinearGradient(
                0f, 0f, 0f, tvhometogether.textSize, getColor(R.color.color_orange), getColor(
                    R.color.color_red
                ), Shader.TileMode.CLAMP
            )
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        tvhometogether.paint.shader = shader

        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if (isConnected) {
//            GetCountrylist()
//            getCountryCode()
        } else {
            Toast.makeText(this, "You Are Not Connected To Network", Toast.LENGTH_SHORT).show()
        }

        //set values in spinner
        val spinnerprofilefor = findViewById<Spinner>(R.id.spinnerregisterprofilefor1)
        if (spinnerprofilefor != null) {
            val adapterSpinner =
                Adapter_Spinner(
                    this@NewRegister,
                    resources.getStringArray(R.array.ProfileFor).toMutableList(),
                    null
                )
            spinnerprofilefor.adapter = adapterSpinner
        }


        //spinner item select
        spinnerprofilefor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if(position!=0) {
                    strprofilefor1 =
                        resources.getStringArray(R.array.ProfileFor).get(position).toString()
                }else{
                    strprofilefor1=""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        ACLivingIn.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var countrypos = 0
                if (countriees.contains(Cname)) {
                    countrypos = countriees.indexOf(Cname)
                }
                strlivingin = jsonArrayCountry.getJSONObject(countrypos).getString("id")
                booleancountryselected = true
            }

        })

        var aclivingincount = 0
        ACLivingIn.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                aclivingincount = count
                if (count > 0) {
                    booleancountryselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                aclivingincount = s?.length!!
            }
        })

        ACLivingIn.setOnDismissListener {

            if (aclivingincount > 0) {
                if (!booleancountryselected) {
                    ACLivingIn.requestFocus()
                    ACLivingIn.showDropDown()
                }
            }
        }
//        spinnerlivingin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//
//                strlivingin = jsonArrayCountry.getJSONObject(position).getString("id")
//
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("Not yet implemented")
//            }
//
//        }

        spinnercountrycode.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var code: List<String> = strarrayCCode.get(position).split(" ")
                var strcountrycode1 = strarrayCCode.get(position).replace("+", "").split(" ")
                strcountrycode = strcountrycode1[0]
                val textView = view?.findViewById<TextView>(R.id.tvspinnercountrycode)
                if (textView != null) {
                    textView.text = "+" + strcountrycode
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }


        //ok button click
        btnthakyouok.setOnClickListener {
            framethankyou.visibility = View.INVISIBLE
            //Starting new Activity
            val intent = Intent(applicationContext, SignIn::class.java)
            startActivity(intent)
        }

        //open gallery to select image of its
        frameuploadpic.setOnClickListener {

            val intent: Intent = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }


        //Signup Button Clicked
        btnsignup.setOnClickListener {

            strfirstname = etfirstname.text.toString()
            strlastname = etlastname.text.toString()
            strdateofbirth = tvDOB.text.toString()
            stremail = etemail.text.toString()
            strmobilenumber = etmobile.text.toString()
            stritscardnumber = etITSnumber.text.toString()
            strpassword = etpassword.text.toString()

            if (strprofilefor1.equals("")) {
                Toast.makeText(this, "Please select \"Profile for\"", Toast.LENGTH_SHORT).show()
            } else {
                if (strfirstname.equals("")) {
                    Toast.makeText(this, "Firstname should not be blank", Toast.LENGTH_SHORT).show()
                } else {
                    if (strlastname.equals("")) {
                        Toast.makeText(this, "Lastname should not be blank", Toast.LENGTH_SHORT).show()
                    } else {
                        if (!booleanMaleChecked && !booleanfemaleChecked) {
                            Toast.makeText(this, "Please select \"Gender\"", Toast.LENGTH_SHORT)
                                .show()
                        } else {
                            if (strdateofbirth.equals("")) {
                                Toast.makeText(
                                    this,
                                    "Please Select \"Date of birth\"",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                if (stremail.equals("")) {
                                    Toast.makeText(
                                        this,
                                        "Email should not be blank",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else {
                                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(stremail)
                                            .matches()
                                    ) {
                                        Toast.makeText(
                                            this,
                                            "Please Enter valid email address",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    } else {
                                        if (strpassword.equals("")) {
                                            Toast.makeText(
                                                this,
                                                "Password should not be blank",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        } else {
                                            if (strpassword.length < 6 && !CheckUppercase(
                                                    strpassword
                                                )
                                            ) {
                                                Toast.makeText(
                                                    this,
                                                    "Password must have 6 characters, one letter & number",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            } else {
                                                if (strmobilenumber.equals("")) {
                                                    Toast.makeText(
                                                        this,
                                                        "Phone number should not be blank",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                } else {
                                                    if (strlivingin.equals("")) {
                                                        Toast.makeText(
                                                            this,
                                                            "Please select \"Living in\"",
                                                            Toast.LENGTH_SHORT
                                                        ).show()
                                                    } else {
                                                        if (stritscardnumber.equals("")) {
                                                            Toast.makeText(
                                                                this,
                                                                "ITS number should not be blank",
                                                                Toast.LENGTH_SHORT
                                                            ).show()
                                                        } else {
                                                            if (stritscardnumber.length < 8) {
                                                                Toast.makeText(
                                                                    this,
                                                                    "ITS number must have 8 characters",
                                                                    Toast.LENGTH_SHORT
                                                                ).show()
                                                            } else {
                                                                if (ITS_Image_File.isFile) {
                                                                    SignUp()
                                                                } else {
                                                                    Toast.makeText(
                                                                        this,
                                                                        "Please upload ITS card photo",
                                                                        Toast.LENGTH_SHORT
                                                                    ).show()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        //linear male female clicked
        linearmale.setOnClickListener {
            ivmalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivmalecheckinside.visibility = View.VISIBLE
            ivfemalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivfemalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = true
            booleanfemaleChecked = false
        }
        linearfemale.setOnClickListener {
            ivfemalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivfemalecheckinside.visibility = View.VISIBLE
            ivmalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivmalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = false
            booleanfemaleChecked = true
        }

        //RadioButton Male & Female Clicked
        ivmalecheckboxout.setOnClickListener {
            ivmalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivmalecheckinside.visibility = View.VISIBLE
            ivfemalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivfemalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = true
            booleanfemaleChecked = false
        }
        ivfemalecheckboxout.setOnClickListener {
            ivfemalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivfemalecheckinside.visibility = View.VISIBLE
            ivmalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivmalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = false
            booleanfemaleChecked = true
        }

        //password visible/invisible
        ivpasswordvisible.setOnClickListener {

            if (!booleanpasswordvisible) {
                ivpasswordvisible.setImageResource(R.drawable.icon_eye_white)
                etpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                booleanpasswordvisible = true

            } else {
                ivpasswordvisible.setImageResource(R.drawable.icon_eye_gray)
                etpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                booleanpasswordvisible = false
            }
        }

        //Opening datepicker on lick
        lineardateofbirth.setOnClickListener {
            frameDOB.visibility = View.VISIBLE
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }

        //set datepicker values to textview
        btndobcancel.setOnClickListener {
            frameDOB.visibility = View.INVISIBLE
        }
        btndobdone.setOnClickListener {
            val day: Int = datepicker.getDayOfMonth()
            val month: Int = datepicker.getMonth()
            val year: Int = datepicker.getYear()
            val calendar: Calendar = Calendar.getInstance()
            calendar.set(year, month, day)

            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val formatedDate: String = sdf.format(calendar.getTime())

            if (getAge(year, month, day) >= 18) {
                tvDOB.setText(formatedDate)
                strdateofbirth = formatedDate
                frameDOB.visibility = View.INVISIBLE
            } else {
                Toast.makeText(this, "Age must be 18+", Toast.LENGTH_SHORT).show()
                strdateofbirth = ""
                tvDOB.setText(strdateofbirth)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 0) {

            val ivitscard = findViewById<ImageView>(R.id.ivregisteritscard)
            val ivitscloudplaceholder = findViewById<ImageView>(R.id.ivuploadits)
            val tvuploadits = findViewById<TextView>(R.id.tvuploadits)

            if (data != null) {
                val contentURI = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor? = this@NewRegister.getContentResolver().query(
                    contentURI!!,
                    filePathColumn, null, null, null
                )
                cursor?.moveToFirst()
                val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
                var picturePath = cursor?.getString(columnIndex!!)
                cursor?.close()
                Log.d("picturePath", picturePath!!)

                // Pathuri=picturePath;
                try {
                    var bitmap = MediaStore.Images.Media.getBitmap(
                        this@NewRegister.getContentResolver(),
                        contentURI
                    )
                    var newBitmap = modifyOrientation(bitmap, picturePath)
                    //var encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
                    val bytes = ByteArrayOutputStream()

                    val file = File(this.cacheDir, "image.jpg")
                    file.createNewFile()
//                    newBitmap!!.compress(Bitmap.CompressFormat.PNG, 1, bytes)
                    val bitmapdata = bytes.toByteArray()

                    val fos = FileOutputStream(file)
                    fos.write(bitmapdata)
                    ITS_Image_File=File("")
                    ITS_Image_File=file
                    fos.flush()
                    fos.close()

//                    if (newBitmap != null) {
//                        picturePath = saveImage(newBitmap)
//                    }
                    val uriImage = data.data
                    ivitscard.setImageURI(uriImage)

                } catch (e: FileNotFoundException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                } catch (e: IOException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }

            ivitscard.visibility = View.VISIBLE
            ivitscloudplaceholder.visibility = View.GONE
            tvuploadits.visibility = View.GONE

        }
    }

    fun saveImage(myBitmap: Bitmap): String? {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.PNG, 50, bytes)
        val wallpaperDirectory = File(
            Environment.getExternalStorageDirectory()
                .toString() + "/Nisbat_ITSPhoto"
        )
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory, Calendar.getInstance()
                    .timeInMillis.toString() + ".PNG"
            )
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this@NewRegister,
                arrayOf(f.path),
                arrayOf("image/jpeg"),
                null
            )
            fo.close()
            ITS_Image_File = f
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }

    private fun CheckUppercase(str: String): Boolean {
        var ch: Char
        var capitalFlag = false
        for (i in 0 until str.length) {
            ch = str[i]
            if (Character.isUpperCase(ch)) {
                capitalFlag = true
            }
            if (capitalFlag) return true
        }
        return false
    }

    @Throws(IOException::class)
    fun modifyOrientation(bitmap: Bitmap, image_absolute_path: String?): Bitmap? {
        val ei = ExifInterface(image_absolute_path!!)
        val orientation: Int =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate(bitmap, 270f)
//            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> flip(bitmap, true, false)
//            ExifInterface.ORIENTATION_FLIP_VERTICAL -> flip(bitmap, false, true)
            else -> bitmap
        }
    }

    fun rotate(bitmap: Bitmap, degrees: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

//    fun flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap? {
//        val matrix = Matrix()
//        matrix.preScale(if (horizontal) -1 else 1, if (vertical) -1 else 1)
//        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
//    }

    fun SignUp() {
        val progressDialog = ProgressDialog(this@NewRegister)
//        progressDialog.setTitle("SignUp")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()
        requestParams.add("profile_for", strprofilefor1)
        requestParams.add("first_name", strfirstname)
        requestParams.add("last_name", strlastname)
        requestParams.add("dob", strdateofbirth)
        requestParams.add("email", stremail)
        requestParams.add("password", strpassword)
        requestParams.add("mobile_no", strmobilenumber)
        requestParams.add("phone_code", strcountrycode)
        requestParams.add("its_card_number", stritscardnumber)
        requestParams.put("its_card_image", ITS_Image_File)
        requestParams.put("living_in", strlivingin)
        requestParams.add("fcm_token", FCMToken)
        if (booleanMaleChecked) {
            requestParams.add("gender", "male")
        } else {
            requestParams.add("gender", "female")
        }
        val asyncHttpClient = AsyncHttpClient()
        asyncHttpClient.connectTimeout = 20000
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "register",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        println(response)
                        var status = response.get("status")
                        if (status is Int) {
                            if (status == 200) {
                                if (response.getJSONObject("data").equals(null)) {
                                    Toast.makeText(
                                        this@NewRegister,
                                        response.getString("message"),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else {
                                    val framethankyou =
                                        findViewById<FrameLayout>(R.id.frameregisterthankyou)
                                    framethankyou.visibility = View.VISIBLE
                                }
                                progressDialog.dismiss()
                            } else if (status == 401) {
                                progressDialog.dismiss()
                                Toast.makeText(
                                    applicationContext,
                                    response.getString("message"),
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                progressDialog.dismiss()
                                Toast.makeText(
                                    applicationContext,
                                    response.getString("message"),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            progressDialog.dismiss()
                            println("status code is missing")
                            val obj = response.getJSONObject("status")
                            Toast.makeText(
                                applicationContext,
                                obj.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@NewRegister, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@NewRegister, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@NewRegister, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    fun GetCountrylist() {

        val requestParams = RequestParams()

        val asyncHttpClient = AsyncHttpClient()
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "get-country",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            jsonArrayCountry = response.getJSONArray("data")
                            for (i in 0 until jsonArrayCountry.length()) {
                                try {
                                    stringArrayCountry.add(
                                        jsonArrayCountry.getJSONObject(i).getString(
                                            "name"
                                        )
                                    )
                                } catch (e: JSONException) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace()
                                }
                            }
//                            if (spinnerlivingin != null) {
//                                val adapter =
//                                    Adapter_Spinner(
//                                        (this@NewRegister),
//                                        stringArrayCountry,
//                                        null
//                                    )
//                                spinnerlivingin.adapter = adapter
//                            }

                        } else if (status == 401) {

                            Toast.makeText(
                                (this@NewRegister),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                }
            })
    }

    fun getCountryCode() {
        val progressDialog = ProgressDialog(this@NewRegister)
//        progressDialog.setTitle("Update")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()

        val asyncHttpClient = AsyncHttpClient()
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "get-country-phone-code",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            strarrayCCode.clear()
                            jsonArrayCCode = response.getJSONArray("data")
                            var locale = resources.configuration.locale.displayCountry
                            var currentcountrycodeposition: Int = 0
                            for (i in 0 until jsonArrayCCode.length()) {
                                var jsonobj = jsonArrayCCode.getJSONObject(i)
                                var code = jsonobj.getString("phonecode")
                                var countryname = jsonobj.getString("name")
                                var phonecode = code.split("-")

                                var finalcode =
                                    "+" + phonecode.get(0).replace("+", "") + "   " + countryname
                                if (!strarrayCCode.contains(finalcode)) {
                                    if (!finalcode.equals("+")) {
                                        strarrayCCode.add(finalcode)
                                    }
                                }
                                if (countryname.equals(locale)) {
                                    currentcountrycodeposition = i
                                }
                            }
//                            Log.d("TAG", "onSuccess: ")
                            var spinnercountrycode =
                                findViewById<Spinner>(R.id.spinnerregistercountrycode)
                            if (spinnercountrycode != null) {

                                val adapterGender =
                                    Adapter_CountryCode(
                                        this@NewRegister,
                                        strarrayCCode,
                                        null
                                    )
                                val adapter = ArrayAdapter(
                                    this@NewRegister,
                                    R.layout.item_spinner_countrycode,
                                    strarrayCCode
                                )
                                spinnercountrycode.adapter = adapterGender
                                spinnercountrycode.setSelection(currentcountrycodeposition)
                            }
                            progressDialog.dismiss()


                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@NewRegister),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@NewRegister),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Log.d("TAG", "onFailure: " + errorResponse)
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Log.d("TAG", "onFailure: " + errorResponse)
                }
            })
    }

    fun getAge(_year: Int, _month: Int, _day: Int): Int {
        val cal = GregorianCalendar()
        val y: Int
        val m: Int
        val d: Int
        var a: Int
        y = cal[Calendar.YEAR]
        m = cal[Calendar.MONTH]
        d = cal[Calendar.DAY_OF_MONTH]
        cal[_year, _month] = _day
        a = y - cal[Calendar.YEAR]
        if (m < cal[Calendar.MONTH]
            || m == cal[Calendar.MONTH] && d < cal[Calendar.DAY_OF_MONTH]
        ) {
            --a
        }
        require(a >= 0) { "Age < 0" }
        return a
    }

//    fun LoadJSONfromAssets():String
//    {
//        var json:String=""
//        var IS:InputStream=this@NewRegister.assets.open("country.json")
//        var size=IS.available()
//        val buffer = ByteArray(size)
//        IS.read(buffer)
//        IS.close()
//        json= buffer.toString()
//
//        return json
//    }

    fun loadCountryJSONFromAsset(): String? {
        var json: String? = null
        json = try {
            val IS: InputStream = this.assets.open("country.json")
            val size = IS.available()
            val buffer = ByteArray(size)
            IS.read(buffer)
            IS.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun loadStateJSONFromAsset(): String? {
        var json: String? = null
        json = try {
            val IS: InputStream = this.assets.open("state.json")
            val size = IS.available()
            val buffer = ByteArray(size)
            IS.read(buffer)
            IS.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun loadCityJSONFromAsset(): String? {
        var json: String? = null
        json = try {
            val IS: InputStream = this.assets.open("city2.json")
            val size = IS.available()
            val buffer = ByteArray(size)
            IS.read(buffer)
            IS.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun checkPermissionForWriteExtertalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result: Int = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    fun GetFCMToken()
    {
        FirebaseApp.initializeApp(this)
        FCMToken= FirebaseInstanceId.getInstance().getToken().toString()
        Log.d("TAG", "GetFCMToken: " + FCMToken)
    }


//    private fun closeKeyBoard() {
//        val view = this.currentFocus
//        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        imm.hideSoftInputFromWindow(view.windowToken, 0)
//
//    }

}
