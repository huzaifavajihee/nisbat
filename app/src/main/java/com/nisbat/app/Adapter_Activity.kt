package com.nisbat.app

import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity

import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class Adapter_Activity(
    val context: Context,
    val datasource: JSONArray,
    val fragment_activity: Fragment_Activity,
    val isAccepted: Boolean = false,
    val isReceived: Boolean = false,
    val isSent: Boolean = false,
    val isDeleted: Boolean = false
) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        //TODO("Not yet implemented")
        return datasource.length()

    }

    override fun getItem(p0: Int): Any {
        //TODO("Not yet implemented")
        return datasource.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        //TODO("Not yet implemented")
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))

        if (isAccepted) {
            val view = inflater.inflate(R.layout.item_list_activity_accepted, p2, false)
            val tvName = view.findViewById(R.id.tvactivityaccepteditemname) as TextView
            val tvSubTitle: TextView =
                view.findViewById(R.id.tvactivityaccpteditemageheightbusiness)
            val tvaddress = view.findViewById<TextView>(R.id.tvactivityaccepteditemaddress)
            val ivimage = view.findViewById<ImageView>(R.id.ivaccepteditem)
            val ivmenu = view.findViewById<ImageView>(R.id.ivactivityacceptedmenu)
            val ibtnwhatsapp = view.findViewById<ImageButton>(R.id.ibtnactivityacceptedwhatsapp)
            val ibtnEmail = view.findViewById<ImageButton>(R.id.ibtnactivityaccepteditememail)
            val ibtnCall = view.findViewById<ImageButton>(R.id.ibtnactivityaccepteditemtelephone)

            val obj = datasource.getJSONObject(p0)

            tvName.text = obj.getString("first_name") + " " + obj.getString("last_name")

            val strAge = obj.getString("dob").split("-")
            var finalage = getAge(strAge[0].toInt(), strAge[1].toInt(), strAge[2].toInt())
            val arrStrHeight = obj.getString("height").split("-")
            val strHeight = arrStrHeight[0].replace(" ", "").replace("ft", "'").replace("in", "\"")
            val strProfession = obj.getString("qualification")
            tvSubTitle.text =
                finalage.toString() + " years" + ", " + strHeight + " | " + strProfession

            var address = ""
            if (!obj.getString("family_location").isNullOrEmpty()) {
                address = address + obj.getString("family_location")
            }
            if (!obj.getString("city").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("city")
                } else {
                    address = address + obj.getString("city")
                }
            }
            if (!obj.getString("state").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("state")
                } else {
                    address = address + obj.getString("state")
                }
            }
            if (!obj.getString("country").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("country")
                } else {
                    address = address + obj.getString("country")
                }
            }
            tvaddress.setText(address)

            var imageurl = ""
            if (!obj.getString("profile_picture").isNullOrEmpty()) {
                imageurl = obj.getString("profile_picture")
            } else {
                imageurl = "abc.jpg"
            }
            if (imageurl.equals("abc.jpg")) {
                if (User_Data.getString("gender").equals("male")) {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_female)
                    Picasso.get()
                        .load(R.drawable.placeholder_female)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_female))
                        .error(context.resources.getDrawable(R.drawable.placeholder_female))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_male)
                    Picasso.get()
                        .load(R.drawable.placeholder_male)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            } else {
                imageurl=imageurl.replace("http:","https:")
                if (User_Data.getString("gender").equals("male")) {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_male))
                        .error(context.resources.getDrawable(R.drawable.placeholder_male))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            }

            ivmenu.setOnClickListener {
                val popup = PopupMenu(context, ivmenu)
                val inflater = popup.menuInflater
                inflater.inflate(R.menu.popupmenu, popup.menu)
                popup.show()

                popup.setOnMenuItemClickListener {
                    if (it.itemId.equals(R.id.ReportMenuItem)) {
                        ReportUser(obj.getString("id"))
                    }
                    return@setOnMenuItemClickListener true
                }
            }

//            ibtnwhatsapp.setOnClickListener {
//                if (!obj.getString("whats_app_no").isNullOrEmpty()) {
//                    fragment_activity.ItemWhatsappClicked(
//                        obj.getString("phone_code"),
//                        obj.getString("whats_app_no")
//                    )
//                } else {
//                    Toast.makeText(context, "Whatsapp number not found !", Toast.LENGTH_SHORT)
//                        .show()
//                }
//            }
//
//            ibtnEmail.setOnClickListener {
//                val recipient = obj.getString("email")
//                fragment_activity.sendEmail(recipient)
//            }
//
//            ibtnCall.setOnClickListener {
//                val number = "+" + obj.getString("phone_code") + obj.getString("mobile_no")
//                fragment_activity.Call(number)
//            }

            return view
        } else if (isReceived) {
            val view = inflater.inflate(R.layout.item_list_activity_received, p2, false)
            val tvName = view.findViewById(R.id.tvactivityreceivednameage) as TextView
            val tvProfession: TextView = view.findViewById(R.id.tvactivityreceivedprofession)
            val tvaddress = view.findViewById<TextView>(R.id.tvactivityreceivedaddress)
            val ivimage = view.findViewById<ImageView>(R.id.ivreceiveditem)
            val btnaccept = view.findViewById<Button>(R.id.btnreceivedlistaccept)
            val btndecline = view.findViewById<Button>(R.id.btnreceivedlistdecline)
            val ivmenu = view.findViewById<ImageView>(R.id.ibtnactivityreceivedmenu)
            val tvimagecount = view.findViewById<TextView>(R.id.tvactivityreceivedimagecount)

            val obj = datasource.getJSONObject(p0)
            val strAge = obj.getString("dob").split("-")
            var finalage = getAge(strAge[0].toInt(), strAge[1].toInt(), strAge[2].toInt())

            tvName.text =
                obj.getString("first_name") + " " + obj.getString("last_name") + ", " + finalage
            tvProfession.setText(obj.getString("qualification"))
            var address = ""
            if (!obj.getString("family_location").isNullOrEmpty()) {
                address = address + obj.getString("family_location")
            }
            if (!obj.getString("image_count").isNullOrEmpty()) {
                tvimagecount.setText(obj.getString("image_count"))
            } else {
                tvimagecount.setText("0")
            }
            if (!obj.getString("city").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("city")
                } else {
                    address = address + obj.getString("city")
                }
            }
            if (!obj.getString("state").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("state")
                } else {
                    address = address + obj.getString("state")
                }
            }
            if (!obj.getString("country").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("country")
                } else {
                    address = address + obj.getString("country")
                }
            }
            tvaddress.setText(address)
            var imageurl = ""
            if (!obj.getString("profile_picture").isNullOrEmpty()) {
                imageurl = obj.getString("profile_picture")
            } else {
                imageurl = "abc.jpg"
            }
            imageurl=imageurl.replace("http:","https:")
            if (imageurl.equals("abc.jpg")) {
                if (User_Data.getString("gender").equals("male")) {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_female)
                    Picasso.get()
                        .load(R.drawable.placeholder_female)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_male)
                    Picasso.get()
                        .load(R.drawable.placeholder_male)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            } else {
                if (User_Data.getString("gender").equals("male")) {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_female))
                        .error(context.resources.getDrawable(R.drawable.placeholder_female))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_male))
                        .error(context.resources.getDrawable(R.drawable.placeholder_male))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            }

            btnaccept.setOnClickListener {
                var opp_its_number = datasource.getJSONObject(p0).getString("its_card_number")
                var own_its_number = User_Data.getString("its_card_number")
                SendResponce(
                    opp_its_number,
                    own_its_number,
                    "accepted",
                    "accept_request",
                    "Received"
                )

            }
            btndecline.setOnClickListener {
                var opp_its_number = datasource.getJSONObject(p0).getString("its_card_number")
                var own_its_number = User_Data.getString("its_card_number")
                SendResponce(opp_its_number, own_its_number, "rejected", "reject", "Received")
            }

            ivmenu.setOnClickListener {
                val popup = PopupMenu(context, ivmenu)
                val inflater = popup.menuInflater
                inflater.inflate(R.menu.popupmenu, popup.menu)
                popup.show()

                popup.setOnMenuItemClickListener {
                    if (it.itemId.equals(R.id.ReportMenuItem)) {
                        ReportUser(obj.getString("id"))
                    }
                    return@setOnMenuItemClickListener true
                }
            }

            return view
        } else if (isSent) {
            val view = inflater.inflate(R.layout.item_list_activity_sent, p2, false)
            val tvName = view.findViewById(R.id.tvactivitysentitemname) as TextView
            val tvSubTitle: TextView =
                view.findViewById(R.id.tvactivitysentitemageheightbusiness)
            val tvaddress = view.findViewById<TextView>(R.id.tvactivitysentitemaddress)
            val ivimage = view.findViewById<ImageView>(R.id.ivsentitem)
            val ivmenu = view.findViewById<ImageView>(R.id.ivactivitysentmenu)
            val ibtnwhatsapp = view.findViewById<ImageButton>(R.id.ibtnactivitysentwhatsapp)
            val ibtnEmail = view.findViewById<ImageButton>(R.id.ibtnactivitysentitememail)
            val ibtnCall = view.findViewById<ImageButton>(R.id.ibtnactivitysentitemtelephone)

            val obj = datasource.getJSONObject(p0)
            tvName.text = obj.getString("first_name") + " " + obj.getString("last_name")

            val strAge = obj.getString("dob").split("-")
            var finalage = getAge(strAge[0].toInt(), strAge[1].toInt(), strAge[2].toInt())
            val arrStrHeight = obj.getString("height").split("-")
            val strHeight = arrStrHeight[0].replace(" ", "").replace("ft", "'").replace("in", "\"")
            val strProfession = obj.getString("qualification")
            tvSubTitle.text =
                finalage.toString() + " years" + ", " + strHeight + " | " + strProfession
            var address = ""
            if (!obj.getString("family_location").isNullOrEmpty()) {
                address = address + obj.getString("family_location")
            }
            if (!obj.getString("city").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("city")
                } else {
                    address = address + obj.getString("city")
                }
            }
            if (!obj.getString("state").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("state")
                } else {
                    address = address + obj.getString("state")
                }
            }
            if (!obj.getString("country").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("country")
                } else {
                    address = address + obj.getString("country")
                }
            }
            tvaddress.setText(address)

            var imageurl = ""
            if (!obj.getString("profile_picture").isNullOrEmpty()) {
                imageurl = obj.getString("profile_picture")
            } else {
                imageurl = "abc.jpg"
            }
            imageurl=imageurl.replace("http:","https:")
            if (imageurl.equals("abc.jpg")) {
                if (User_Data.getString("gender").equals("male")) {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_female)
                    Picasso.get()
                        .load(R.drawable.placeholder_female)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_male)
                    Picasso.get()
                        .load(R.drawable.placeholder_male)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            } else {
                if (User_Data.getString("gender").equals("male")) {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_female))
                        .error(context.resources.getDrawable(R.drawable.placeholder_female))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_male))
                        .error(context.resources.getDrawable(R.drawable.placeholder_male))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            }

            ivmenu.setOnClickListener {
                val popup = PopupMenu(context, ivmenu)
                val inflater = popup.menuInflater
                inflater.inflate(R.menu.popupmenu, popup.menu)
                popup.show()

                popup.setOnMenuItemClickListener {
                    if (it.itemId.equals(R.id.ReportMenuItem)) {
                        ReportUser(obj.getString("id"))
                    }
                    return@setOnMenuItemClickListener true
                }
            }

//            ibtnwhatsapp.setOnClickListener {
//                if (!obj.getString("whats_app_no").isNullOrEmpty()) {
//                    fragment_activity.ItemWhatsappClicked(
//                        obj.getString("phone_code"),
//                        obj.getString("whats_app_no")
//                    )
//                } else {
//                    Toast.makeText(context, "Whatsapp number not found !", Toast.LENGTH_SHORT)
//                        .show()
//                }
//            }
//
//            ibtnEmail.setOnClickListener {
//                val recipient = obj.getString("email")
//                fragment_activity.sendEmail(recipient)
//            }
//
//            ibtnCall.setOnClickListener {
//                val number = "+" + obj.getString("phone_code") + obj.getString("mobile_no")
//                fragment_activity.Call(number)
//            }

            return view
        } else {
            val view = inflater.inflate(R.layout.item_list_activity_deleted, p2, false)
            val tvName = view.findViewById(R.id.tvactivitydeleteditemname) as TextView
            val tvSubTitle: TextView =
                view.findViewById(R.id.tvactivitydeleteditemageheightbusiness)
            val tvaddress = view.findViewById<TextView>(R.id.tvactivitydeleteditemaddress)
            val ivimage = view.findViewById<ImageView>(R.id.ivdeleteitem)
            val ivmenu = view.findViewById<ImageView>(R.id.ivactivitydeletedmenu)
            val ibtnwhatsapp = view.findViewById<ImageButton>(R.id.ibtnactivitydeletedwhatsapp)
            val ibtnEmail = view.findViewById<ImageButton>(R.id.ibtnactivitydeleteditememail)
            val ibtnCall = view.findViewById<ImageButton>(R.id.ibtnactivitydeleteditemtelephone)
            val tvmessage = view.findViewById<TextView>(R.id.tvactivitydeleterequestmessage)

            val obj = datasource.getJSONObject(p0)
            tvName.text = obj.getString("first_name") + " " + obj.getString("last_name")

            val strAge = obj.getString("dob").split("-")
            var finalage = getAge(strAge[0].toInt(), strAge[1].toInt(), strAge[2].toInt())
            val arrStrHeight = obj.getString("height").split("-")
            val strHeight = arrStrHeight[0].replace(" ", "").replace("ft", "'").replace("in", "\"")
            val strProfession = obj.getString("qualification")
            tvSubTitle.text =
                finalage.toString() + " years" + ", " + strHeight + " | " + strProfession
            var address = ""
            if (!obj.getString("family_location").isNullOrEmpty()) {
                address = address + obj.getString("family_location")
            }
            if (!obj.getString("city").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("city")
                } else {
                    address = address + obj.getString("city")
                }
            }
            if (!obj.getString("state").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("state")
                } else {
                    address = address + obj.getString("state")
                }
            }
            if (!obj.getString("country").isNullOrEmpty()) {
                if (!address.isNullOrEmpty()) {
                    address = address + ", " + obj.getString("country")
                } else {
                    address = address + obj.getString("country")
                }
            }
            tvaddress.setText(address)

            var imageurl = ""
            if (!obj.getString("profile_picture").isNullOrEmpty()) {
                imageurl = obj.getString("profile_picture")
            } else {
                imageurl = "abc.jpg"
            }
            imageurl=imageurl.replace("http:","https:")
            if (imageurl.equals("abc.jpg")) {
                if (User_Data.getString("gender").equals("male")) {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_female)

                    Picasso.get()
                        .load(R.drawable.placeholder_female)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
//                    ivimage.setBackgroundResource(R.drawable.placeholder_male)
                    Picasso.get()
                        .load(R.drawable.placeholder_male)
                        .fit()
                        .centerCrop()
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            } else {
                if (User_Data.getString("gender").equals("male")) {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_female))
                        .error(context.resources.getDrawable(R.drawable.placeholder_female))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                } else {
                    Picasso.get()
                        .load(imageurl)
                        .fit()
                        .centerCrop()
                        .placeholder(context.resources.getDrawable(R.drawable.placeholder_male))
                        .error(context.resources.getDrawable(R.drawable.placeholder_male))
                        .transform(
                            CircleTransform(
                                context.resources.getDimension(
                                    R.dimen._12sdp
                                )
                                    .toInt(), 0
                            )
                        )
                        .into(ivimage)
                }
            }

            var ownits = fragment_activity.jsonUserData.getString("its_card_number")
            if (obj.getString("sender_its").equals(ownits)) {
                if (fragment_activity.jsonUserData.getString("gender").toLowerCase()
                        .equals("male")
                ) {
                    tvmessage.setText("She Deleted Your Request.")
                } else {
                    tvmessage.setText("He Deleted Your Request.")
                }
            } else if (obj.getString("receiver_its").equals(ownits)) {
                if (fragment_activity.jsonUserData.getString("gender").toLowerCase()
                        .equals("male")
                ) {
                    tvmessage.setText("You Deleted Her Request.")
                } else {
                    tvmessage.setText("You Deleted His Request.")
                }
            }
            ivmenu.setOnClickListener {
                val popup = PopupMenu(context, ivmenu)
                val inflater = popup.menuInflater
                inflater.inflate(R.menu.popupmenu, popup.menu)
                popup.show()

                popup.setOnMenuItemClickListener {
                    if (it.itemId.equals(R.id.ReportMenuItem)) {
                        ReportUser(obj.getString("id"))
                    }
                    return@setOnMenuItemClickListener true
                }
            }

            ibtnwhatsapp.setOnClickListener {
                if (!obj.getString("whats_app_no").isNullOrEmpty()) {
                    fragment_activity.ItemWhatsappClicked(
                        obj.getString("phone_code"),
                        obj.getString("whats_app_no")
                    )
                } else {
                    Toast.makeText(context, "Whatsapp Detail Not Found !", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            ibtnEmail.setOnClickListener {
                val recipient = obj.getString("email")
                fragment_activity.sendEmail(recipient)
            }
            ibtnCall.setOnClickListener {
                val number = "+" + obj.getString("phone_code") + obj.getString("mobile_no")
                fragment_activity.Call(number)
            }
            return view
        }
    }

    private fun SendResponce(
        oppItsNumber: String,
        ownItsNumber: String,
        type: String,
        actionType: String,
        item: String
    ) {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        val progressDialog = ProgressDialog(context)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "receiver_its_card_number",
            ownItsNumber
        )
        requestParams.add("sender_its_card_number", oppItsNumber)
        requestParams.add("type", type)
        requestParams.add("action_type", actionType)

        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            context.getString(R.string.Api2) + "user/activity-type-update",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            fragment_activity.getActivityData(item)
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun ReportUser(
        targetId: String
    ) {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var id = fragment_activity.jsonUserData.getString("id")
        val progressDialog = ProgressDialog(context)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "parent_id",
            id
        )
        requestParams.add(
            "target_id",
            targetId
        )
        requestParams.add(
            "text",
            "Sorry For That !"
        )
        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            context.getString(R.string.Api2) + "user/report-user",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }
            })
    }

    fun getAge(_year: Int, _month: Int, _day: Int): Int {
        val cal = GregorianCalendar()
        val y: Int
        val m: Int
        val d: Int
        var a: Int
        y = cal[Calendar.YEAR]
        m = cal[Calendar.MONTH]
        d = cal[Calendar.DAY_OF_MONTH]
        cal[_year, _month] = _day
        a = y - cal[Calendar.YEAR]
        if (m < cal[Calendar.MONTH]
            || m == cal[Calendar.MONTH] && d < cal[Calendar.DAY_OF_MONTH]
        ) {
            --a
        }
        require(a >= 0) { "Age < 0" }
        return a
    }
}
