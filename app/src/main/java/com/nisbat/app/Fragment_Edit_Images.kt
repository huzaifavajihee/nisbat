package com.nisbat.app

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.GridView
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import gun0912.tedimagepicker.builder.TedImagePicker
import gun0912.tedimagepicker.builder.listener.OnSelectedListener
import kotlinx.android.synthetic.main.item_list_activity_deleted.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.util.*
import kotlin.math.max

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Images.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Images : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var jsonObjPhoto1 = JSONObject()
    var jsonArrayPhotos2 = JSONArray()
    var jsonArrayPhotos = JSONArray()
    var jsonImageFullArray = JSONArray()
    var placeholderAdded: Boolean = false
    var firstImageUrl: String = "abc.jpg"

    lateinit var gvImages: GridView
    lateinit var ivCoverimage: ImageView
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var ivdeletefirst: ImageView
    lateinit var jsonTemp: JSONObject
    var fullimagepos = 0
    var imagecount = 0
    var isopened = true

    lateinit var progressdialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_images, container, false)
        ivCoverimage = view.findViewById<ImageView>(R.id.ivimageeditcoverimage)
        gvImages = view.findViewById<GridView>(R.id.gvimagesedit)
        ivdeletefirst = view.findViewById<ImageView>(R.id.ivfirstimagedelete)

        (activity as Edit_Profile).fragmentEditImages = this@Fragment_Edit_Images

        GetUserphotos()

        ivCoverimage.setOnClickListener {
            if (firstImageUrl.equals("abc.jpg")) {
                SelectImageFromGallery()
            } else {
                ShowImageInFullScreen(0)
            }
        }
        ivdeletefirst.setOnClickListener {
            AlertDialog.Builder(activity as Edit_Profile)
//                .setTitle("Confirm?")
                .setMessage("Do you want to delete Image?")
                .setPositiveButton(
                    "Confirm"
                ) { dialog, id ->
                    try {
                        var id = jsonObjPhoto1.getString("id")
                        var Url = jsonObjPhoto1.getString("profile_picture").split("/")
                        var filename: String = Url[Url.size - 1]
                        DeleteImage(id, filename, -1)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    dialog.cancel()
                }
                .setNegativeButton(
                    "Cancel"
                ) { dialog, id -> dialog.cancel() }.show()

        }

        gvImages.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                TODO("Not yet implemented")
            }

        })

//        GetUserphotos()
        return view
    }

    fun ShowImageInFullScreen(pos: Int) {
        (activity as Edit_Profile).framebgblackfullimage.visibility = View.VISIBLE
        (activity as Edit_Profile).framefullimagepopup.visibility = View.VISIBLE
        var imageurl = jsonImageFullArray.getJSONObject(pos).getString("profile_picture")
        fullimagepos = pos
        Picasso.get()
            .load(imageurl)
            .placeholder(R.drawable.icon_placeholder_add_image)
            .error(R.drawable.icon_logo_grey)
            .fit()
            .centerInside()
            .into((activity as Edit_Profile).fullimage)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Images.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Images().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun DeleteImage(ImageId: String, Filename: String, pos: Int) {
        val progress = ProgressDialog(activity as Edit_Profile)
        progress.setMessage("Please Wait...")
        progress.setCancelable(false)
        progress.show()
        val requestParams = RequestParams()
        requestParams.add("id", ImageId)
        requestParams.add("profile_picture", Filename)
        val asyncHttpClient = AsyncHttpClient()
        val token_type = sharedPrefencesLogin.getString("token_type", "")
        val access_token = sharedPrefencesLogin.getString("access_token", "")
        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val requestHandle = asyncHttpClient.post(
            resources.getString(R.string.Api2).toString() + "user/delete-image",
            requestParams,
            object : JsonHttpResponseHandler() {
                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (pos == -1) {
                        jsonImageFullArray.remove(0)
                        if (jsonArrayPhotos2.length() > 0) {
                            jsonObjPhoto1 = jsonArrayPhotos2.getJSONObject(0)
                            firstImageUrl = jsonObjPhoto1.getString("profile_picture")
                            jsonArrayPhotos2.remove(0)
                            if (jsonImageFullArray.length() <= 1) {
                                ivdeletefirst.visibility = View.GONE
                            }
                            if (firstImageUrl.equals("abc.jpg")) {
                                ivdeletefirst.visibility = View.GONE
                                imagecount = 0
                                var editor = sharedPrefencesLogin.edit()
                                editor.putString(
                                    "profile_picture",
                                    jsonUserData.getString("profile_picture")
                                )
                                editor.commit()
                            }
                        } else {
                            firstImageUrl = "abc.jpg"
                            ivdeletefirst.visibility = View.GONE
                            imagecount = 0
                        }
                        jsonUserData.put("profile_picture", firstImageUrl)
                        var editor = sharedPrefencesLogin.edit()
                        editor.putString(
                            "User_Data",
                            jsonUserData.toString()
                        )
                        editor.commit()
                        if (firstImageUrl.equals("abc.jpg")) {
                            Picasso.get()
                                .load(firstImageUrl)
                                .placeholder(R.drawable.icon_placeholder_add_image)
                                .error(R.drawable.icon_placeholder_add_image)
                                .transform(
                                    CircleTransform(
                                        (activity as Edit_Profile).resources.getDimension(
                                            R.dimen._12sdp
                                        )
                                            .toInt(), 0
                                    )
                                )
                                .fit()
                                .centerInside()
                                .into(ivCoverimage)
                        } else {
                            Picasso.get()
                                .load(firstImageUrl)
                                .placeholder(R.drawable.icon_placeholder_add_image)
                                .error(R.drawable.icon_logo_grey)
                                .transform(
                                    CircleTransform(
                                        (activity as Edit_Profile).resources.getDimension(
                                            R.dimen._12sdp
                                        )
                                            .toInt(), 0
                                    )
                                )
                                .fit()
                                .centerInside()
                                .into(ivCoverimage)
                        }
                        if (!placeholderAdded) {
                            jsonTemp = JSONObject()
                            jsonTemp.put("id", "0")
                            jsonTemp.put("profile_picture", "abc.jpg")
                            jsonArrayPhotos2.put(jsonTemp)
                            placeholderAdded = true
                        }
                        val adapter = Adapter_Image(
                            (activity as Edit_Profile),
                            jsonArrayPhotos2,
                            this@Fragment_Edit_Images
                        )
                        gvImages.adapter = adapter

                    } else {
                        jsonArrayPhotos2.remove(pos)
                        if (jsonArrayPhotos2.getJSONObject(0).getString("profile_picture")
                                .equals("abc.jpg") ||
                            jsonArrayPhotos2.getJSONObject(0).getString("profile_picture")
                                .isNullOrEmpty()
                        ) {
                            ivdeletefirst.visibility = View.GONE
                        }
                        jsonImageFullArray.remove(pos + 1)
                        imagecount = jsonArrayPhotos2.length() + 1
                        if (!placeholderAdded) {
                            var jsonTemp = JSONObject()
                            jsonTemp.put("id", "0")
                            jsonTemp.put("profile_picture", "abc.jpg")
                            jsonArrayPhotos2.put(jsonTemp)
                            placeholderAdded = true
                        }
                        val adapter = Adapter_Image(
                            (activity as Edit_Profile),
                            jsonArrayPhotos2,
                            this@Fragment_Edit_Images
                        )
                        gvImages.adapter = adapter
                    }
                    progress.dismiss()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progress.dismiss()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progress.dismiss()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progress.dismiss()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    fun GetUserphotos() {
        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )
        requestParams.add("status", "1")
        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPrefencesLogin.getString("token_type", "")
        var access_token = sharedPrefencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/get-user-profile",
            requestParams,
            object : JsonHttpResponseHandler() {
                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObjuserdata: JSONObject = response.getJSONObject("data")
                            jsonArrayPhotos = JSONArray()
                            if ((jsonObjuserdata.getString("profilePicture").isNullOrEmpty())) {
                                var jsonTemp = JSONObject()
                                jsonTemp.put("id", "0")
                                jsonTemp.put("profile_picture", "abc.jpg")
                                jsonObjPhoto1 = jsonTemp
                                jsonArrayPhotos2.put(jsonTemp)
                                firstImageUrl = jsonTemp.getString("profile_picture")
                                var editor: SharedPreferences.Editor = sharedPrefencesLogin.edit()
                                editor.putString("profile_picture", "abc.jpg")
                                editor.commit()
                                ivdeletefirst.visibility = View.GONE
//                                Picasso.get()
//                                    .load(firstImageUrl)
//                                    .placeholder(R.drawable.icon_placeholder_add_image)
//                                    .transform(
//                                        CircleTransform(
//                                            (activity as Edit_Profile).resources.getDimension(
//                                                R.dimen._12sdp
//                                            )
//                                                .toInt(), 0
//                                        )
//                                    )
//                                    .fit()
//                                    .centerCrop()
//                                    .into(ivCoverimage)
                            } else {
                                jsonImageFullArray =
                                    JSONArray(jsonObjuserdata.getString("profilePicture"))
                                jsonArrayPhotos =
                                    JSONArray(jsonObjuserdata.getString("profilePicture"))
                                if (jsonArrayPhotos.length() <= 1) {
                                    ivdeletefirst.visibility = View.GONE
                                } else {
                                    ivdeletefirst.visibility = View.VISIBLE
                                }
                                imagecount = jsonArrayPhotos.length()
                                jsonObjPhoto1 = jsonArrayPhotos.getJSONObject(0)
                                firstImageUrl = jsonObjPhoto1.getString("profile_picture")
                                firstImageUrl=firstImageUrl.replace("http:","https:")
                                jsonUserData.put("profile_picture", firstImageUrl)
                                var editor = sharedPrefencesLogin.edit()
                                editor.putString("User_Data", jsonUserData.toString())
                                editor.commit()
                                if (jsonObjuserdata.getString("gender").equals("male")) {
                                    Picasso.get()
                                        .load(firstImageUrl)
                                        .placeholder(R.drawable.placeholder_male)
                                        .error(R.drawable.placeholder_male)
                                        .transform(
                                            CircleTransform(
                                                (activity as Edit_Profile).resources.getDimension(
                                                    R.dimen._12sdp
                                                )
                                                    .toInt(), 0
                                            )
                                        )
                                        .fit()
                                        .centerCrop()
                                        .into(ivCoverimage)
                                }else
                                {
                                    Picasso.get()
                                        .load(firstImageUrl)
                                        .placeholder(R.drawable.placeholder_female)
                                        .error(R.drawable.placeholder_female)
                                        .transform(
                                            CircleTransform(
                                                (activity as Edit_Profile).resources.getDimension(
                                                    R.dimen._12sdp
                                                )
                                                    .toInt(), 0
                                            )
                                        )
                                        .fit()
                                        .centerCrop()
                                        .into(ivCoverimage)
                                }
                                jsonArrayPhotos2 = jsonArrayPhotos
                                jsonArrayPhotos2.remove(0)
                                if (jsonArrayPhotos2.length() < 4) {
                                    jsonTemp = JSONObject()
                                    jsonTemp.put("id", "0")
                                    jsonTemp.put("profile_picture", "abc.jpg")
                                    jsonArrayPhotos2.put(jsonTemp)
                                    placeholderAdded = true
                                } else {
                                    placeholderAdded = false
                                }
                                val adapter = Adapter_Image(
                                    (activity as Edit_Profile),
                                    jsonArrayPhotos2,
                                    this@Fragment_Edit_Images
                                )
                                gvImages.adapter = adapter
                            }
                        } else if (status == 401) {

                            Toast.makeText(
                                (activity as HomeScreen),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                (activity as HomeScreen),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }
            })
    }

    fun SelectImageFromGallery() {
//        var PICK_IMAGE_MULTIPLE=1
        val intent: Intent = Intent()
        intent.type = "image/*"
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true)
        intent.action = Intent.ACTION_PICK
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {

            if (data != null) {
                val contentURI = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor? = context?.getContentResolver()?.query(
                    contentURI!!,
                    filePathColumn, null, null, null
                )
                cursor?.moveToFirst()
                val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
                var picturePath = cursor?.getString(columnIndex!!)
                cursor?.close()
                Log.d("picturePath", picturePath!!)

                // Pathuri=picturePath;
                try {
                    var bitmap = MediaStore.Images.Media.getBitmap(
                        context?.getContentResolver(),
                        contentURI
                    )
                    var newBitmap = modifyOrientation(bitmap, picturePath)
                    val bytes = ByteArrayOutputStream()
                    newBitmap?.compress(Bitmap.CompressFormat.PNG, 0, bytes)

                    if (newBitmap != null) {

                        saveImage(newBitmap)
                    }

//                    val f = File(picturePath)
//                    UploadImage(f)
                } catch (e: FileNotFoundException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                } catch (e: IOException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }
        }
    }


    @Throws(IOException::class)
    fun modifyOrientation(bitmap: Bitmap, image_absolute_path: String?): Bitmap? {
        val ei = ExifInterface(image_absolute_path!!)
        val orientation: Int =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate(bitmap, 270f)
//            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> flip(bitmap, true, false)
//            ExifInterface.ORIENTATION_FLIP_VERTICAL -> flip(bitmap, false, true)
            else -> bitmap
        }
    }

    fun rotate(bitmap: Bitmap, degrees: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun saveImage(myBitmap: Bitmap) {

        var file = File("")
        var progressDialog: ProgressDialog = ProgressDialog(activity as Edit_Profile)
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        Thread {
            val bytes = ByteArrayOutputStream()
            myBitmap.compress(Bitmap.CompressFormat.PNG, 30, bytes)
            val wallpaperDirectory = File(
                Environment.getExternalStorageDirectory()
                    .toString() + "/Nisbat_Photo"
            )
            // have the object build the directory structure, if needed.
            if (!wallpaperDirectory.exists()) {
                wallpaperDirectory.mkdirs()
            }
            try {
                file = File(
                    wallpaperDirectory, "Image" + ".png"
                )
                val fo = FileOutputStream(file)
                fo.write(bytes.toByteArray())
                MediaScannerConnection.scanFile(
                    context,
                    arrayOf(file.path),
                    arrayOf("image/jpeg"),
                    null
                )
                fo.close()
            } catch (e1: IOException) {
                e1.printStackTrace()
            }
            progressDialog.dismiss()
            (activity as Edit_Profile).runOnUiThread({
                UploadImage(file)
            })
        }.start()
    }

    private fun UploadImage(imageFile: File) {
        val progressDialog = ProgressDialog(activity as Edit_Profile)
//        progressDialog.setTitle("Update")
        progressDialog.setMessage("Uploading Image...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()

        requestParams.put("profile_picture", imageFile)
        val asyncHttpClient = AsyncHttpClient()
        asyncHttpClient.setTimeout(20000)

        var token_type = sharedPrefencesLogin.getString("token_type", "")
        var access_token = sharedPrefencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/add-profile-picture",
            requestParams,
            object : JsonHttpResponseHandler() {
                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            progressDialog.dismiss()
                            GetUserphotos()
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (activity as HomeScreen),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (activity as HomeScreen),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as Edit_Profile,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    override fun onResume() {
        super.onResume()

    }
}
