package com.nisbat.app

import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class Register_Info : AppCompatActivity() {

    val manager = supportFragmentManager

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_info)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = ContextCompat.getColor(this,R.color.color_white)
        }

        sharedPrefencesLogin =
            this@Register_Info.getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            this@Register_Info.getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))

        val tvusername = findViewById<TextView>(R.id.tvregisterinfousername)
        val btnthankyouok = findViewById<Button>(R.id.btnthankyouscreenok)

        //set text on screen
        tvusername.text = "Hi, " + jsonUserData.getString("first_name")

        ShowBasicFragment()
//        ShowLocationInfo()
    }

    fun ShowBasicFragment() {
        val transaction = manager.beginTransaction()
        val fragment = Fragment_Register_Basic_Info()
        transaction.replace(R.id.frameregisterdetailfragments, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun ShowCareerInfo() {
        val transaction = manager.beginTransaction()
        val fragment = Fragment_Register_Career_Info()
        transaction.replace(R.id.frameregisterdetailfragments, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun ShowLocationInfo() {
        val transaction = manager.beginTransaction()
        val fragment = Fragment_Register_Location_Info()
        transaction.replace(R.id.frameregisterdetailfragments, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun ShowFamilyInfo() {
        val transaction = manager.beginTransaction()
        val fragment = Fragment_Register_Family_Info()
        transaction.replace(R.id.frameregisterdetailfragments, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun ShowContactInfo() {
        val transaction = manager.beginTransaction()
        val fragment = Fragment_Register_Contact_Info()
        transaction.replace(R.id.frameregisterdetailfragments, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun ShowYourInfo() {
        val transaction = manager.beginTransaction()
        val fragment = Fragment_Register_Your_Info()
        transaction.replace(R.id.frameregisterdetailfragments, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun FinalSubmitButtonClicked() {

        val progressDialog = ProgressDialog(this@Register_Info)
//        progressDialog.setTitle("Update")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )
        requestParams.add("profile_for", jsonUserData.getString("profile_for"))
        requestParams.add(
            "profile_created_by",
            sharedprefencesReginfo.getString("profile_created_by", "")
        )
        requestParams.add("first_name", jsonUserData.getString("first_name"))
        requestParams.add("last_name", jsonUserData.getString("last_name"))
        requestParams.add("gender", jsonUserData.getString("gender"))
        requestParams.add("dob", jsonUserData.getString("dob"))
        requestParams.add(
            "marital_status",
            sharedprefencesReginfo.getString("marital_status", "")
        )
        requestParams.add("height", sharedprefencesReginfo.getString("height", ""))
        requestParams.add(
            "body_weight_kg",
            sharedprefencesReginfo.getString("body_weight_kg", "")
        )
        requestParams.add(
            "any_disability",
            sharedprefencesReginfo.getString("any_disability", "")
        )
        requestParams.add(
            "health_problem",
            sharedprefencesReginfo.getString("health_problem", "")
        )
        requestParams.add("blood_group", sharedprefencesReginfo.getString("blood_group", ""))
        requestParams.add(
            "whats_your_diet",
            sharedprefencesReginfo.getString("whats_your_diet", "")
        )
        requestParams.add("language", sharedprefencesReginfo.getString("languages", ""))
        requestParams.add("qualification", sharedprefencesReginfo.getString("qualification", ""))
        requestParams.add("college_name", sharedprefencesReginfo.getString("college_name", ""))
        requestParams.add("occupation", sharedprefencesReginfo.getString("occupation", ""))
        requestParams.add("annual_income", sharedprefencesReginfo.getString("annual_income", ""))
        requestParams.add("country", sharedprefencesReginfo.getString("country", ""))
        requestParams.add("state", sharedprefencesReginfo.getString("state", ""))
        requestParams.add("city", sharedprefencesReginfo.getString("city", ""))
        requestParams.add(
            "residency_status",
            sharedprefencesReginfo.getString("residency_status", "")
        )
        requestParams.add("grew_up_in", sharedprefencesReginfo.getString("grew_up_in", ""))
        requestParams.add(
            "fathers_status",
            sharedprefencesReginfo.getString("fathers_status", "")
        )
        requestParams.add(
            "mothers_status",
            sharedprefencesReginfo.getString("mothers_status", "")
        )
        requestParams.add(
            "family_location",
            sharedprefencesReginfo.getString("family_location", "")
        )
        requestParams.add("native_place", sharedprefencesReginfo.getString("native_place", ""))
        requestParams.add(
            "married_brothers",
            sharedprefencesReginfo.getString("married_brothers", "")
        )
        requestParams.add(
            "unmarried_brothers",
            sharedprefencesReginfo.getString("unmarried_brothers", "")
        )
        requestParams.add(
            "married_sisters",
            sharedprefencesReginfo.getString("married_sisters", "")
        )
        requestParams.add(
            "unmarried_sisters",
            sharedprefencesReginfo.getString("unmarried_sisters", "")
        )
        requestParams.add("family_type", sharedprefencesReginfo.getString("family_type", ""))
        requestParams.add("family_lifestyles", sharedprefencesReginfo.getString("family_lifestyles", ""))
        requestParams.add(
            "family_background",
            sharedprefencesReginfo.getString("family_background", "")
        )
        requestParams.add("email", jsonUserData.getString("email"))
        requestParams.add("mobile_no", jsonUserData.getString("mobile_no"))
        requestParams.add("phone_code", jsonUserData.getString("phone_code"))
        requestParams.add("whats_app_no", sharedprefencesReginfo.getString("whats_app_no", ""))
        requestParams.add("instagram_id", sharedprefencesReginfo.getString("instagram_id", ""))
        requestParams.add("facebook_id", sharedprefencesReginfo.getString("facebook_id", ""))
        requestParams.add(
            "linkedin_profile",
            sharedprefencesReginfo.getString("linkedin_profile", "")
        )
        requestParams.add("about_me", sharedprefencesReginfo.getString("about_me", ""))

        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPrefencesLogin.getString("token_type", "")
        var access_token = sharedPrefencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )

        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/store-basic-info",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {

                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObjuserdata: JSONObject = response.getJSONObject("data")
                            var editor = sharedPrefencesLogin.edit()
                            editor.putString("User_Data", jsonObjuserdata.toString())
                            editor.putBoolean("is_info_updated",true)
                            editor.commit()
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@Register_Info),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()

                            //Starting new Activity
                            val intent = Intent(applicationContext, Edit_Profile::class.java)
                            intent.putExtra("FragmentName", "Images")
                            intent.putExtra("From","RegisterInfo")
                            startActivity(intent)

                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@Register_Info),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@Register_Info),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    response: JSONArray?
                ) {
                    super.onSuccess(statusCode, headers, response)
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@Register_Info,"Internal Server Error !",Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@Register_Info,"Internal Server Error !",Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@Register_Info,"Internal Server Error !",Toast.LENGTH_SHORT).show()
                }
            })


    }


    override fun onBackPressed() {

        val fragments: ArrayList<Fragment>
        fragments = supportFragmentManager.fragments as ArrayList<Fragment>
        val fragmentsize = fragments.size


        val fragment: Fragment
        fragment = fragments[fragmentsize - 1]
        if (fragment is Fragment_Register_Career_Info) {

            super.onBackPressed()
//            val fragmentManager = supportFragmentManager
//            fragmentManager.beginTransaction()
//                .replace(R.id.FrameLayoutFragment, fragment_products)
//                .addToBackStack(null)
//                .commit()
        } else if (fragment is Fragment_Register_Location_Info) {
            super.onBackPressed()
        } else if (fragment is Fragment_Register_Family_Info) {
            super.onBackPressed()
        } else if (fragment is Fragment_Register_Contact_Info) {
            super.onBackPressed()
        } else if (fragment is Fragment_Register_Your_Info) {
            super.onBackPressed()
        }
    }
}