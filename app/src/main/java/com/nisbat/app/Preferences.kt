package com.nisbat.app

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.log

class Preferences : AppCompatActivity() {

    var strminAge = ""
    var strmaxAge = ""
    var strminheight = ""
    var strmaxheight = ""
    var strmaritalstatus = ""
    var strmothertongue = ""
    var strcountry = ""
    var strstate = ""
    var strqualification = ""
    var strcurrency = "0"
    var strcurrencybelow = "0"
    var strcurrencyabove = "0"
    var strdiet = "0"
    var strprofilecreatedby = "0"

    var stringArrayCountry = ArrayList<String>()
    var stringArrayState = ArrayList<String>()
    var jsonArrayCountry: JSONArray = JSONArray()
    var jsonArrayState: JSONArray = JSONArray()

    lateinit var SpinnerCountry: Spinner
    lateinit var SpinnerState: Spinner
    lateinit var SpinnerCurrency: Spinner
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedPrefencesPref: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var RangeSeekBarAge: RangeSeekBar
    lateinit var RangeSeekBarHeight: RangeSeekBar
    lateinit var SpinnerAbove: Spinner
    lateinit var SpinnerBelow: Spinner
    lateinit var SpinnerDiet: Spinner
    lateinit var SpinnerMaritalStatus: Spinner
    lateinit var SpinnerMotherTongue: Spinner
    lateinit var SpinnerProfileCreatedby: Spinner
    lateinit var SpinnerQualification: Spinner
    lateinit var LinearIncomeDoesntmatter: LinearLayout
    lateinit var LinearIncomeSpecifyRange: LinearLayout
    lateinit var ivdoesntmatterinside: ImageView
    lateinit var ivspecifyincomeinside: ImageView
    lateinit var LinearIncomeRange: LinearLayout
    lateinit var LinearLocationdetails: LinearLayout
    lateinit var LinearEducation: LinearLayout
    lateinit var LinearIncome: LinearLayout
    lateinit var LinearLifestyle: LinearLayout
    lateinit var LinearOthers: LinearLayout
    lateinit var tvminage: TextView
    lateinit var tvmaxage: TextView
    lateinit var tvminheight: TextView
    lateinit var tvmaxheight: TextView
    lateinit var framelocation: FrameLayout
    lateinit var frameeducation: FrameLayout
    lateinit var frameannualincome: FrameLayout
    lateinit var framelifestyle: FrameLayout
    lateinit var frameothers: FrameLayout
    lateinit var db: CSC_Database_Helper
    lateinit var scrollView: ScrollView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.nisbat.app.R.layout.activity_preferences)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = ContextCompat.getColor(this, R.color.color_white)
        }

        sharedPrefencesLogin =
            (this@Preferences).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))


        val ibtnback = findViewById<ImageButton>(R.id.ibtnpreferenceback)
        RangeSeekBarAge = findViewById<RangeSeekBar>(R.id.rangeseekbarprefage)
        RangeSeekBarHeight = findViewById<RangeSeekBar>(R.id.rangeseekbarprefheight)
        SpinnerMaritalStatus = findViewById<Spinner>(R.id.spinnerprefmaritalstatus)
//        SpinnerMotherTongue = findViewById<Spinner>(R.id.spinnerprefmothertongue)
        SpinnerCountry = findViewById<Spinner>(R.id.spinnerprefcountrylivingin)
        SpinnerState = findViewById<Spinner>(R.id.spinnerprefstatelivingin)
        SpinnerQualification = findViewById<Spinner>(R.id.spinnerprefqualification)
        SpinnerCurrency = findViewById<Spinner>(R.id.spinnerprefcurrency)
        SpinnerBelow = findViewById<Spinner>(R.id.spinnerprefbelow)
        SpinnerAbove = findViewById<Spinner>(R.id.spinnerprefabove)
        SpinnerDiet = findViewById<Spinner>(R.id.spinnerprefdiet)
        SpinnerProfileCreatedby = findViewById<Spinner>(R.id.spinnerprefprofilecreatedby)
        LinearIncomeDoesntmatter = findViewById<LinearLayout>(R.id.linearprefincomedoentmatter)
        LinearIncomeSpecifyRange = findViewById<LinearLayout>(R.id.linearprefincomespecifyrange)
        ivdoesntmatterinside = findViewById<ImageView>(R.id.ivprefdoesntmatterinside)
        ivspecifyincomeinside = findViewById<ImageView>(R.id.ivprefspecifyrangeinside)
        LinearIncomeRange = findViewById<LinearLayout>(R.id.linearincomerange)
        LinearLocationdetails = findViewById<LinearLayout>(R.id.linearlocationdetail)
        LinearEducation = findViewById<LinearLayout>(R.id.lineareducation)
        LinearIncome = findViewById<LinearLayout>(R.id.linearincome)
        LinearLifestyle = findViewById<LinearLayout>(R.id.linearlifestyle)
        LinearOthers = findViewById<LinearLayout>(R.id.linearothers)
        tvminage = findViewById<TextView>(R.id.tvprefminage)
        tvmaxage = findViewById<TextView>(R.id.tvprefmaxage)
        tvminheight = findViewById<TextView>(R.id.tvprefminheight)
        tvmaxheight = findViewById<TextView>(R.id.tvprefmaxheight)
        framelocation = findViewById<FrameLayout>(R.id.framepreferencesLocationDetail)
        frameeducation = findViewById<FrameLayout>(R.id.frameprefeerencesEducation)
        frameannualincome = findViewById<FrameLayout>(R.id.framepreferencesAnnualIncome)
        framelifestyle = findViewById<FrameLayout>(R.id.framepreferencesLifestyle)
        frameothers = findViewById<FrameLayout>(R.id.framepreferencesOthers)
        scrollView = findViewById<ScrollView>(R.id.scrollviewpreferences)

        scrollView.visibility = View.GONE
        GetPreferenceValues()

        db = CSC_Database_Helper(this@Preferences)
        jsonArrayCountry = db.GetCountry()!!
//        if (SpinnerCountry != null) {
//            val adapterSpinnerCsc = Adapter_Spinner_Pref_3(this@Preferences, jsonArrayCountry, null)
//            SpinnerCountry.adapter = adapterSpinnerCsc
//            if (!jsonUserData.getString("country").isNullOrEmpty()) {
//                var tmpstr =
//                    jsonUserData.getString("country")
//                for (i in 0 until jsonArrayCountry.length()) {
//                    var tmp = jsonArrayCountry.getJSONObject(i).getString("id")
//                    if (tmpstr.equals(tmp)) {
//                        SpinnerCountry.setSelection(i)
//                        strcountry = tmp
//                        jsonArrayState = db.GetStates(strcountry)!!
//                    }
//                }
//            }
//        }
        RangeSeekBarAge.setProgress(18F, 60F)
        RangeSeekBarHeight.setProgress(4.9F, 6.4F)
        strminAge = "18"
        strmaxAge = "60"
        strminheight = "4.9"
        strmaxheight = "6.4"
        RangeSeekBarAge.setOnRangeChangedListener(object : OnRangeChangedListener {

            var left: String = ""
            var right: String = ""
            override fun onRangeChanged(
                view: RangeSeekBar?,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {
                left = leftValue.toInt().toString()
                right = rightValue.toInt().toString()
            }

            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
                if (view != null && !isLeft) {
                    strmaxAge = right
                    tvmaxage.setText(strmaxAge)
                } else {
                    strminAge = left
                    tvminage.setText(strminAge)
                }
            }
        })
        RangeSeekBarHeight.setOnRangeChangedListener(object : OnRangeChangedListener {

            var left: String = ""
            var right: String = ""
            override fun onRangeChanged(
                view: RangeSeekBar?,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {
                val formatter: NumberFormat = NumberFormat.getNumberInstance()
                formatter.setMinimumFractionDigits(1)
                formatter.setMaximumFractionDigits(1)
                left = formatter.format(leftValue)
                right = formatter.format(rightValue)
            }

            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
                if (view != null && !isLeft) {
                    strmaxheight = right
                    tvmaxheight.setText(strmaxheight)
                } else {
                    strminheight = left
                    tvminheight.setText(strminheight)
                }
            }
        })
        //Setting default views of elements
        LinearLocationdetails.visibility = View.GONE
        LinearEducation.visibility = View.GONE
        LinearIncome.visibility = View.GONE
        LinearLifestyle.visibility = View.GONE
        LinearOthers.visibility = View.GONE
        //setting radiobutton for Annualincome
        ivdoesntmatterinside.visibility = View.VISIBLE
        ivspecifyincomeinside.visibility = View.GONE
        LinearIncomeRange.visibility = View.GONE

        //set value in spinner

//        if (SpinnerMaritalStatus != null) {
//            val adapter = Adapter_Spinner_Pref(
//                this,
//                resources.getStringArray(R.array.MaritulStatus).toMutableList(), null
//            )
//            SpinnerMaritalStatus.adapter = adapter
//            var tmpstr = jsonUserData.getString("marital_status")?.replace("_", " ")
//            if (!tmpstr.isNullOrEmpty()) {
//                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
//                var pos = resources.getStringArray(R.array.MaritulStatus).indexOf(tmpstr)
//                SpinnerMaritalStatus.setSelection(pos)
//                strmaritalstatus = resources.getStringArray(R.array.MaritulStatus).get(pos)
//            }
//        }
//        if (SpinnerMotherTongue != null) {
//            val adapter = Adapter_Spinner_Pref(
//                this,
//                resources.getStringArray(R.array.Languages).toMutableList(),
//                null
//            )
//            SpinnerMotherTongue.adapter = adapter
//            var tmpstr = jsonUserData.getString("language")?.replace("_", " ")
//            if (!tmpstr.isNullOrEmpty()) {
//                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
//                var pos = resources.getStringArray(R.array.Languages).indexOf(tmpstr)
//                SpinnerMotherTongue.setSelection(pos)
//                strmothertongue = resources.getStringArray(R.array.Languages).get(pos)
//            }
//        }
        if (SpinnerQualification != null) {
            val adapter = Adapter_Spinner_Pref(
                this,
                resources.getStringArray(R.array.Qualification).toMutableList(),
                null
            )
            SpinnerQualification.adapter = adapter
            strqualification = ""
        }
        if (SpinnerDiet != null) {
            val adapter = Adapter_Spinner_Pref(
                this,
                resources.getStringArray(R.array.DietPref).toMutableList(),
                null
            )
            SpinnerDiet.adapter = adapter
            SpinnerDiet.setSelection(resources.getStringArray(R.array.DietPref).size - 1)
            strdiet = resources.getStringArray(R.array.DietPref)
                .get(resources.getStringArray(R.array.DietPref).size - 1)
        }
        if (SpinnerProfileCreatedby != null) {
            val adapter = Adapter_Spinner_Pref(
                this,
                resources.getStringArray(R.array.ProfieCreatedByPref).toMutableList(),
                null
            )
            SpinnerProfileCreatedby.adapter = adapter
            SpinnerProfileCreatedby.setSelection(resources.getStringArray(R.array.ProfieCreatedByPref).size - 1)
            strprofilecreatedby = resources.getStringArray(R.array.ProfieCreatedByPref)
                .get(resources.getStringArray(R.array.ProfieCreatedByPref).size - 1)
        }

        //Radio button Click
        LinearIncomeDoesntmatter.setOnClickListener {
            ivdoesntmatterinside.visibility = View.VISIBLE
            ivspecifyincomeinside.visibility = View.GONE
            LinearIncomeRange.visibility = View.GONE
            strcurrency = "0"
            strcurrencyabove = "0"
            strcurrencybelow = "0"
        }
        LinearIncomeSpecifyRange.setOnClickListener {
            ivdoesntmatterinside.visibility = View.GONE
            ivspecifyincomeinside.visibility = View.VISIBLE
            LinearIncomeRange.visibility = View.VISIBLE
            if (SpinnerCurrency != null) {
                val adapter = Adapter_Spinner_Pref(
                    this,
                    resources.getStringArray(R.array.Currency).toMutableList(),
                    null
                )
                SpinnerCurrency.adapter = adapter
                strcurrency = "INR"
            }
            if (SpinnerAbove != null) {
                val adapter = Adapter_Spinner_Pref(
                    this,
                    resources.getStringArray(R.array.CurrencyAboveINR).toMutableList(),
                    null
                )
                SpinnerAbove.adapter = adapter
                strcurrencyabove = resources.getStringArray(R.array.CurrencyAboveINR).get(0)
            }
            if (SpinnerBelow != null) {
                val adapter = Adapter_Spinner_Pref(
                    this,
                    resources.getStringArray(R.array.CurrencyBeloveINR).toMutableList(),
                    null
                )
                SpinnerBelow.adapter = adapter
                strcurrencybelow = resources.getStringArray(R.array.CurrencyBeloveINR).get(0)
            }
        }

        //Hide/Show Layouts
        framelocation.setOnClickListener {

            if (LinearLocationdetails.visibility == View.VISIBLE) {
                LinearLocationdetails.visibility = View.GONE
            } else {
                LinearLocationdetails.visibility = View.VISIBLE
            }

        }
        frameeducation.setOnClickListener {
            if (LinearEducation.visibility == View.VISIBLE) {
                LinearEducation.visibility = View.GONE
            } else {
                LinearEducation.visibility = View.VISIBLE
            }

        }
        frameannualincome.setOnClickListener {
            if (LinearIncome.visibility == View.VISIBLE) {
                LinearIncome.visibility = View.GONE
            } else {
                LinearIncome.visibility = View.VISIBLE
            }

        }
        framelifestyle.setOnClickListener {
            if (LinearLifestyle.visibility == View.VISIBLE) {
                LinearLifestyle.visibility = View.GONE
            } else {
                LinearLifestyle.visibility = View.VISIBLE
            }

        }
        frameothers.setOnClickListener {
            if (LinearOthers.visibility == View.VISIBLE) {
                LinearOthers.visibility = View.GONE
            } else {
                LinearOthers.visibility = View.VISIBLE
            }

        }

        //spinner item click
        SpinnerMaritalStatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strmaritalstatus =
                        resources.getStringArray(R.array.MaritulStatus).get(position)
                } else {
                    strmaritalstatus = ""
                }

                Log.d("TAG", "onItemSelected: " + strmaritalstatus)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

//        SpinnerMotherTongue.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//
//                if (position != 0) {
//                    strmothertongue =
//                        resources.getStringArray(R.array.Languages).get(position)
//                } else {
//                    strmothertongue = ""
//                }
//
//                Log.d("TAG", "onItemSelected: " + strmothertongue)
//
//
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("Not yet implemented")
//            }
//        }

        SpinnerCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                strcountry = jsonArrayCountry.getJSONObject(position).getString("id")
//                if (strcountry.equals("0")) {
//                    strcountry = ""
//                }
//                GetStateList(strcountry)
                if (!strcountry.isNullOrEmpty()) {
                    jsonArrayState = db.GetStates(strcountry)!!

                    SetStatesAsSelected(strcountry)
                }


            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                strstate = "0"
            }
        }

        SpinnerState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                strstate = jsonArrayState.getJSONObject(position).getString("id")
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        SpinnerQualification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strqualification = resources.getStringArray(R.array.Qualification).get(position)
                } else {
                    strqualification = ""
                }

                Log.d("TAG", "onItemSelected: " + strqualification)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        SpinnerDiet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (position != resources.getStringArray(R.array.DietPref).size - 1) {
                    strdiet = resources.getStringArray(R.array.DietPref).get(position)
                } else {
                    strdiet = "0"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        SpinnerProfileCreatedby.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {

                    if (position != resources.getStringArray(R.array.ProfieCreatedByPref).size - 1) {
                        strprofilecreatedby =
                            resources.getStringArray(R.array.ProfieCreatedByPref).get(position)
                    } else {
                        strprofilecreatedby = "0"
                    }
                    if (strprofilecreatedby.equals("Parent/Guardian")) {
                        strprofilecreatedby = "Parent_Guardian"
                    }
                    Log.d("TAG", "onItemSelected: " + strprofilecreatedby)


                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        SpinnerCurrency.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    strcurrency = "INR"
                    if (SpinnerBelow != null) {
                        val adapter = Adapter_Spinner_Pref(
                            this@Preferences,
                            resources.getStringArray(R.array.CurrencyBeloveINR).toMutableList(),
                            null
                        )
                        SpinnerBelow.adapter = adapter
                        if (strcurrencybelow.isNullOrEmpty()) {
                            strcurrencybelow =
                                resources.getStringArray(R.array.CurrencyBeloveINR).get(0)
                        } else {
                            var pos =
                                resources.getStringArray(R.array.CurrencyBeloveINR)
                                    .indexOf(strcurrencybelow)
                            if (pos > 0) {
                                SpinnerBelow.setSelection(pos)
                            }
                        }
                    }
                    if (SpinnerAbove != null) {
                        val adapter = Adapter_Spinner_Pref(
                            this@Preferences,
                            resources.getStringArray(R.array.CurrencyAboveINR).toMutableList(),
                            null
                        )
                        SpinnerAbove.adapter = adapter
                        if (strcurrencyabove.isNullOrEmpty()) {
                            strcurrencyabove =
                                resources.getStringArray(R.array.CurrencyAboveINR).get(0)
                        } else {
                            var pos =
                                resources.getStringArray(R.array.CurrencyAboveINR)
                                    .indexOf(strcurrencyabove)
                            if (pos > 0) {
                                SpinnerAbove.setSelection(pos)
                            }
                        }
                    }

                } else if (position == 1) {
                    strcurrency = "USD"
                    if (SpinnerBelow != null) {
                        val adapter = Adapter_Spinner_Pref(
                            this@Preferences,
                            resources.getStringArray(R.array.CurrencyBeloveUSD).toMutableList(),
                            null
                        )
                        SpinnerBelow.adapter = adapter
                        strcurrencybelow =
                            resources.getStringArray(R.array.CurrencyBeloveUSD).get(0)
                    }
                    if (SpinnerAbove != null) {
                        val adapter = Adapter_Spinner_Pref(
                            this@Preferences,
                            resources.getStringArray(R.array.CurrencyAboveUSD).toMutableList(),
                            null
                        )
                        SpinnerAbove.adapter = adapter
                        strcurrencyabove = resources.getStringArray(R.array.CurrencyAboveUSD).get(0)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }
        }
        SpinnerBelow.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (strcurrency.equals("INR")) {
                        strcurrencybelow =
                            resources.getStringArray(R.array.CurrencyBeloveINR).get(position)
                    } else {
                        strcurrencybelow =
                            resources.getStringArray(R.array.CurrencyBeloveUSD).get(position)
                    }
                    Log.d("TAG", "onItemSelected: " + strcurrencybelow)

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        SpinnerAbove.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (strcurrency.equals("INR")) {
                        strcurrencyabove =
                            resources.getStringArray(R.array.CurrencyAboveINR).get(position)
                    } else {
                        strcurrencyabove =
                            resources.getStringArray(R.array.CurrencyAboveUSD).get(position)
                    }

                    Log.d("TAG", "onItemSelected: " + strcurrencyabove)

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {

                }
            }

        ibtnback.setOnClickListener {
            finish()
        }


    }

    private fun SetStatesAsSelected(strcountry: String) {
//        jsonArrayState = JSONArray()
//        for (i in 0 until tempjsonArrayState.length()) {
//            var tempObj = tempjsonArrayState.getJSONObject(i)
//            var country_Id = tempObj.getString("country_id")
//            if (country_Id.equals(strcountry)) {
//                jsonArrayState.put(tempObj)
//            }
//        }
        if (SpinnerState != null) {
            val adapter =
                Adapter_Spinner_Pref_3(
                    this,
                    jsonArrayState, null
                )
            SpinnerState.adapter = adapter
        }
        for (i in 0 until jsonArrayState.length()) {
            val currentid: String =
                jsonArrayState.getJSONObject(i).getString("id")
            if (strstate.equals(currentid)) {
                SpinnerState.setSelection(i)
                strstate = currentid
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun SetPreferenceValues() {
        val progressDialog = ProgressDialog(this@Preferences)
//        progressDialog.setTitle("SignUp")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        var its = jsonUserData.getString("its_card_number")
        requestParams.add(
            "its_card_number",
            its
        )
        requestParams.add("min_age", strminAge)
        requestParams.add("max_age", strmaxAge)
        requestParams.add("min_height", strminheight)
        requestParams.add("max_height", strmaxheight)
        requestParams.add("marital_status", strmaritalstatus.replace(" ", "_"))
        requestParams.add("mother_tongue", strmothertongue.replace(" ", "_"))
        requestParams.add("country", strcountry)
        requestParams.add("state", strstate)
        requestParams.add("qualification", strqualification.replace(" ", "_"))
        requestParams.add("currency", strcurrency)
        requestParams.add("below", strcurrencybelow.toLowerCase())
        requestParams.add("above", strcurrencyabove.toLowerCase())
        requestParams.add("whats_your_diet", strdiet.replace(" ", "_"))
        requestParams.add("profile_created_by", strprofilecreatedby.replace(" ", "_"))

        val asyncHttpClient = AsyncHttpClient()
        var token_type = sharedPrefencesLogin.getString("token_type", "")
        var access_token = sharedPrefencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
//        asyncHttpClient.addHeader("Content-Type","application/json")
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/set-preferences",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
//                        val spinnerlivingin = findViewById<Spinner>(R.id.spinnerregisterlivingin)
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObjuserdata: JSONObject = response.getJSONObject("data")
                            sharedPrefencesPref =
                                (this@Preferences).getSharedPreferences("pref_user_preferences", 0)
                            var editor = sharedPrefencesPref.edit()
                            editor.putString("User_Pref_data", jsonObjuserdata.toString())
                            editor.commit()
                            ShowAlertDialogue("Partner preference has been saved successfully")
                            progressDialog.dismiss()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@Preferences, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@Preferences, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@Preferences, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    fun GetPreferenceValues() {
        val progressDialog = ProgressDialog(this@Preferences)
//        progressDialog.setTitle("SignUp")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        var its = jsonUserData.getString("its_card_number")
        requestParams.add(
            "its_card_number",
            its
        )
        val asyncHttpClient = AsyncHttpClient()
        var token_type = sharedPrefencesLogin.getString("token_type", "")
        var access_token = sharedPrefencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
//        asyncHttpClient.addHeader("Content-Type","application/json")
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/get-preferences",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
//                        val spinnerlivingin = findViewById<Spinner>(R.id.spinnerregisterlivingin)
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObject = JSONObject(response.getString("data"))
                            if (jsonObject.has("min_age") && !jsonObject.getString("min_age")
                                    .isNullOrEmpty()
                            ) {
                                strminAge = jsonObject.getString("min_age")
                            }
                            if (jsonObject.has("max_age") && !jsonObject.getString("max_age")
                                    .isNullOrEmpty()
                            ) {
                                strmaxAge = jsonObject.getString("max_age")
                            }
                            if (jsonObject.has("min_height") && !jsonObject.getString("min_height")
                                    .isNullOrEmpty()
                            ) {
                                strminheight = jsonObject.getString("min_height")
                            }
                            if (jsonObject.has("max_height") && !jsonObject.getString("max_height")
                                    .isNullOrEmpty()
                            ) {
                                strmaxheight = jsonObject.getString("max_height")
                            }
                            if (jsonObject.has("marital_status") && !jsonObject.getString("marital_status")
                                    .isNullOrEmpty()
                            ) {
                                strmaritalstatus = jsonObject.getString("marital_status")
                            }
//                            if (jsonObject.has("mother_tongue") && !jsonObject.getString("mother_tongue")
//                                    .isNullOrEmpty()
//                            ) {
//                                strmothertongue = jsonObject.getString("mother_tongue")
//                            }
                            if (jsonObject.has("country") && !jsonObject.getString("country")
                                    .isNullOrEmpty()
                            ) {
                                strcountry = jsonObject.getString("country")
                            }
//                            else {
//                                strcountry = jsonUserData.getString("country")
//                                jsonArrayState = db.GetStates(strcountry)!!
//                            }
                            if (jsonObject.has("state") && !jsonObject.getString("state")
                                    .isNullOrEmpty()
                            ) {
                                strstate = jsonObject.getString("state")
                            }
//                            else {
//                                strstate = jsonUserData.getString("state")
//                            }
                            if (jsonObject.has("qualification") && !jsonObject.getString("qualification")
                                    .isNullOrEmpty() && !jsonObject.getString("qualification")
                                    .equals("Select_Qualification")
                            ) {
                                strqualification = jsonObject.getString("qualification")
                            }
                            if (jsonObject.has("currency") && !jsonObject.getString("currency")
                                    .isNullOrEmpty()
                            ) {
                                strcurrency = jsonObject.getString("currency")
                            }
                            if (jsonObject.has("below") && !jsonObject.getString("below")
                                    .isNullOrEmpty()
                            ) {
                                strcurrencybelow = jsonObject.getString("below")
                            }
                            if (jsonObject.has("above") && !jsonObject.getString("above")
                                    .isNullOrEmpty()
                            ) {
                                strcurrencyabove = jsonObject.getString("above")
                            }
                            if (jsonObject.has("whats_your_diet") && !jsonObject.getString("whats_your_diet")
                                    .isNullOrEmpty()
                            ) {
                                strdiet = jsonObject.getString("whats_your_diet")
                            }
                            if (jsonObject.has("profile_created_by") && !jsonObject.getString("profile_created_by")
                                    .isNullOrEmpty()
                            ) {
                                strprofilecreatedby = jsonObject.getString("profile_created_by")

                            }

                            RangeSeekBarAge.setProgress(strminAge.toFloat(), strmaxAge.toFloat())
                            RangeSeekBarHeight.setProgress(
                                strminheight.toFloat(),
                                strmaxheight.toFloat()
                            )
                            tvminage.text = strminAge
                            tvmaxage.text = strmaxAge
                            tvminheight.text = strminheight
                            tvmaxheight.text = strmaxheight
                            var pos = 0
//                            if (SpinnerMaritalStatus != null) {
//                                strmaritalstatus = strmaritalstatus.replace("_", " ")
//                                strmaritalstatus =
//                                    strmaritalstatus.split(" ")
//                                        .joinToString(" ") { it.capitalize() }
//                                pos=
//                                    resources.getStringArray(R.array.MaritulStatus)
//                                        .indexOf(strmaritalstatus)
//                                if (pos > 0) {
//                                    SpinnerMaritalStatus.setSelection(pos)
//                                }
//                            }
                            if (SpinnerMaritalStatus != null) {
                                val adapter = Adapter_Spinner_Pref(
                                    this@Preferences,
                                    resources.getStringArray(R.array.MaritulStatus).toMutableList(),
                                    null
                                )
                                SpinnerMaritalStatus.adapter = adapter
                                if (!strmaritalstatus.isNullOrEmpty()) {
                                    strmaritalstatus = strmaritalstatus?.split(" ")
                                        ?.joinToString(" ") { it.capitalize() }
                                    var pos = resources.getStringArray(R.array.MaritulStatus)
                                        .indexOf(strmaritalstatus)
                                    SpinnerMaritalStatus.setSelection(pos)
                                    strmaritalstatus =
                                        resources.getStringArray(R.array.MaritulStatus).get(pos)
                                }
                            }
                            if (SpinnerCountry != null) {
                                val adapterSpinnerCsc =
                                    Adapter_Spinner_Pref_3(this@Preferences, jsonArrayCountry, null)
                                SpinnerCountry.adapter = adapterSpinnerCsc
                                if (!strcountry.isNullOrEmpty()) {
                                    for (i in 0 until jsonArrayCountry.length()) {
                                        var tmp = jsonArrayCountry.getJSONObject(i).getString("id")
                                        if (strcountry.equals(tmp)) {
                                            SpinnerCountry.setSelection(i)
                                            strcountry = tmp
                                            jsonArrayState = db.GetStates(strcountry)!!
                                        }
                                    }
                                } else {
                                    var jsonObject123 = JSONObject()
                                    jsonObject123.put("id", "")
                                    jsonObject123.put("name", "Select Country")
                                    jsonArrayCountry.put(0, jsonObject123)
                                    val adapterSpinnerCsc =
                                        Adapter_Spinner_Pref_3(
                                            this@Preferences,
                                            jsonArrayCountry,
                                            null
                                        )
                                    SpinnerCountry.adapter = adapterSpinnerCsc
                                }
                            }
                            if (SpinnerState != null) {
                                val adapterSpinnerCsc =
                                    Adapter_Spinner_Pref_3(this@Preferences, jsonArrayState, null)
                                SpinnerState.adapter = adapterSpinnerCsc
                                if (!strstate.isNullOrEmpty()) {
                                    for (i in 0 until jsonArrayState.length()) {
                                        var tmp = jsonArrayState.getJSONObject(i).getString("id")
                                        if (strstate.equals(tmp)) {
                                            SpinnerState.setSelection(i)
                                            strstate = tmp
//                                            jsonArrayState = db.GetStates(strcountry)!!
                                        }
                                    }
                                } else {
                                    var jsonObject123 = JSONObject()
                                    jsonObject123.put("id", "")
                                    jsonObject123.put("name", "Select State")
                                    jsonArrayState.put(0, jsonObject123)
                                    val adapterSpinnerCsc =
                                        Adapter_Spinner_Pref_3(
                                            this@Preferences,
                                            jsonArrayState,
                                            null
                                        )
                                    SpinnerState.adapter = adapterSpinnerCsc
                                }
                            }


//                            strmothertongue = strmothertongue.replace("_", " ")
//                            strmothertongue =
//                                strmothertongue.split(" ").joinToString(" ") { it.capitalize() }
//                            pos =
//                                resources.getStringArray(R.array.Languages).indexOf(strmothertongue)
//                            if (pos > 0) {
//                                SpinnerMotherTongue.setSelection(pos)
//                            }
                            pos = resources.getStringArray(R.array.Qualification)
                                .indexOf(strqualification)
                            if (pos > 0) {
                                SpinnerQualification.setSelection(pos)
                            }
                            if (!strcurrency.equals("0")) {
                                ivdoesntmatterinside.visibility = View.GONE
                                ivspecifyincomeinside.visibility = View.VISIBLE
                                LinearIncomeRange.visibility = View.VISIBLE
                                if (strcurrency.equals("INR")) {
                                    strcurrencybelow = jsonObject.getString("below")
                                    strcurrencyabove = jsonObject.getString("above")
                                    strcurrencybelow =
                                        strcurrencybelow.split(" ")
                                            .joinToString(" ") { it.capitalize() }
                                    strcurrencyabove =
                                        strcurrencyabove.split(" ")
                                            .joinToString(" ") { it.capitalize() }


                                    if (SpinnerCurrency != null) {
                                        val adapter = Adapter_Spinner_Pref(
                                            this@Preferences,
                                            resources.getStringArray(R.array.Currency)
                                                .toMutableList(),
                                            null
                                        )
                                        SpinnerCurrency.adapter = adapter
                                        SpinnerCurrency.setSelection(0)
                                    }
                                    if (SpinnerAbove != null) {
                                        val adapter = Adapter_Spinner_Pref(
                                            this@Preferences,
                                            resources.getStringArray(R.array.CurrencyAboveINR)
                                                .toMutableList(),
                                            null
                                        )
                                        SpinnerAbove.adapter = adapter
                                        pos =
                                            resources.getStringArray(R.array.CurrencyAboveINR)
                                                .indexOf(strcurrencyabove)
                                        if (pos > 0) {
                                            SpinnerAbove.setSelection(pos)
                                        }
                                    }
                                    if (SpinnerBelow != null) {
                                        val adapter = Adapter_Spinner_Pref(
                                            this@Preferences,
                                            resources.getStringArray(R.array.CurrencyBeloveINR)
                                                .toMutableList(),
                                            null
                                        )
                                        SpinnerBelow.adapter = adapter
                                        pos =
                                            resources.getStringArray(R.array.CurrencyBeloveINR)
                                                .indexOf(strcurrencybelow)
                                        if (pos > 0) {
                                            SpinnerBelow.setSelection(pos)
                                        }
                                    }


                                } else {
                                    strcurrencybelow = jsonObject.getString("below")
                                    strcurrencyabove = jsonObject.getString("above")
                                    strcurrencybelow =
                                        strcurrencybelow.split(" ")
                                            .joinToString(" ") { it.capitalize() }
                                    strcurrencyabove =
                                        strcurrencyabove.split(" ")
                                            .joinToString(" ") { it.capitalize() }


                                    if (SpinnerCurrency != null) {
                                        val adapter = Adapter_Spinner_Pref(
                                            this@Preferences,
                                            resources.getStringArray(R.array.Currency)
                                                .toMutableList(),
                                            null
                                        )
                                        SpinnerCurrency.adapter = adapter
                                        SpinnerCurrency.setSelection(1)
                                    }
                                    if (SpinnerAbove != null) {
                                        val adapter = Adapter_Spinner_Pref(
                                            this@Preferences,
                                            resources.getStringArray(R.array.CurrencyAboveUSD)
                                                .toMutableList(),
                                            null
                                        )
                                        SpinnerAbove.adapter = adapter
                                        pos =
                                            resources.getStringArray(R.array.CurrencyAboveUSD)
                                                .indexOf(strcurrencyabove)
                                        if (pos > 0) {
                                            SpinnerAbove.setSelection(pos)
                                        }
                                    }
                                    if (SpinnerBelow != null) {
                                        val adapter = Adapter_Spinner_Pref(
                                            this@Preferences,
                                            resources.getStringArray(R.array.CurrencyBeloveUSD)
                                                .toMutableList(),
                                            null
                                        )
                                        SpinnerBelow.adapter = adapter
                                        pos =
                                            resources.getStringArray(R.array.CurrencyBeloveUSD)
                                                .indexOf(strcurrencybelow)
                                        if (pos > 0) {
                                            SpinnerBelow.setSelection(pos)
                                        }
                                    }

                                }
                            }
                            if (strdiet.equals("0")) {
                                strdiet = "doesn't matter"
                                strdiet =
                                    strdiet.split(" ").joinToString(" ") { it.capitalize() }
                                pos =
                                    resources.getStringArray(R.array.DietPref).indexOf(strdiet)
                                if (pos > 0) {
                                    SpinnerDiet.setSelection(pos)
                                    strdiet = "0"
                                }
                            } else {
                                strdiet = strdiet.replace("_", " ")
                                strdiet =
                                    strdiet.split(" ").joinToString(" ") { it.capitalize() }
                                pos =
                                    resources.getStringArray(R.array.DietPref).indexOf(strdiet)
                                if (pos > 0) {
                                    SpinnerDiet.setSelection(pos)
                                }
                            }
                            if (strprofilecreatedby.equals("0")) {
                                strprofilecreatedby = "doesn't matter"
                                strprofilecreatedby = strprofilecreatedby.split(" ")
                                    .joinToString(" ") { it.capitalize() }
                                pos = resources.getStringArray(R.array.ProfieCreatedByPref)
                                    .indexOf(strprofilecreatedby)
                                if (pos > 0) {
                                    SpinnerProfileCreatedby.setSelection(pos)
                                    strprofilecreatedby = "0"
                                }
                            } else {
                                if (strprofilecreatedby.equals("parent_guardian")) {
                                    strprofilecreatedby = "Parent/Guardian"
                                }
                                strprofilecreatedby = strprofilecreatedby.replace("_", " ")
                                strprofilecreatedby = strprofilecreatedby.split(" ")
                                    .joinToString(" ") { it.capitalize() }
                                pos = resources.getStringArray(R.array.ProfieCreatedByPref)
                                    .indexOf(strprofilecreatedby)
                                if (pos > 0) {
                                    SpinnerProfileCreatedby.setSelection(pos)
                                }
                            }


                            scrollView.visibility = View.VISIBLE

                            progressDialog.dismiss()
                        } else if (status == 404) {
                            scrollView.visibility = View.VISIBLE
                            var jsonObject = JSONObject()
                            jsonObject.put("id", "")
                            jsonObject.put("name", "Select Country")
                            var jsonObject1 = JSONObject()
                            jsonObject1.put("id", "")
                            jsonObject1.put("name", "Select State")
                            jsonArrayCountry.put(0, jsonObject)
                            jsonArrayState.put(0, jsonObject1)
                            if (SpinnerMaritalStatus != null) {
                                val adapter = Adapter_Spinner_Pref(
                                    this@Preferences,
                                    resources.getStringArray(R.array.MaritulStatus).toMutableList(),
                                    null
                                )
                                SpinnerMaritalStatus.adapter = adapter
//                                if (!strmaritalstatus.isNullOrEmpty()) {
//                                    strmaritalstatus = strmaritalstatus?.split(" ")
//                                        ?.joinToString(" ") { it.capitalize() }
//                                    var pos = resources.getStringArray(R.array.MaritulStatus)
//                                        .indexOf(strmaritalstatus)
//                                    SpinnerMaritalStatus.setSelection(pos)
//                                    strmaritalstatus =
//                                        resources.getStringArray(R.array.MaritulStatus).get(pos)
//                                }
                            }
                            if (SpinnerCountry != null) {
                                val adapterSpinnerCsc =
                                    Adapter_Spinner_Pref_3(this@Preferences, jsonArrayCountry, null)
                                SpinnerCountry.adapter = adapterSpinnerCsc
//                                if (!strcountry.isNullOrEmpty()) {
//                                    for (i in 0 until jsonArrayCountry.length()) {
//                                        var tmp = jsonArrayCountry.getJSONObject(i).getString("id")
//                                        if (strcountry.equals(tmp)) {
//                                            SpinnerCountry.setSelection(i)
//                                            strcountry = tmp
//                                            jsonArrayState = db.GetStates(strcountry)!!
//                                        }
//                                    }
//                                }
                            }
                            if (SpinnerState != null) {
                                val adapterSpinnerCsc =
                                    Adapter_Spinner_Pref_3(this@Preferences, jsonArrayState, null)
                                SpinnerState.adapter = adapterSpinnerCsc
//                                if (!strstate.isNullOrEmpty()) {
//                                    for (i in 0 until jsonArrayState.length()) {
//                                        var tmp = jsonArrayState.getJSONObject(i).getString("id")
//                                        if (strstate.equals(tmp)) {
//                                            SpinnerState.setSelection(i)
//                                            strstate = tmp
////                                            jsonArrayState = db.GetStates(strcountry)!!
//                                        }
//                                    }
//                                }
                            }
//
//                            for (i in 0 until jsonArrayCountry.length()) {
//                                var tmp = jsonUserData.getString("country")
//                                var tmpcountry =
//                                    jsonArrayCountry.getJSONObject(i).getString("id")
//                                if (tmpcountry.equals(tmp)) {
//                                    SpinnerCountry.setSelection(i)
//                                    strcountry = tmp
//                                    jsonArrayState = db.GetStates(strcountry)!!
//                                }
//                            }
//                            for (i in 0 until jsonArrayState.length()) {
//                                var tmp1 = jsonUserData.getString("state")
//                                var tmpstate = jsonArrayState.getJSONObject(i).getString("id")
//                                if (tmpstate.equals(tmp1)) {
//                                    SpinnerState.setSelection(i)
//                                    strstate = tmp1
//                                }
//
//                            }
                            progressDialog.dismiss()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        this@Preferences,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        this@Preferences,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(
                        this@Preferences,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            })
    }

    fun SavePreferenceClicked(view: View) {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if (isConnected) {
            if (strdiet.equals("Doesn't Matter")) {
                strdiet = "0"
            }
            if (strprofilecreatedby.equals("Doesn't Matter")) {
                strprofilecreatedby = "0"
            }
            SetPreferenceValues()
        } else {
            Toast.makeText(this, "You Are Not Connected To Network", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun ShowAlertDialogue(message: String) {
        val builder = AlertDialog.Builder(this@Preferences)
        builder.setMessage(message)

        builder.setPositiveButton("Ok") { dialog, which ->
            var intent = Intent(this@Preferences, HomeScreen::class.java)
            intent.putExtra("From", "Preference Success")
            finish()
            startActivity(intent)
        }
        builder.show()
    }
}