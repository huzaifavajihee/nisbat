package com.nisbat.app

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.service.controls.ControlsProviderService.TAG
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Register_Location_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Register_Location_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var strcountry: String = ""
    var strstate: String = ""
    var strcity: String = ""
    var strresidencystatus: String = ""
    var strgrewin: String = ""
    var jsonArrayCountry: JSONArray = JSONArray()
    var jsonArrayState: JSONArray = JSONArray()
    var tempjsonArrayState: JSONArray = JSONArray()
    var jsonArrayCity: JSONArray = JSONArray()
    var tempjsonArrayCity: JSONArray = JSONArray()
    var stringArrayCountry = ArrayList<String>()
    var stringArrayState = ArrayList<String>()
    var stringArrayCity = ArrayList<String>()


    //    lateinit var spinnerCountry: Spinner
//    lateinit var spinnerState: Spinner
//    lateinit var spinnerCity: Spinner

    //    lateinit var spinnergrewin: Spinner
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor
    lateinit var RICountryAutocomplete: AutoCompleteTextView
    lateinit var RIGrewinAutocomplete: AutoCompleteTextView
    lateinit var RIStateAutocomplete: AutoCompleteTextView
    lateinit var RICityAutocomplete: AutoCompleteTextView
    var countriees = ArrayList<String>()
    var states = ArrayList<String>()
    var cities = ArrayList<String>()
    var currentcountrypos = 0
    var currentstatepos = 0
    var currentcitypos = 0
    var currentgrewinpos = 0
    var booleangrewinselected = false
    var booleancountryselected = false
    var booleanstateselected = false
    var booleancityselected = false
    lateinit var db: CSC_Database_Helper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Register_Info).getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            (activity as Register_Info).getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_location_info, container, false)

        val btnnext = view.findViewById<Button>(R.id.btnlocationinfonext)
        val btnback = view.findViewById<Button>(R.id.btnlocationinfoback)
//        spinnerCountry = view.findViewById<Spinner>(R.id.spinnerregistercountrylivingin) as Spinner
//        spinnerState = view.findViewById<Spinner>(R.id.spinnerregisterstatelivingin) as Spinner
//        spinnerCity = view.findViewById<Spinner>(R.id.spinnerregistercitylivingin) as Spinner
        val spinnerresidencystatus = view.findViewById<Spinner>(R.id.spinnerregisterresidencystatus)
//        spinnergrewin = view.findViewById<Spinner>(R.id.spinnerregistergrewupin)
        RICountryAutocomplete = view.findViewById(R.id.RICountryautocomplete)
        RIGrewinAutocomplete = view.findViewById(R.id.RIGrewinautocomplete)
        RIStateAutocomplete = view.findViewById(R.id.RIStateautocomplete)
        RICityAutocomplete = view.findViewById(R.id.RICityautocomplete)
//        val progressDialog = ProgressDialog(activity as Register_Info)
//        progressDialog.setMessage("Please Wait...")
//        progressDialog.setCancelable(false)
//        progressDialog.show()


        db = CSC_Database_Helper(activity as Register_Info)
        jsonArrayCountry = db.GetCountry()!!
        var country = ""
        if (!sharedprefencesReginfo.getString("country", "").isNullOrEmpty()) {
            country =
                sharedprefencesReginfo.getString("country", "").toString()
        } else if (!jsonUserData.getString("living_in").isNullOrEmpty()) {
            country =
                jsonUserData.getString("living_in")
        }
        for (i in 0 until jsonArrayCountry.length()) {
            var Cname = jsonArrayCountry.getJSONObject(i).getString("name")
            countriees.add(Cname)
            try {

                if (jsonArrayCountry.getJSONObject(i).getString(
                        "id"
                    ).equals(country)
                ) {
                    currentcountrypos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptercountry: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Register_Info,
                android.R.layout.simple_list_item_1,
                countriees
            )
        RICountryAutocomplete.setAdapter(adaptercountry)
        if (currentcountrypos >= 0) {
            RICountryAutocomplete.setText(countriees.get(currentcountrypos))
            strcountry = jsonArrayCountry.getJSONObject(currentcountrypos).getString("id")
            //SelectStateStateAndCity(strcountry)
            booleancountryselected = true
        }

        if (!sharedprefencesReginfo.getString("grew_up_in", "").isNullOrEmpty()) {
            country =
                sharedprefencesReginfo.getString("grew_up_in", "").toString()
        } else if (!jsonUserData.getString("living_in").isNullOrEmpty()) {
            country =
                jsonUserData.getString("living_in")
        }
        for (i in 0 until jsonArrayCountry.length()) {
            try {

                if (jsonArrayCountry.getJSONObject(i).getString(
                        "id"
                    ).equals(country)
                ) {
                    currentgrewinpos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptergrewin: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Register_Info,
                android.R.layout.simple_list_item_1,
                countriees
            )
        RIGrewinAutocomplete.setAdapter(adaptergrewin)
        if (currentgrewinpos >= 0) {
            RIGrewinAutocomplete.setText(countriees.get(currentgrewinpos))
            strgrewin = jsonArrayCountry.getJSONObject(currentgrewinpos).getString("id")
            booleangrewinselected = true
        }
        if (spinnerresidencystatus != null) {
            val adapter =
                Adapter_Spinner(
                    (activity as Register_Info), resources.getStringArray(
                        R.array.ResidencyStatus
                    ).toMutableList(), null
                )
            spinnerresidencystatus.adapter = adapter
            if (!sharedprefencesReginfo.getString("residency_status", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("residency_status", "")?.replace("_", " ")
                for (i in 0 until resources.getStringArray(R.array.ResidencyStatus).size) {
                    var tmp = resources.getStringArray(R.array.ResidencyStatus).get(i)
                    if (tmpstr.equals(tmp)) {
                        spinnerresidencystatus.setSelection(i)
                        strresidencystatus =
                            resources.getStringArray(R.array.ResidencyStatus).get(i)
                        editorReginfo.putString("residency_status", strresidencystatus)
                        editorReginfo.commit()
                    }
                }
            }
        }
        RICountryAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var countrypos = 0
                if (countriees.contains(Cname)) {
                    countrypos = countriees.indexOf(Cname)
                }
                if (countrypos >= 0) {
                    strcountry = jsonArrayCountry.getJSONObject(countrypos).getString("id")
                    booleancountryselected = true
                    SelectStateStateAndCity(strcountry)
                } else {
                    strcountry = "0"
                }
            }

        })

        var accountrycount = 0
        RICountryAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                accountrycount = count
                if (count > 0) {
                    booleancountryselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                accountrycount = s?.length!!
            }
        })

        RICountryAutocomplete.setOnDismissListener {

            if (accountrycount > 0) {
                if (!booleancountryselected) {
                    RICountryAutocomplete.requestFocus()
                    RICountryAutocomplete.showDropDown()
                }
            }
        }
        RIGrewinAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var countrypos = 0
                if (countriees.contains(Cname)) {
                    countrypos = countriees.indexOf(Cname)
                }
                if (countrypos >= 0) {
                    strgrewin = jsonArrayCountry.getJSONObject(countrypos).getString("id")
                    booleangrewinselected = true
                } else {
                    strgrewin = "0"
                }
            }

        })

        var acgrewincount = 0
        RIGrewinAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                acgrewincount = count
                if (count > 0) {
                    booleangrewinselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                acgrewincount = s?.length!!
            }
        })

        RIGrewinAutocomplete.setOnDismissListener {

            if (acgrewincount > 0) {
                if (!booleangrewinselected) {
                    RIGrewinAutocomplete.requestFocus()
                    RIGrewinAutocomplete.showDropDown()
                }
            }
        }

        RIStateAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var statepos = 0
                if (states.contains(Cname)) {
                    statepos = states.indexOf(Cname)
                }
                if (statepos >= 0) {
                    strstate = jsonArrayState.getJSONObject(statepos).getString("id")
                    booleanstateselected = true
                    SetCityValue(strstate)
                } else {
                    strstate = "0"
                }
            }

        })

        var acstatecount = 0
        RIStateAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                accountrycount = count
                if (count > 0) {
                    booleanstateselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                acstatecount = s?.length!!
            }
        })

        RIStateAutocomplete.setOnDismissListener {

            if (acstatecount > 0) {
                if (!booleanstateselected) {
                    RIStateAutocomplete.requestFocus()
                    RIStateAutocomplete.showDropDown()
                }
            }
        }

        RICityAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var citypos = 0
                if (cities.contains(Cname)) {
                    citypos = cities.indexOf(Cname)
                }
                if (citypos >= 0) {
                    strcity = jsonArrayCity.getJSONObject(citypos).getString("id")
                    booleancityselected = true
                } else {
                    strcity = "0"
                }
            }

        })

        var accitycount = 0
        RICityAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                accitycount = count
                if (count > 0) {
                    booleancityselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                accitycount = s?.length!!
            }
        })

        RICityAutocomplete.setOnDismissListener {

            if (accitycount > 0) {
                if (!booleancityselected) {
                    RICityAutocomplete.requestFocus()
                    RICityAutocomplete.showDropDown()
                }
            }
        }


//        //spinner item click
        spinnerresidencystatus.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        strresidencystatus = resources.getStringArray(R.array.ResidencyStatus).get(
                            position
                        )
                    } else {
                        strresidencystatus = ""
                    }
                    val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                    if (textView != null) {
                        textView.setTextColor(resources.getColor(R.color.color_white))
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }
            }

        // button back clicked
        btnback.setOnClickListener {
            editorReginfo.putString("country", strcountry)
            editorReginfo.putString("state", strstate)
            editorReginfo.putString("city", strcity)
            editorReginfo.putString("residency_status", strresidencystatus)
            editorReginfo.putString("grew_up_in", strgrewin)
            editorReginfo.commit()
            (activity as Register_Info).ShowCareerInfo()
        }        // Inflate the layout for this fragment

        // button next click
        btnnext.setOnClickListener {

            if (!strcountry.isNullOrEmpty() && !strstate.isNullOrEmpty() && !strcity.isNullOrEmpty()) {

                editorReginfo.putString("country", strcountry)
                editorReginfo.putString("state", strstate)
                editorReginfo.putString("city", strcity)
                editorReginfo.putString("residency_status", strresidencystatus.replace(" ", "_"))
                editorReginfo.putString("grew_up_in", strgrewin)
                editorReginfo.commit()
                (activity as Register_Info).ShowFamilyInfo()
            } else {
                if (strcountry.isNullOrEmpty()) {
                    Toast.makeText(
                        (activity as Register_Info),
                        "Please select\"Country\"",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (strstate.isNullOrEmpty()) {
                    Toast.makeText(
                        (activity as Register_Info),
                        "Please select \"State\"",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (strcity.isNullOrEmpty()) {
                    Toast.makeText(
                        (activity as Register_Info),
                        "Please select \"City\"",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
        return view
    }

    private fun SelectStateStateAndCity2(strcountry: String) {
        states = ArrayList<String>()
        currentstatepos = 0
        jsonArrayState = db.GetStates(strcountry)!!
        var state = ""
        if (!sharedprefencesReginfo.getString("state", "").isNullOrEmpty()) {
            state =
                sharedprefencesReginfo.getString("state", "").toString()
        } else if (!jsonUserData.getString("state").isNullOrEmpty()) {
            state =
                jsonUserData.getString("state")
        }
        for (i in 0 until jsonArrayState.length()) {
            var Cname = jsonArrayState.getJSONObject(i).getString("name")
            states.add(Cname)
            try {

                if (jsonArrayState.getJSONObject(i).getString(
                        "id"
                    ).equals(state)
                ) {
                    currentstatepos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adapterstate: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Register_Info,
                android.R.layout.simple_list_item_1,
                states
            )
        RIStateAutocomplete.setAdapter(adapterstate)
        if (currentstatepos >= 0) {
//            RIStateAutocomplete.setText(states.get(currentstatepos))
            strstate = jsonArrayState.getJSONObject(currentstatepos).getString("id")
            booleanstateselected = true
        }
        SetCityValue2(strstate)
    }

    private fun SetCityValue2(strstate: String) {
        currentcitypos = 0
        cities = ArrayList<String>()
        jsonArrayCity = db.GetCities(strstate)!!
        var city = ""
        if (!sharedprefencesReginfo.getString("city", "").isNullOrEmpty()) {
            city =
                sharedprefencesReginfo.getString("city", "").toString()
        } else if (!jsonUserData.getString("city").isNullOrEmpty()) {
            city =
                jsonUserData.getString("city")
        }
        for (i in 0 until jsonArrayCity.length()) {
            var Cname = jsonArrayCity.getJSONObject(i).getString("name")
            cities.add(Cname)
            try {

                if (jsonArrayCity.getJSONObject(i).getString(
                        "id"
                    ).equals(city)
                ) {
                    currentcitypos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptercity: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Register_Info,
                android.R.layout.simple_list_item_1,
                cities
            )
        RICityAutocomplete.setAdapter(adaptercity)
        if (currentcitypos >= 0) {
//            RICityAutocomplete.setText(cities.get(currentcitypos))
            strcity = jsonArrayCity.getJSONObject(currentcitypos).getString("id")
            booleancityselected = true
        }
    }

    private fun SelectStateStateAndCity(strcountry: String) {
        states = ArrayList<String>()
        currentstatepos = 0
        jsonArrayState = db.GetStates(strcountry)!!
        var state = ""
        if (!sharedprefencesReginfo.getString("state", "").isNullOrEmpty()) {
            state =
                sharedprefencesReginfo.getString("state", "").toString()
        } else if (!jsonUserData.getString("state").isNullOrEmpty()) {
            state =
                jsonUserData.getString("state")
        }
        for (i in 0 until jsonArrayState.length()) {
            var Cname = jsonArrayState.getJSONObject(i).getString("name")
            states.add(Cname)
            try {

                if (jsonArrayState.getJSONObject(i).getString(
                        "id"
                    ).equals(state)
                ) {
                    currentstatepos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adapterstate: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Register_Info,
                android.R.layout.simple_list_item_1,
                states
            )
        RIStateAutocomplete.setAdapter(adapterstate)
        if (currentstatepos >= 0) {
            RIStateAutocomplete.setText(states.get(currentstatepos))
            strstate = jsonArrayState.getJSONObject(currentstatepos).getString("id")
            booleanstateselected = true
        }
        SetCityValue(strstate)


    }

    private fun SetCityValue(strstate: String) {
        currentcitypos = 0
        cities = ArrayList<String>()
        jsonArrayCity = db.GetCities(strstate)!!
        var city = ""
        if (!sharedprefencesReginfo.getString("city", "").isNullOrEmpty()) {
            city =
                sharedprefencesReginfo.getString("city", "").toString()
        } else if (!jsonUserData.getString("city").isNullOrEmpty()) {
            city =
                jsonUserData.getString("city")
        }
        for (i in 0 until jsonArrayCity.length()) {
            var Cname = jsonArrayCity.getJSONObject(i).getString("name")
            cities.add(Cname)
            try {

                if (jsonArrayCity.getJSONObject(i).getString(
                        "id"
                    ).equals(city)
                ) {
                    currentcitypos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptercity: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Register_Info,
                android.R.layout.simple_list_item_1,
                cities
            )
        RICityAutocomplete.setAdapter(adaptercity)
        if (currentcitypos >= 0) {
            RICityAutocomplete.setText(cities.get(currentcitypos))
            strcity = jsonArrayCity.getJSONObject(currentcitypos).getString("id")
            booleancityselected = true
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Register_Location_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Register_Location_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
