package com.nisbat.app

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.security.AccessController.getContext
import java.util.*

class AboutProfile : AppCompatActivity() {

    var strarrayCategories = arrayOf<String>(
        "About",
        "Contact",
        "Basic Info",
        "Family Info",
        "Education"
    )
    val manager = supportFragmentManager
    lateinit var jsonUserData: JSONObject
    lateinit var sharedPreferencesLogin: SharedPreferences
    lateinit var usernameage: TextView
    lateinit var useraddress: TextView
    lateinit var userprofession: TextView
    lateinit var ivimage: ImageView
    var jsonarrProfilePic = JSONArray()
    var currentimagepos: Int = 0
    lateinit var tvimagecount: TextView
    var jsonOwndata = JSONObject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_profile)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
            window.statusBarColor = resources.getColor(R.color.color_white)
        }

        sharedPreferencesLogin =
            this.getSharedPreferences("pref_login_user", 0)
        jsonOwndata = JSONObject(sharedPreferencesLogin.getString("User_Data", ""))

        val data = JSONObject(intent.getStringExtra("Data"))
        var its_number = ""
        if (data.has("from_its_card_number")) {
            its_number = data.getString("from_its_card_number")
        } else if (data.has("its_card_number")) {
            its_number = data.getString("its_card_number")
        }

        GetUserData(its_number)


        val btnabout = findViewById<Button>(R.id.btnhorirecycerviewabout)
        val btncontact = findViewById<Button>(R.id.btnhorirecycerviewcontact)
        val btnbasicinfo = findViewById<Button>(R.id.btnhorirecyclerviewbasicinfo)
        val btnfamilyinfo = findViewById<Button>(R.id.btnhorirecycerviewfamilyinfo)
        val btneducation = findViewById<Button>(R.id.btnhorirecycervieweducation)
        val ibtnback = findViewById<ImageButton>(R.id.ibtnaboutprofileback)
        usernameage = findViewById<TextView>(R.id.tvmymatchelistitemnameandage)
        userprofession = findViewById<TextView>(R.id.tvmymatchelistitemprofession)
        useraddress = findViewById<TextView>(R.id.tvmymatchelistitemaddress)
        val ibtnpreviusimage = findViewById<ImageButton>(R.id.ibtnaboutprofileprevios)
        val ibtnnextimage = findViewById<ImageButton>(R.id.ibtnaboutprofilenext)
        val btnconnectnow = findViewById<Button>(R.id.btnmymatchesProfileconnectnow)
        ivimage = findViewById(R.id.ivaboutprofile)
        tvimagecount = findViewById(R.id.tvaboutprofileimagecount)
        val ibtnmenu = findViewById<ImageView>(R.id.ibtnaboutprofileimagemenu)


        ibtnnextimage.setOnClickListener {
            val length = jsonarrProfilePic.length()
            if (length > 0) {
              if (currentimagepos + 1 <= length - 1) {
                    currentimagepos = currentimagepos + 1
                    if (jsonOwndata.getString("gender").equals("male")) {
                        Picasso.get()
                            .load(
                                jsonarrProfilePic.getJSONObject(currentimagepos)
                                    .getString("profile_picture").replace("http:","https:")
                            )
                            .placeholder(R.drawable.placeholder_female)
                            .error(R.drawable.placeholder_female)
                            .fit()
                            .centerCrop()
                            .into(ivimage)
                    } else {
                        Picasso.get()
                            .load(
                                jsonarrProfilePic.getJSONObject(currentimagepos)
                                    .getString("profile_picture").replace("http:","https:")
                            )
                            .placeholder(R.drawable.placeholder_male)
                            .error(R.drawable.placeholder_male)
                            .fit()
                            .centerCrop()
                            .into(ivimage)
                    }

                }
            }
        }
        ibtnpreviusimage.setOnClickListener {
            val length = jsonarrProfilePic.length()
            if (length > 0) {
                if (currentimagepos - 1 >= 0) {
                    currentimagepos = currentimagepos - 1
                    if (jsonOwndata.getString("gender").equals("male")) {
                        Picasso.get()
                            .load(
                                jsonarrProfilePic.getJSONObject(currentimagepos)
                                    .getString("profile_picture").replace("http:","https:")
                            )
                            .placeholder(R.drawable.placeholder_female)
                            .error(R.drawable.placeholder_female)
                            .fit()
                            .centerCrop()
                            .into(ivimage)
                    } else {
                        Picasso.get()
                            .load(
                                jsonarrProfilePic.getJSONObject(currentimagepos)
                                    .getString("profile_picture")
                            )
                            .placeholder(R.drawable.placeholder_male)
                            .error(R.drawable.placeholder_male)
                            .fit()
                            .centerCrop()
                            .into(ivimage)
                    }
                }
            }
        }
        //Loading First Fragment Bydefault
        btnabout.isSelected = true

        btnconnectnow.setOnClickListener {
            CallConnectNow(jsonUserData)
        }

        ibtnmenu.setOnClickListener {
            val popup = PopupMenu(this@AboutProfile, ibtnmenu)
            val inflater = popup.menuInflater
            inflater.inflate(R.menu.popupmenu, popup.menu)
            popup.show()

            popup.setOnMenuItemClickListener {
                if (it.itemId.equals(R.id.ReportMenuItem)) {

                    ReportUser(jsonUserData.getString("its_card_number"))

                }
                return@setOnMenuItemClickListener true
            }
        }


        //Button click events
        btnabout.setOnClickListener {
            btnabout.isSelected = true
            btncontact.isSelected = false
            btnbasicinfo.isSelected = false
            btnfamilyinfo.isSelected = false
            btneducation.isSelected = false
            val transaction = manager.beginTransaction()
            val fragment = Fragment_About_About_Profile()
            transaction.replace(R.id.frameaboutprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        btncontact.setOnClickListener {
            btnabout.isSelected = false
            btncontact.isSelected = true
            btnbasicinfo.isSelected = false
            btnfamilyinfo.isSelected = false
            btneducation.isSelected = false
            val transaction = manager.beginTransaction()
            val fragment = Fragment_Contact_About_Profile()
            transaction.replace(R.id.frameaboutprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        btnbasicinfo.setOnClickListener {
            btnabout.isSelected = false
            btncontact.isSelected = false
            btnbasicinfo.isSelected = true
            btnfamilyinfo.isSelected = false
            btneducation.isSelected = false
            val transaction = manager.beginTransaction()
            val fragment = Fragment_About_Profile_Basic_Info()
            transaction.replace(R.id.frameaboutprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        btnfamilyinfo.setOnClickListener {
            btnabout.isSelected = false
            btncontact.isSelected = false
            btnbasicinfo.isSelected = false
            btnfamilyinfo.isSelected = true
            btneducation.isSelected = false
            val transaction = manager.beginTransaction()
            val fragment = Fragment_Family_About_Profile()
            transaction.replace(R.id.frameaboutprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        btneducation.setOnClickListener {
            btnabout.isSelected = false
            btncontact.isSelected = false
            btnbasicinfo.isSelected = false
            btnfamilyinfo.isSelected = false
            btneducation.isSelected = true
            val transaction = manager.beginTransaction()
            val fragment = Fragment_Education_About_Profile()
            transaction.replace(R.id.frameaboutprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        ibtnback.setOnClickListener {
            finish()
        }
    }

    private fun ReportUser(string: String) {
        var loginPreferences: SharedPreferences =
            this@AboutProfile.getSharedPreferences("pref_login_user", 0)
        var id = jsonOwndata.getString("id")
        val progressDialog = ProgressDialog(this@AboutProfile)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "parent_id",
            id
        )
        requestParams.add(
            "target_id",
            string
        )
        requestParams.add(
            "text",
            "Sorry For That !"
        )
        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            resources.getString(R.string.Api2) + "user/report-user",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    private fun GetUserData(itsNumber: String) {
        val progressDialog = ProgressDialog(this@AboutProfile)
//        progressDialog.setTitle("Update")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            itsNumber
        )
        requestParams.add("status", "1")
        val asyncHttpClient = AsyncHttpClient()

        val token_type = sharedPreferencesLogin.getString("token_type", "")
        val access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/get-user-profile",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        AddToRecentlyVisitors(itsNumber)

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            jsonUserData = response.getJSONObject("data")

                            if (!jsonUserData.getString("dob").isNullOrEmpty()) {
                                var dob = jsonUserData.getString("dob").split("-")
                                var age = getAge(dob[0].toInt(), dob[1].toInt(), dob[2].toInt())
                                var name =
                                    jsonUserData.getString("first_name") + " " + jsonUserData.getString(
                                        "last_name"
                                    )
                                usernameage.setText(name + ", " + age)
                            }
                            userprofession.setText(jsonUserData.getString("occupation"))
                            var address = ""
                            if (jsonUserData.getString("family_location").isNullOrEmpty()) {
                                address = address + jsonUserData.getString("family_location")
                            }
                            if (!jsonUserData.getString("city").isNullOrEmpty()) {
                                if (!address.isNullOrEmpty()) {
                                    address = address + ", " + jsonUserData.getString("city")
                                } else {
                                    address = address + jsonUserData.getString("city")
                                }
                            }
                            if (!jsonUserData.getString("image_count").isNullOrEmpty()) {
                                tvimagecount.setText(jsonUserData.getString("image_count"))
                            } else {
                                tvimagecount.setText("0")
                            }
                            if (!jsonUserData.getString("state").isNullOrEmpty()) {
                                if (!address.isNullOrEmpty()) {
                                    address = address + ", " + jsonUserData.getString("state")
                                } else {
                                    address = address + jsonUserData.getString("state")
                                }
                            }
                            if (!jsonUserData.getString("country").isNullOrEmpty()) {
                                if (!address.isNullOrEmpty()) {
                                    address = address + ", " + jsonUserData.getString("country")
                                } else {
                                    address = address + jsonUserData.getString("country")
                                }
                            }
                            useraddress.setText(address)
                            if (!jsonUserData.getString("profilePicture").isNullOrEmpty()) {
                                jsonarrProfilePic =
                                    JSONArray(jsonUserData.getString("profilePicture"))
                                if (jsonOwndata.getString("gender").equals("male")) {
                                    Picasso.get()
                                        .load(
                                            jsonarrProfilePic.getJSONObject(0)
                                                .getString("profile_picture").replace("http:","https:")
                                        )
                                        .placeholder(R.drawable.placeholder_female)
                                        .error(R.drawable.placeholder_female)
                                        .fit()
                                        .centerCrop()
                                        .into(ivimage)
                                } else {
                                    Picasso.get()
                                        .load(
                                            jsonarrProfilePic.getJSONObject(0)
                                                .getString("profile_picture").replace("http:","https:")
                                        )
                                        .placeholder(R.drawable.placeholder_male)
                                        .error(R.drawable.placeholder_male)
                                        .fit()
                                        .centerCrop()
                                        .into(ivimage)
                                }
                                currentimagepos = 0
                            } else {
                                if (jsonOwndata.getString("gender").equals("male")) {
                                    ivimage.setImageResource(R.drawable.placeholder_female)
//                                    Picasso.get()
//                                        .load(R.drawable.placeholder_female)
//                                        .placeholder(R.drawable.placeholder_female)
//                                        .error(R.drawable.placeholder_female)
//                                        .fit()
//                                        .centerCrop()
//                                        .into(ivimage)
                                } else {
                                    ivimage.setImageResource(R.drawable.placeholder_male)
//                                    Picasso.get()
//                                        .load(R.drawable.placeholder_male)
//                                        .placeholder(R.drawable.placeholder_male)
//                                        .error(R.drawable.placeholder_male)
//                                        .fit()
//                                        .centerCrop()
//                                        .into(ivimage)
                                }

                            }

                            val transaction = manager.beginTransaction()
                            val fragment = Fragment_About_About_Profile()
                            transaction.replace(R.id.frameaboutprofilefragment, fragment)
                            transaction.addToBackStack(null)
                            transaction.commit()
                            progressDialog.dismiss()
                        } else if (status == 401) {

                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {

                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })

    }

    private fun AddToRecentlyVisitors(itsNumber: String) {
        val requestParams = RequestParams()
        requestParams.add(
            "target_its_card_number",
            itsNumber
        )
        requestParams.add(
            "viewer_its_card_number",
            jsonOwndata.getString("its_card_number")
        )
        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/recently-visitors",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {


                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                        } else if (status == 401) {

                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {

                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
//                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
//                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
//                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })


    }

    fun getAge(_year: Int, _month: Int, _day: Int): Int {
        val cal = GregorianCalendar()
        val y: Int
        val m: Int
        val d: Int
        var a: Int
        y = cal[Calendar.YEAR]
        m = cal[Calendar.MONTH]
        d = cal[Calendar.DAY_OF_MONTH]
        cal[_year, _month] = _day
        a = y - cal[Calendar.YEAR]
        if (m < cal[Calendar.MONTH]
            || m == cal[Calendar.MONTH] && d < cal[Calendar.DAY_OF_MONTH]
        ) {
            --a
        }
        require(a >= 0) { "Age < 0" }
        return a
    }

    private fun CallConnectNow(jsonObject: JSONObject?) {
        var loginPreferences: SharedPreferences =
            this@AboutProfile.getSharedPreferences("pref_login_user", 0)
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))
        var oppItsNumber = jsonObject?.getString("its_card_number")
        var ownItsNumber = User_Data.getString("its_card_number")
        var type = "pending"
        var actionType = "sent_request"
        val progressDialog = ProgressDialog(this@AboutProfile)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "receiver_its_card_number",
            oppItsNumber
        )
        requestParams.add("sender_its_card_number", ownItsNumber)
        requestParams.add("type", type)
        requestParams.add("action_type", actionType)

        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            this@AboutProfile.getString(R.string.Api2) + "user/activity-type-update",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
//                            fragment_Home.GetMatches()
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                this@AboutProfile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@AboutProfile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    override fun onBackPressed() {
        finish()
    }
}