package com.nisbat.app;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

class Adapter_Image extends BaseAdapter {

    Context mComtext;
    JSONArray Images;
    String interestedin;
    SharedPreferences AppPref;
    Fragment_Edit_Images fragment;

    public Adapter_Image(Context context, JSONArray ImageList, Fragment_Edit_Images fragment_edit_images) {

        mComtext = context;
        Images = ImageList;
        fragment = fragment_edit_images;
    }

    @Override
    public int getCount() {
        return Images.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String ImageUrl = "";


        convertView = LayoutInflater.from(mComtext).inflate(R.layout.item_image_adapter, parent, false);
        ImageView imageView = convertView.findViewById(R.id.ivimageadapter);
        ImageView imageView1 = convertView.findViewById(R.id.ivgriditemdelete);
        imageView1.setTag(position);
        imageView.setTag((position));
        try {
            ImageUrl = Images.getJSONObject(position).getString("profile_picture");
            if (ImageUrl.equals("abc.jpg")) {
                imageView1.setVisibility(View.INVISIBLE);
               imageView.setBackgroundResource(R.drawable.no_image_placeholder);
            } else {
                imageView1.setVisibility(View.VISIBLE);
                ImageUrl=ImageUrl.replace("http:","https:");
                Picasso.get()
                        .load(ImageUrl)
                        .placeholder(R.drawable.icon_logo_grey)
                        .error(R.drawable.icon_logo_grey)
                        .transform(new CircleTransform((int) mComtext.getResources().getDimension(R.dimen._12sdp), 0))
                        .fit()
                        .centerCrop()
                        .into(imageView);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String finalImageUrl = ImageUrl;
        imageView1.setOnClickListener(new View.OnClickListener() {
            private Object Fragment;

            @Override
            public void onClick(View view) {
                int pos = Integer.parseInt(String.valueOf(view.getTag()));

                new AlertDialog.Builder(mComtext)
//                        .setTitle("Confirm?")
                        .setMessage("Are you sure you want to delete photo?")
                        .setPositiveButton("Confirm",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(11)
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            if (!Images.getJSONObject(pos).getString("id").equals("0")) {
                                                String ImageID = Images.getJSONObject(pos).getString("id");
                                                String Url[] = Images.getJSONObject(pos).getString("profile_picture").split("/");
                                                String filename = Url[Url.length - 1];
                                                fragment.DeleteImage(ImageID, filename, pos);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        }).show();

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = Integer.parseInt(String.valueOf(view.getTag()));
                try {
                    if (Images.getJSONObject((Integer) imageView.getTag()).getString("profile_picture").equals("abc.jpg")) {
                        fragment.SelectImageFromGallery();
                    }else
                    {
                        fragment.ShowImageInFullScreen((Integer) imageView.getTag()+1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        return convertView;
    }


}
