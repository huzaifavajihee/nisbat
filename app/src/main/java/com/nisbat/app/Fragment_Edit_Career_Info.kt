package com.nisbat.app

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Career_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Career_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var strincome: String = ""
    var strqualification: String = ""

    lateinit var etqualification: EditText
    lateinit var etcollegename: EditText
    lateinit var etoccupation: EditText
    lateinit var spinnerqualification: Spinner
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_career_info, container, false)
        // Inflate the layout for this fragment

        spinnerqualification =
            view.findViewById<Spinner>(R.id.spinnereditbasicinfohighestqualification)
        etcollegename = view.findViewById<EditText>(R.id.eteditcollageschool)
        etoccupation = view.findViewById<EditText>(R.id.eteditbusiness)
        val spinnerincome = view.findViewById<Spinner>(R.id.spinnereditannualincome)

        (activity as Edit_Profile).fragmentEditCarrerInfo = this@Fragment_Edit_Career_Info

        val tempstr1 = jsonUserData.getString("college_name")
        etcollegename.setText(tempstr1)

        val tempstr2 = jsonUserData.getString("occupation")
        etoccupation.setText(tempstr2)

        if (spinnerincome != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.AnnualIncome).toMutableList(), null
            )
            spinnerincome.adapter = adapter
            var tmpstr = jsonUserData.getString("annual_income")?.replace("_", " ")
                ?.toLowerCase()
            if (!tmpstr.isNullOrEmpty())
            {
            for (i in 0 until resources.getStringArray(R.array.AnnualIncome).size) {
                var tmp = resources.getStringArray(R.array.AnnualIncome).get(i).toLowerCase()
                if (tmpstr.equals(tmp)) {
                    spinnerincome.setSelection(i)
                    strincome = resources.getStringArray(R.array.AnnualIncome).get(i)
                }
            }}
        }
        if (spinnerqualification != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.Qualification).toMutableList(), null
            )
            spinnerqualification.adapter = adapter
            var tmpstr = jsonUserData.getString("qualification")?.replace("_", " ")
                ?.toLowerCase()
            for (i in 0 until resources.getStringArray(R.array.Qualification).size) {
                var tmp = resources.getStringArray(R.array.Qualification).get(i).toLowerCase()
                if (tmpstr.equals(tmp)) {
                    spinnerqualification.setSelection(i)
                    strqualification = resources.getStringArray(R.array.Qualification).get(i)
                }
            }
        }

        //spinner item click
        spinnerincome.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strincome = resources.getStringArray(R.array.AnnualIncome).get(position)
                } else {
                    strincome = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerqualification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0 && position != 1 && position != 9 && position != 23 && position != 33 && position != 42 && position != 67 && position != 74
                    && position != 79 && position != 82 && position != 89
                ) {
                    strqualification = resources.getStringArray(R.array.Qualification).get(position)
                } else {
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Please select sub category from list",
                        Toast.LENGTH_SHORT
                    ).show()
                    strqualification = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Career_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Career_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}