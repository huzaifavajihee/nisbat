package com.nisbat.app

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Your_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Your_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var etyourinfo:EditText
    lateinit var sharedPrefencesLogin:SharedPreferences
    lateinit var jsonUserData:JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin=
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData= JSONObject(sharedPrefencesLogin.getString("User_Data",""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_edit_your_info, container, false)
        // Inflate the layout for this fragment

        (activity as Edit_Profile).fragmentEditYourInfo = this@Fragment_Edit_Your_Info

        etyourinfo = view.findViewById<EditText>(R.id.etedityourinfoaboutyourself)

        if (!jsonUserData.getString("about_me").equals(""))
        {
            etyourinfo.setText(jsonUserData.getString("about_me"))
        }
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Your_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Your_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}