package com.nisbat.app

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.service.controls.ControlsProviderService.TAG
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.getSystemService
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Location_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Location_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var strcountry: String = ""
    var strstate: String = ""
    var strcity: String = ""
    var strresidencystatus: String = ""
    var strgrewin: String = ""
    var jsonArrayCountry: JSONArray = JSONArray()
    var jsonArrayState: JSONArray = JSONArray()
    var jsonArrayCity: JSONArray = JSONArray()
    var stringArrayCountry = ArrayList<String>()
    var stringArrayState = ArrayList<String>()
    var stringArrayCity = ArrayList<String>()
    var tempjsonArrayState: JSONArray = JSONArray()
    var tempjsonArrayCity: JSONArray = JSONArray()


    //    lateinit var spinnerCountry: Spinner
//    lateinit var spinnerState: Spinner
//    lateinit var spinnerCity: Spinner
//    lateinit var spinnergrewin: Spinner
    lateinit var EICountryAutocomplete: AutoCompleteTextView
    lateinit var EIGrewinAutocomplete: AutoCompleteTextView
    lateinit var EIStateAutocomplete: AutoCompleteTextView
    lateinit var EICityAutocomplete: AutoCompleteTextView
    var countriees = ArrayList<String>()
    var states = ArrayList<String>()
    var cities = ArrayList<String>()
    var currentcountrypos = 0
    var currentstatepos = 0
    var currentcitypos = 0
    var currentgrewinpos = 0
    var booleangrewinselected = false
    var booleancountryselected = false
    var booleanstateselected = false
    var booleancityselected = false
    lateinit var db: CSC_Database_Helper
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
        progressDialog = ProgressDialog(activity as Edit_Profile)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_location_info, container, false)
        // Inflate the layout for this fragment

//        spinnerCountry = view.findViewById<Spinner>(R.id.spinnereditcountrylivingin) as Spinner
//        spinnerState = view.findViewById<Spinner>(R.id.spinnereditstatelivingin) as Spinner
//        spinnerCity = view.findViewById<Spinner>(R.id.spinnereditcitylivingin) as Spinner
        EICountryAutocomplete = view.findViewById(R.id.EICountryautocomplete)
        EIGrewinAutocomplete = view.findViewById(R.id.EIGrewinautocomplete)
        EIStateAutocomplete = view.findViewById(R.id.EIStateautocomplete)
        EICityAutocomplete=view.findViewById(R.id.EICityautocomplete)
        val spinnerresidencystatus = view.findViewById<Spinner>(R.id.spinnereditresidencystatus)
//        spinnergrewin = view.findViewById<Spinner>(R.id.spinnereditgrewupin)

        (activity as Edit_Profile).fragmentEditLocationInfo = this@Fragment_Edit_Location_Info

        db = CSC_Database_Helper(activity as Edit_Profile)
        jsonArrayCountry = db.GetCountry()!!
        var tmpcountry =
            jsonUserData.getString("country")
        for (i in 0 until jsonArrayCountry.length()) {
            var Cname = jsonArrayCountry.getJSONObject(i).getString("name")
            countriees.add(Cname)
            try {

                if (jsonArrayCountry.getJSONObject(i).getString(
                        "id"
                    ).equals(tmpcountry)
                ) {
                    currentcountrypos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptercountry: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Edit_Profile,
                android.R.layout.simple_list_item_1,
                countriees
            )
        EICountryAutocomplete.setAdapter(adaptercountry)
        if (currentcountrypos >= 0) {
            EICountryAutocomplete.setText(countriees.get(currentcountrypos))
            strcountry = jsonArrayCountry.getJSONObject(currentcountrypos).getString("id")
            SelectStateStateAndCity(strcountry)
            booleancountryselected = true
        }

        var tmpgrewin =
            jsonUserData.getString("grew_up_in")
        countriees=ArrayList<String>()
        for (i in 0 until jsonArrayCountry.length()) {
            var Cname = jsonArrayCountry.getJSONObject(i).getString("name")
            countriees.add(Cname)
            try {

                if (jsonArrayCountry.getJSONObject(i).getString(
                        "id"
                    ).equals(tmpgrewin)
                ) {
                    currentgrewinpos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptergrewin: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Edit_Profile,
                android.R.layout.simple_list_item_1,
                countriees
            )
        EICountryAutocomplete.setAdapter(adaptergrewin)
        if (currentgrewinpos >= 0) {
            EIGrewinAutocomplete.setText(countriees.get(currentgrewinpos))
            strgrewin = jsonArrayCountry.getJSONObject(currentgrewinpos).getString("id")
            booleangrewinselected = true
        }
//        if (spinnerCountry != null) {
//            val adapterSpinnerCsc = Adapter_Spinner_CSC(activity, jsonArrayCountry, null)
//            spinnerCountry.adapter = adapterSpinnerCsc
//            if (!jsonUserData.getString("country").isNullOrEmpty()) {
//                var tmpstr =
//                    jsonUserData.getString("country")
//                for (i in 0 until jsonArrayCountry.length()) {
//                    var tmp = jsonArrayCountry.getJSONObject(i).getString("id")
//                    if (tmpstr.equals(tmp)) {
//                        spinnerCountry.setSelection(i)
//                        strcountry = tmp
////                        jsonArrayState = db.GetStates(strcountry)!!
////                        SetStatesAsSelected(strcountry)
//                    }
//                }
//            }
//            val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//            if (textView != null) {
//                textView.setTextColor(resources.getColor(R.color.color_white))
//            }
//        }
//        if (spinnergrewin != null) {
//            val adapterSpinnerCsc = Adapter_Spinner_CSC(activity, jsonArrayCountry, null)
//            spinnergrewin.adapter = adapterSpinnerCsc
//            for (i in 0 until jsonArrayCountry.length()) {
//                val currentid: String =
//                    jsonArrayCountry.getJSONObject(i).getString("id")
//                val grewupin: String =
//                    jsonUserData.getString("grew_up_in").toString()
//                if (currentid.equals(grewupin)) {
//                    spinnergrewin.setSelection(i)
//                    strgrewin = currentid
//                }
//            }
//            val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//            if (textView != null) {
//                textView.setTextColor(resources.getColor(R.color.color_white))
//            }
//        }
        if (spinnerresidencystatus != null) {
            val adapter =
                Adapter_Spinner(
                    (activity as Edit_Profile), resources.getStringArray(
                        R.array.ResidencyStatus
                    ).toMutableList(), null
                )
            spinnerresidencystatus.adapter = adapter
            var tmpstr = jsonUserData.getString("residency_status")?.replace("_", " ")
            if (!tmpstr.isNullOrEmpty()) {
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.ResidencyStatus).indexOf(tmpstr)
                spinnerresidencystatus.setSelection(pos)
                strresidencystatus = resources.getStringArray(R.array.ResidencyStatus).get(pos)
            } else {
                spinnerresidencystatus.setSelection(0)
            }
        }

        val cm =
            (activity as Edit_Profile).getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if (isConnected) {
//            GetCountrylist()
        } else {
            Toast.makeText(
                (activity as Edit_Profile),
                "You Are Not Connected To Network",
                Toast.LENGTH_SHORT
            ).show()
        }


//        //spinner item click
//        spinnerCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                strcountry = jsonArrayCountry.getJSONObject(position).getString("id")
//                jsonArrayState = db.GetStates(strcountry)!!
//                SetStatesAsSelected(strcountry)
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("Not yet implemented")
//            }
//        }
//
//        spinnerState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//
//                strstate = jsonArrayState.getJSONObject(position).getString("id")
//                jsonArrayCity = db.GetCities(strstate)!!
//                SetCityAsSelected(strstate)
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//
//            }
//        }
//
//        spinnerCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                strcity = jsonArrayCity.getJSONObject(position).getString("id")
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//
//            }
//        }


        spinnerresidencystatus.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {

                    strresidencystatus =
                        resources.getStringArray(R.array.ResidencyStatus).get(position)

                    val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                    if (textView != null) {
                        textView.setTextColor(resources.getColor(R.color.color_white))
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }
            }

//        spinnergrewin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//
//                strgrewin = jsonArrayCountry.getJSONObject(position).getString("id")
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("Not yet implemented")
//            }
//        }
        EICountryAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var countrypos = 0
                if (countriees.contains(Cname)) {
                    countrypos = countriees.indexOf(Cname)
                }
                if (countrypos >= 0) {
                    strcountry = jsonArrayCountry.getJSONObject(countrypos).getString("id")
                    booleancountryselected = true
                    SelectStateStateAndCity(strcountry)
                } else {
                    strcountry = "0"
                }
            }

        })

        var accountrycount = 0
        EICountryAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                accountrycount = count
                if (count > 0) {
                    booleancountryselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                accountrycount = s?.length!!
            }
        })

        EICountryAutocomplete.setOnDismissListener {

            if (accountrycount > 0) {
                if (!booleancountryselected) {
                    EICountryAutocomplete.requestFocus()
                    EICountryAutocomplete.showDropDown()
                }
            }
        }
        EIGrewinAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var countrypos = 0
                if (countriees.contains(Cname)) {
                    countrypos = countriees.indexOf(Cname)
                }
                if (countrypos >= 0) {
                    strgrewin = jsonArrayCountry.getJSONObject(countrypos).getString("id")
                    booleangrewinselected = true
                } else {
                    strgrewin = "0"
                }
            }

        })

        var acgrewincount = 0
        EIGrewinAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                acgrewincount = count
                if (count > 0) {
                    booleangrewinselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                acgrewincount = s?.length!!
            }
        })

        EIGrewinAutocomplete.setOnDismissListener {

            if (acgrewincount > 0) {
                if (!booleangrewinselected) {
                    EIGrewinAutocomplete.requestFocus()
                    EIGrewinAutocomplete.showDropDown()
                }
            }
        }

        EIStateAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var statepos = 0
                if (states.contains(Cname)) {
                    statepos = states.indexOf(Cname)
                }
                if (statepos >= 0) {
                    strstate = jsonArrayState.getJSONObject(statepos).getString("id")
                    booleanstateselected = true
                    SetCityValue(strstate)
                } else {
                    strstate = "0"
                }
            }

        })

        var acstatecount = 0
        EIStateAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                accountrycount = count
                if (count > 0) {
                    booleanstateselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                acstatecount = s?.length!!
            }
        })

        EIStateAutocomplete.setOnDismissListener {

            if (acstatecount > 0) {
                if (!booleanstateselected) {
                    EIStateAutocomplete.requestFocus()
                    EIStateAutocomplete.showDropDown()
                }
            }
        }

        EICityAutocomplete.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var Cname = parent?.getItemAtPosition(position)
                var citypos = 0
                if (cities.contains(Cname)) {
                    citypos = cities.indexOf(Cname)
                }
                if (citypos >= 0) {
                    strcity = jsonArrayCity.getJSONObject(citypos).getString("id")
                    booleancityselected = true
                } else {
                    strcity = "0"
                }
            }

        })

        var accitycount = 0
        EICityAutocomplete.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                accitycount = count
                if (count > 0) {
                    booleancityselected = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                accitycount = s?.length!!
            }
        })

        EICityAutocomplete.setOnDismissListener {

            if (accitycount > 0) {
                if (!booleancityselected) {
                    EICityAutocomplete.requestFocus()
                    EICityAutocomplete.showDropDown()
                }
            }
        }
        return view
    }

//    private fun SetStatesAsSelected(strcountry: String) {
//        var currentstatepos = 0
//        val currentstate: String =
//            jsonUserData.getString("state").toString()
//        if (jsonArrayState.length() > 0) {
//            for (i in 0 until jsonArrayState.length()) {
//                var tempObj = jsonArrayState.getJSONObject(i)
//                var state_Id = tempObj.getString("id")
//                if (state_Id.equals(currentstate)) {
//                    currentstatepos = i
//                }
//            }
//            if (spinnerState != null) {
//                val adapter =
//                    Adapter_Spinner_CSC(
//                        (activity as Edit_Profile),
//                        jsonArrayState, null
//                    )
//                spinnerState.adapter = adapter
//                spinnerState.setSelection(currentstatepos)
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//        } else {
//            Toast.makeText(activity as Edit_Profile, "No State Found !", Toast.LENGTH_SHORT)
//                .show()
//            strstate = "0"
//        }
//    }

//    private fun SetCityAsSelected(strstate: String) {
//        var currentpos = 0
//        val currentcity: String =
//            jsonUserData.getString("city").toString()
//        if (jsonArrayCity.length() > 0) {
//            for (i in 0 until jsonArrayCity.length()) {
//                var tempObj = jsonArrayCity.getJSONObject(i)
//                var city_Id = tempObj.getString("id")
//                if (city_Id.equals(currentcity)) {
//                    currentpos = i
//                }
//            }
//            if (spinnerCity != null) {
//                val adapter =
//                    Adapter_Spinner_CSC(
//                        (activity as Edit_Profile),
//                        jsonArrayCity, null
//                    )
//                spinnerCity.adapter = adapter
//                spinnerCity.setSelection(currentpos)
//                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
//                if (textView != null) {
//                    textView.setTextColor(resources.getColor(R.color.color_white))
//                }
//            }
//        } else {
//            Toast.makeText(activity as Edit_Profile, "No City Found !", Toast.LENGTH_SHORT)
//                .show()
//            strcity = "0"
//        }
//    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Location_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Location_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun SelectStateStateAndCity(strcountry: String) {
        states = ArrayList<String>()
        currentstatepos = 0
        jsonArrayState = db.GetStates(strcountry)!!
        var state = ""
        state = jsonUserData.getString("state")
        for (i in 0 until jsonArrayState.length()) {
            var Cname = jsonArrayState.getJSONObject(i).getString("name")
            states.add(Cname)
            try {

                if (jsonArrayState.getJSONObject(i).getString(
                        "id"
                    ).equals(state)
                ) {
                    currentstatepos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adapterstate: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Edit_Profile,
                android.R.layout.simple_list_item_1,
                states
            )
        EIStateAutocomplete.setAdapter(adapterstate)
        if (currentstatepos >= 0) {
            EIStateAutocomplete.setText(states.get(currentstatepos))
            strstate = jsonArrayState.getJSONObject(currentstatepos).getString("id")
            booleanstateselected = true
        }
        SetCityValue(strstate)
    }

    private fun SetCityValue(strstate: String) {
        currentcitypos = 0
        cities = ArrayList<String>()
        jsonArrayCity = db.GetCities(strstate)!!
        var city = ""
        city = jsonUserData.getString("city")
        for (i in 0 until jsonArrayCity.length()) {
            var Cname = jsonArrayCity.getJSONObject(i).getString("name")
            cities.add(Cname)
            try {

                if (jsonArrayCity.getJSONObject(i).getString(
                        "id"
                    ).equals(city)
                ) {
                    currentcitypos = i
                }

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        val adaptercity: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity as Edit_Profile,
                android.R.layout.simple_list_item_1,
                cities
            )
        EICityAutocomplete.setAdapter(adaptercity)
        if (currentcitypos >= 0) {
            EICityAutocomplete.setText(cities.get(currentcitypos))
            strcity = jsonArrayCity.getJSONObject(currentcitypos).getString("id")
            booleancityselected = true
        }
    }
}