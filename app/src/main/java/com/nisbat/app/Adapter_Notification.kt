package com.nisbat.app

import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity

import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class Adapter_Notification(
    val context: Context,
    val datasource: JSONArray
) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        //TODO("Not yet implemented")
        return datasource.length()

    }

    override fun getItem(p0: Int): Any {
        //TODO("Not yet implemented")
        return datasource.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        //TODO("Not yet implemented")
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))

        val view = inflater.inflate(R.layout.item_notification, p2, false)
        val tvMessage = view.findViewById(R.id.tvitemnotification) as TextView
        val image = view.findViewById<ImageView>(R.id.ivitemnotification)

        var obj = datasource.getJSONObject(p0)

        if (obj.has("profile_picture") && !obj.getString("profile_picture").isNullOrEmpty()) {
            Picasso.get()
                .load(obj.getString("profile_picture"))
                .fit()
                .centerCrop()
                .placeholder(context.resources.getDrawable(R.drawable.icon_logo_grey))
                .error(context.resources.getDrawable(R.drawable.icon_logo_grey))
                .transform(
                    CircleTransform(
                        context.resources.getDimension(
                            R.dimen._12sdp
                        )
                            .toInt(), 0
                    )
                )
                .into(image)
        } else {
            if (User_Data.getString("gender").equals("male"))
            {
                image.setBackgroundResource(R.drawable.placeholder_female)
//                Picasso.get()
//                    .load(R.drawable.placeholder_female)
//                    .fit()
//                    .placeholder(context.resources.getDrawable(R.drawable.placeholder_female))
//                    .error(context.resources.getDrawable(R.drawable.placeholder_female))
//                    .transform(
//                        CircleTransform(
//                            context.resources.getDimension(
//                                R.dimen._12sdp
//                            )
//                                .toInt(), 0
//                        )
//                    )
//                    .into(image)
            }else
            {
                image.setBackgroundResource(R.drawable.placeholder_male)
//                Picasso.get()
//                    .load(R.drawable.placeholder_male)
//                    .fit()
//                    .placeholder(context.resources.getDrawable(R.drawable.placeholder_male))
//                    .error(context.resources.getDrawable(R.drawable.placeholder_male))
//                    .transform(
//                        CircleTransform(
//                            context.resources.getDimension(
//                                R.dimen._12sdp
//                            )
//                                .toInt(), 0
//                        )
//                    )
//                    .into(image)
            }

        }

        if (obj.has("message") && !obj.getString("message").isNullOrEmpty()) {
            tvMessage.setText(obj.getString("message"))
        }


        return view
    }
}
