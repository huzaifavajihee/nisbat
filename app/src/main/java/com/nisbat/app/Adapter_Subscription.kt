package com.nisbat.app

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.SkuDetails
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject


class Adapter_Subscription(
    val context: Context,
    val datasource: List<SkuDetails>?,
    val billingClient: BillingClient?

    ) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //lateinit var btnmenu:ImageButton

    override fun getCount(): Int {
        //TODO("Not yet implemented")
        return datasource!!.size

    }

    override fun getItem(p0: Int): Any {
        //TODO("Not yet implemented")
        return datasource!!.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        //TODO("Not yet implemented")
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))

        val view = inflater.inflate(R.layout.item_subscription, p2, false)

        val tvname=view.findViewById<TextView>(R.id.tvsubscriptionitemname)
        val tvdiscount=view.findViewById<TextView>(R.id.tvsubscriptionitemdiscount)
        val tvprice=view.findViewById<TextView>(R.id.tvsubscriptionitemprice)
        val background=view.findViewById<FrameLayout>(R.id.subscriptionbackground)


        var jsonObject=datasource!!.get(p0)


        var price=jsonObject.price.split(".").toTypedArray()
        tvname.text= jsonObject.title.replace(" (Nisbat)","")
        tvprice.text=price.get(0)
        tvdiscount.text=jsonObject.description

        println(jsonObject.originalJson)

        tvname.setOnClickListener {

            CallBillingFlow(datasource!!.get(p0))

        }

        background.setOnClickListener {

            CallBillingFlow(datasource!!.get(p0))
        }
        return view
    }



    private fun CallBillingFlow(get: SkuDetails) {
        var billingFlowParams= BillingFlowParams.newBuilder()
            .setSkuDetails(get)
            .build()

        val response=
            billingClient!!.launchBillingFlow(context as Activity,billingFlowParams).responseCode

        when (response)
        {
            BillingClient.BillingResponseCode.BILLING_UNAVAILABLE ->
                Toast.makeText(context,"Billing Unavailable !",Toast.LENGTH_SHORT).show()
            BillingClient.BillingResponseCode.DEVELOPER_ERROR ->
                Toast.makeText(context,"Developer Error !",Toast.LENGTH_SHORT).show()
            BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED ->
                Toast.makeText(context,"Feature Not Supported !",Toast.LENGTH_SHORT).show()
            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED ->
                Toast.makeText(context,"Item Already Owned !",Toast.LENGTH_SHORT).show()
            BillingClient.BillingResponseCode.SERVICE_DISCONNECTED->
                Toast.makeText(context,"Service Disconnected !",Toast.LENGTH_SHORT).show()
            BillingClient.BillingResponseCode.SERVICE_TIMEOUT ->
                Toast.makeText(context,"Service Timeout !",Toast.LENGTH_SHORT).show()
            BillingClient.BillingResponseCode.ITEM_UNAVAILABLE ->
                Toast.makeText(context,"Item Unavailable !",Toast.LENGTH_SHORT).show()

        }
    }
}
