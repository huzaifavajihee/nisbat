package com.nisbat.app

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.Constraints
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Register_Career_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Register_Career_Info : Fragment() {

    var strhighestqualification: String = ""
    var strcollageschool: String = ""
    var strbusiness: String = ""
    var strincome: String = ""

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Register_Info).getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            (activity as Register_Info).getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_career_info, container, false)
        // Inflate the layout for this fragment
        val btnnext = view.findViewById<Button>(R.id.btncareerinfonext)
        val btnback = view.findViewById<Button>(R.id.btncarrerinfoback)
        val spinnerQualification =
            view.findViewById<Spinner>(R.id.spinnerregisterbasicinfohighestqualification)
        val etcollageuniversity = view.findViewById<EditText>(R.id.etregistercollageschool)
        val etbusiness = view.findViewById<EditText>(R.id.etregisterbusiness)
        val spinnerincome = view.findViewById<Spinner>(R.id.spinnerregisterannualincome)

        //set value in spinner
        if (spinnerincome != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.AnnualIncome).toMutableList(), null
            )
            spinnerincome.adapter = adapter
            if (!sharedprefencesReginfo.getString("annual_income", "").isNullOrEmpty()) {
                var tmpstrincome =
                    sharedprefencesReginfo.getString("annual_income", "")
                for (i in 0 until resources.getStringArray(R.array.AnnualIncome).size) {
                    var tmp = resources.getStringArray(R.array.AnnualIncome).get(i)
                    if (tmpstrincome.equals(tmp)) {
                        spinnerincome.setSelection(i)
                        strincome = resources.getStringArray(R.array.AnnualIncome).get(i)
                        editorReginfo.putString("annual_income", strincome)
                        editorReginfo.commit()
                    }
                }
            }
        }

        if (spinnerQualification != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.Qualification).toMutableList(), null
            )
            spinnerQualification.adapter = adapter
            if (!sharedprefencesReginfo.getString("qualification", "").isNullOrEmpty()) {
                var tmpstrqualification =
                    sharedprefencesReginfo.getString("qualification", "")
                for (i in 0 until resources.getStringArray(R.array.Qualification).size) {
                    var tmp = resources.getStringArray(R.array.Qualification).get(i)
                    if (tmpstrqualification.equals(tmp)) {
                        spinnerQualification.setSelection(i)
                        tmpstrqualification = resources.getStringArray(R.array.Qualification).get(i)
                        editorReginfo.putString("qualification", tmpstrqualification)
                        editorReginfo.commit()
                    }
                }
            }
        }

        if (!sharedprefencesReginfo.getString("college_name", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("college_name", "")
            etcollageuniversity.setText(tmpstr)
            strcollageschool = tmpstr.toString()
        }
        if (!sharedprefencesReginfo.getString("occupation", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("occupation", "")
            etbusiness.setText(tmpstr)
            strbusiness = tmpstr.toString()
        }


        //spinner item click
        spinnerincome.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strincome = resources.getStringArray(R.array.AnnualIncome).get(position)
                } else {
                    strincome = ""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
        spinnerQualification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strhighestqualification =
                        resources.getStringArray(R.array.Qualification).get(position)
                } else {
                    strhighestqualification = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        btnback.setOnClickListener {
            editorReginfo.putString("qualification", strhighestqualification)
            editorReginfo.putString("college_name", strcollageschool)
            editorReginfo.putString("occupation", strbusiness)
            editorReginfo.putString("annual_income", strincome)
            editorReginfo.commit()
            //(activity as Register_Info).onBackPressed()
            (activity as Register_Info).ShowBasicFragment()
        }
        btnnext.setOnClickListener {
            strcollageschool = etcollageuniversity.text.toString()
            strbusiness = etbusiness.text.toString()

            if (!strhighestqualification.isNullOrEmpty()) {
                editorReginfo.putString("qualification", strhighestqualification)
                editorReginfo.putString("college_name", strcollageschool)
                editorReginfo.putString("occupation", strbusiness)
                editorReginfo.putString("annual_income", strincome)
                editorReginfo.commit()

                (activity as Register_Info).ShowLocationInfo()
            } else {
                if(strhighestqualification.isNullOrEmpty())
                {Toast.makeText(
                    (activity as Register_Info),
                    "Please Select Qualification !",
                    Toast.LENGTH_SHORT
                ).show()}else if (strincome.isNullOrEmpty())
                {Toast.makeText(
                    (activity as Register_Info),
                    "Please Select Income !",
                    Toast.LENGTH_SHORT
                ).show()}

            }

        }
        return view

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Register_Career_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Register_Career_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
