package com.nisbat.app

import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Activity.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Activity : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var listView: ListView

    lateinit var jsonUserData: JSONObject
    lateinit var sharedPreferencesLogin: SharedPreferences

    lateinit var ivplaceholder: ImageView
    lateinit var btnAccepted: LinearLayout
    lateinit var btnReceived: LinearLayout
    lateinit var btnSent: LinearLayout
    lateinit var btnDelete: LinearLayout
    lateinit var tvaccepted: TextView
    lateinit var tvreceived: TextView
    lateinit var tvsent: TextView
    lateinit var tvdelete: TextView
    lateinit var tvacceptedcount: TextView
    lateinit var tvreceivedcount: TextView
    lateinit var ivNotification:ImageView
    lateinit var pulltorefresh: SwipeRefreshLayout
    var isloadingfirsttime = true
    var selectedtype = ""

    lateinit var linearnomatches: LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPreferencesLogin =
            activity!!.getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPreferencesLogin.getString("User_Data", ""))

        if (!(activity as HomeScreen).isActivityLoaded) {
            selectedtype = "Accepted"
            getActivityData(selectedtype)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_activity, container, false)

        listView = view.findViewById(R.id.listviewactivity)
        btnAccepted = view.findViewById<LinearLayout>(R.id.linearactivityaccepted)
        btnReceived = view.findViewById<LinearLayout>(R.id.linearactivityreceived)
        btnSent = view.findViewById<LinearLayout>(R.id.linearactivitysentrequests)
        btnDelete = view.findViewById<LinearLayout>(R.id.linearactivitydeleterequests)
        ivplaceholder = view.findViewById<ImageView>(R.id.ivnodatalaceholder)
        tvaccepted = view.findViewById<TextView>(R.id.tvactivityaccepted)
        tvreceived = view.findViewById(R.id.tvactivityreceived)
        tvsent = view.findViewById(R.id.tvactivitysentrequest)
        tvdelete = view.findViewById(R.id.tvactivitydeleterequests)
        linearnomatches = view.findViewById<LinearLayout>(R.id.framenodatalaceholder)
        tvacceptedcount = view.findViewById(R.id.tvactivityacceptedcount)
        tvreceivedcount = view.findViewById(R.id.tvactivityreceivedcount)
        pulltorefresh = view.findViewById<SwipeRefreshLayout>(R.id.pulltorefreshactivity)
        ivNotification=view.findViewById(R.id.ivbtnnotification)

        tvreceivedcount.setTextColor(resources.getColor(R.color.color_black))

//        if ((activity as HomeScreen).arrAccepted.length() > 0) {
//            if ((activity as HomeScreen).arrAccepted.length() > 0) {
//                tvacceptedcount.setText(
//                    (activity as HomeScreen).arrAccepted.length().toString()
//                )
//                tvacceptedcount.visibility = View.VISIBLE
//
//            } else {
//                tvacceptedcount.visibility = View.GONE
//
//            }
//        }
//        if ((activity as HomeScreen).arrReceived.length() > 0) {
//            tvreceivedcount.setText(
//                (activity as HomeScreen).arrReceived.length().toString()
//            )
//            tvreceivedcount.visibility = View.VISIBLE
//
//        } else {
//            tvreceivedcount.visibility = View.GONE
//        }


        btnAccepted.setOnClickListener(View.OnClickListener {
            btnAccepted.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10_new)
            tvaccepted.setTextColor(resources.getColor(R.color.color_white))

            btnReceived.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvreceived.setTextColor(resources.getColor(R.color.color_black))
            btnSent.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvsent.setTextColor(resources.getColor(R.color.color_black))
            btnDelete.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvdelete.setTextColor(resources.getColor(R.color.color_black))
            tvacceptedcount.setTextColor(resources.getColor(R.color.color_white))
            tvreceivedcount.setTextColor(resources.getColor(R.color.color_black))

            selectedtype = "Accepted"
            SetList(selectedtype)
        })

        btnReceived.setOnClickListener(View.OnClickListener {
            btnReceived.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10_new)
            tvreceived.setTextColor(resources.getColor(R.color.color_white))

            btnAccepted.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvaccepted.setTextColor(resources.getColor(R.color.color_black))
            btnSent.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvsent.setTextColor(resources.getColor(R.color.color_black))
            btnDelete.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvdelete.setTextColor(resources.getColor(R.color.color_black))
            tvacceptedcount.setTextColor(resources.getColor(R.color.color_black))
            tvreceivedcount.setTextColor(resources.getColor(R.color.color_white))

            selectedtype = "Received"
            SetList(selectedtype)
        })


        btnSent.setOnClickListener(View.OnClickListener {
            btnSent.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10_new)
            tvsent.setTextColor(resources.getColor(R.color.color_white))

            btnReceived.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvreceived.setTextColor(resources.getColor(R.color.color_black))
            btnAccepted.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvaccepted.setTextColor(resources.getColor(R.color.color_black))
            btnDelete.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvdelete.setTextColor(resources.getColor(R.color.color_black))
            tvacceptedcount.setTextColor(resources.getColor(R.color.color_black))
            tvreceivedcount.setTextColor(resources.getColor(R.color.color_black))

            selectedtype = "Sent"
            SetList(selectedtype)
        })


        btnDelete.setOnClickListener(View.OnClickListener {
            btnDelete.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10_new)
            tvdelete.setTextColor(resources.getColor(R.color.color_white))

            btnReceived.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvreceived.setTextColor(resources.getColor(R.color.color_black))
            btnSent.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvsent.setTextColor(resources.getColor(R.color.color_black))
            btnAccepted.setBackgroundResource(R.drawable.rounded_red_white_button_selector_10)
            tvaccepted.setTextColor(resources.getColor(R.color.color_black))
            tvacceptedcount.setTextColor(resources.getColor(R.color.color_black))
            tvacceptedcount.setTextColor(
                ContextCompat.getColor(
                    activity as HomeScreen,
                    R.color.color_black
                )
            )
            tvreceivedcount.setTextColor(resources.getColor(R.color.color_black))

            selectedtype = "Deleted"
            SetList(selectedtype)
        })

        pulltorefresh.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                isloadingfirsttime = false
                getActivityData(selectedtype)
            }
        })

        ivNotification.setOnClickListener {
            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = true

            (activity as HomeScreen).OpenEditProfile()
        }


        return view
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Activity.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Activity().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun getActivityData(type: String) {
        val progressDialog = ProgressDialog(activity)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        if (isloadingfirsttime) {
            progressDialog.show()
        }

        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )

        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/activity",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        if (isloadingfirsttime) {
                            progressDialog.dismiss()
                        }
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            if (!response.isNull("data")) {
                                val objData = response.getJSONObject("data")
                                if (objData.has("accepted")) {
                                    (activity as HomeScreen).arrAccepted = JSONArray(
                                        objData.getJSONArray("accepted").toString()
                                    )

                                    if ((activity as HomeScreen).arrAccepted.length() > 0) {
                                        tvacceptedcount.setText(
                                            (activity as HomeScreen).arrAccepted.length().toString()
                                        )
                                        tvacceptedcount.visibility = View.VISIBLE

                                    } else {
                                        tvacceptedcount.visibility = View.GONE

                                    }
                                } else {
                                    (activity as HomeScreen).arrAccepted = JSONArray()
                                    tvreceivedcount.visibility = View.GONE
                                }
                                if (objData.has("received")) {
                                    (activity as HomeScreen).arrReceived =
                                        objData.getJSONArray("received")
                                    if ((activity as HomeScreen).arrReceived.length() > 0) {
                                        tvreceivedcount.setText(
                                            (activity as HomeScreen).arrReceived.length().toString()
                                        )
                                        tvreceivedcount.visibility = View.VISIBLE

                                    } else {
                                        tvreceivedcount.visibility = View.GONE
                                    }

                                } else {
                                    (activity as HomeScreen).arrReceived = JSONArray()
                                    tvreceivedcount.visibility = View.GONE
                                }
                                if (objData.has("sent_request")) {
                                    (activity as HomeScreen).arrSent =
                                        objData.getJSONArray("sent_request")

                                } else {
                                    (activity as HomeScreen).arrSent = JSONArray()
                                }
                                if (objData.has("delete_request")) {
                                    (activity as HomeScreen).arrDeleted =
                                        objData.getJSONArray("delete_request")

                                } else {
                                    (activity as HomeScreen).arrDeleted = JSONArray()
                                }
                                SetList(type)
                                (activity as HomeScreen).isActivityLoaded = true
                                if (pulltorefresh.isRefreshing) {
                                    pulltorefresh.isRefreshing = false
                                }
                            } else {
                                (activity as HomeScreen).arrAccepted = JSONArray()
                                (activity as HomeScreen).arrReceived = JSONArray()
                                (activity as HomeScreen).arrSent = JSONArray()
                                (activity as HomeScreen).arrDeleted = JSONArray()
                                SetList(type)
                                (activity as HomeScreen).isActivityLoaded = true
                                if (pulltorefresh.isRefreshing) {
                                    pulltorefresh.isRefreshing = false
                                }
                            }

                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                activity,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                activity,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun SetList(s: String) {

        if (s.equals("Accepted")) {
            if ((activity as HomeScreen).arrAccepted.length() > 0) {
                val adapterActivity = Adapter_Activity(
                    activity!!,
                    (activity as HomeScreen).arrAccepted,
                    this@Fragment_Activity,
                    isAccepted = true
                )
                listView.adapter = adapterActivity
                listView.visibility = View.VISIBLE
                linearnomatches.visibility = View.GONE
            } else {
                listView.visibility = View.GONE
                linearnomatches.visibility = View.VISIBLE

            }
        } else if (s.equals("Received")) {
            if ((activity as HomeScreen).arrReceived.length() > 0) {
                val adapterActivity = Adapter_Activity(
                    activity!!,
                    (activity as HomeScreen).arrReceived,
                    this@Fragment_Activity,
                    isReceived = true
                )
                listView.adapter = adapterActivity
                listView.visibility = View.VISIBLE
                linearnomatches.visibility = View.GONE
            } else {
                listView.visibility = View.GONE
                linearnomatches.visibility = View.VISIBLE
            }
        } else if (s.equals("Sent")) {
            if ((activity as HomeScreen).arrSent.length() > 0) {
                val adapterActivity =
                    Adapter_Activity(
                        activity!!,
                        (activity as HomeScreen).arrSent,
                        this@Fragment_Activity,
                        isSent = true
                    )
                listView.adapter = adapterActivity
                listView.visibility = View.VISIBLE
                linearnomatches.visibility = View.GONE
            } else {

                listView.visibility = View.GONE
                linearnomatches.visibility = View.VISIBLE
            }
        } else if (s.equals("Deleted")) {
            if ((activity as HomeScreen).arrDeleted.length() > 0) {
                val adapterActivity =
                    Adapter_Activity(
                        activity!!,
                        (activity as HomeScreen).arrDeleted,
                        this@Fragment_Activity,
                        isDeleted = true
                    )
                listView.adapter = adapterActivity
                listView.visibility = View.VISIBLE
                linearnomatches.visibility = View.GONE
            } else {
                listView.visibility = View.GONE
                linearnomatches.visibility = View.VISIBLE
            }
        }

    }

    fun ItemWhatsappClicked(code: String, number: String) {


//        new AlertDialog.Builder(this)
//                .setTitle("Chat...")
//                .setMessage("Do you want to chat with customer support?")
//                .setPositiveButton("YES",
//                        new DialogInterface.OnClickListener() {
//                            @TargetApi(11)
//                            public void onClick(DialogInterface dialog, int id) {
        try {
            val pm: PackageManager = context!!.packageManager
            pm.getPackageInfo("com.whatsapp.w4b", PackageManager.GET_ACTIVITIES)
            val sendIntent = Intent("android.intent.action.MAIN")
            sendIntent.component = ComponentName("com.whatsapp.w4b", "com.whatsapp.Conversation")
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.type = "text/plain"
            sendIntent.putExtra(Intent.EXTRA_TEXT, "")
            sendIntent.putExtra("jid", code + number + "@s.whatsapp.net")
            sendIntent.setPackage("com.whatsapp.w4b")
            startActivity(sendIntent)
        } catch (e: PackageManager.NameNotFoundException) {
            try {
                val contact = "+" + code + number // use country code with your phone number
                val url = "https://api.whatsapp.com/send?phone=$contact"
                val pm: PackageManager = context!!.packageManager
                pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            } catch (e1: PackageManager.NameNotFoundException) {
                Toast.makeText(
                    activity as HomeScreen,
                    "Whatsapp not installed in your phone",
                    Toast.LENGTH_SHORT
                ).show()
                e1.printStackTrace()
            }
            e.printStackTrace()
        }
    }

    fun sendEmail(recipient: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        //put the Subject in the intent
//        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
//        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        } catch (e: Exception) {
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            Toast.makeText(activity as HomeScreen, e.message, Toast.LENGTH_LONG).show()
        }

    }

    fun Call(nmbr: String) {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + nmbr))
        startActivity(intent)
    }
}
