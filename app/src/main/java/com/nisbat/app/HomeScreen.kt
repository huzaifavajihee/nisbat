package com.nisbat.app

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

class HomeScreen : AppCompatActivity() {
    val manager = supportFragmentManager
    var booleanbasicprofile: Boolean = false
    var booleancareerprofile: Boolean = false
    var booleancontactprofile: Boolean = false
    var booleanfamilyprofile: Boolean = false
    var booleanlocationprofile: Boolean = false
    var booleanyourinfo: Boolean = false
    var booleansettings: Boolean = false
    var booleanpurchase: Boolean = false
    var booleannotification: Boolean = false
    var booleanaccounttype: Boolean = false

    var jsonMatchesData = JSONArray()
    var jsonRecentVisitors = JSONArray()
    var jsonNotificationData = JSONArray()

    lateinit var arrAccepted: JSONArray
    lateinit var arrSent: JSONArray
    lateinit var arrReceived: JSONArray
    lateinit var arrDeleted: JSONArray
    lateinit var ibhome: ImageButton
    lateinit var ibactivity: ImageButton
    lateinit var ibprofile: ImageButton

    var isMatchesLoaded = false
    var isNotificationLoaded = false
    var isActivityLoaded = false

    var preferreces:String=""

    var fragmentHome = Fragment_Home()
    var fragmentActivity = Fragment_Activity()
    var fragmentProfie = Fragment_Profie()

    var fragment_list = mutableListOf("")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = resources.getColor(R.color.color_white)
        }

        preferreces=intent.getStringExtra("From").toString()

        var countries=loadJSONFromAsset()

        ibhome = findViewById<ImageButton>(R.id.ibhomescreenhomebutton)
        ibactivity = findViewById<ImageButton>(R.id.ibhomescreenactivitybutton)
        ibprofile = findViewById<ImageButton>(R.id.ibhomescreenprofilebutton)

        val transaction = manager.beginTransaction()
        transaction.replace(R.id.framehomescreenfragment, fragmentHome)
        transaction.addToBackStack(null)
        transaction.commit()
        ibhome.isSelected = true
        ibactivity.isSelected = false
        ibprofile.isSelected = false
        fragment_list.add("Home")


        //profile button clicked
        ibprofile.setOnClickListener {
            ibprofile.isSelected = true
            ibactivity.isSelected = false
            ibhome.isSelected = false
            val transaction = manager.beginTransaction()
            transaction.replace(R.id.framehomescreenfragment, fragmentProfie)
            transaction.addToBackStack(null)
            transaction.commit()
            fragment_list.add("Profile")
        }

        ibhome.setOnClickListener {
            ibhome.isSelected = true
            ibactivity.isSelected = false
            ibprofile.isSelected = false
            val transaction = manager.beginTransaction()
            transaction.replace(R.id.framehomescreenfragment, fragmentHome)
            transaction.addToBackStack(null)
            transaction.commit()
            fragment_list.add("Home")
        }

        ibactivity.setOnClickListener {
            ibactivity.isSelected = true
            ibprofile.isSelected = false
            ibhome.isSelected = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Activity()
            transaction.replace(R.id.framehomescreenfragment, fragmentActivity)
            transaction.addToBackStack(null)
            transaction.commit()
            fragment_list.add("Activity")
        }
    }

    fun loadJSONFromAsset(): String? {
        var json: String? = null
        try {
            val `is`: InputStream = resources.getAssets().open("country.json")
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            val  charset:Charset=Charsets.UTF_8
            `is`.read(buffer)
            `is`.close()
            json=String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun checkPermissionForCall(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result: Int = checkSelfPermission(Manifest.permission.CALL_PHONE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    fun OpenEditProfile() {
        if (booleanbasicprofile) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Basic")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleancareerprofile) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Career")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleanlocationprofile) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Location")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleanfamilyprofile) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Family")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleancontactprofile) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Contact")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleanyourinfo) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "About YourSelf")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleansettings) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Settings")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleanpurchase) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Purchase")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        } else if (booleannotification) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Notification")
            intent.putExtra("From", "Home")
            startActivity(intent)
        } else if (booleanaccounttype) {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Subscription_Detail")
            intent.putExtra("From", "Home")
            startActivity(intent)
        } else {
            val intent = Intent(this, Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Images")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        }
    }

    override fun onBackPressed() {

        if (fragment_list.size > 1) {
            var fragment = fragment_list.get(fragment_list.size - 2)
            if (fragment.equals("Home")) {
                ibhome.isSelected = true
                ibactivity.isSelected = false
                ibprofile.isSelected = false
                val transaction = manager.beginTransaction()
                val fragment = Fragment_Home()
                transaction.replace(R.id.framehomescreenfragment, fragmentHome)
                transaction.addToBackStack(null)
                transaction.commit()
                fragment_list.add("Home")
            } else if (fragment.equals("Activity")) {
                ibactivity.isSelected = true
                ibprofile.isSelected = false
                ibhome.isSelected = false
                if (!checkPermissionForCall()) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.CALL_PHONE),
                        1
                    )
                }
                val transaction = manager.beginTransaction()
                val fragment = Fragment_Activity()
                transaction.replace(R.id.framehomescreenfragment, fragmentActivity)
                transaction.addToBackStack(null)
                transaction.commit()
                fragment_list.add("Activity")
            } else if (fragment.equals("Profile")) {
                ibprofile.isSelected = true
                ibactivity.isSelected = false
                ibhome.isSelected = false
                val transaction = manager.beginTransaction()
                transaction.replace(R.id.framehomescreenfragment, fragmentProfie)
                transaction.addToBackStack(null)
                transaction.commit()
                fragment_list.add("Profile")
            }
        }
    }
}
