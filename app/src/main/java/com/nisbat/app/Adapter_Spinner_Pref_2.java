package com.nisbat.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

class Adapter_Spinner_Pref_2 extends BaseAdapter {

    Context mComtext;
    List<String> Genders;
    String interestedin;
    SharedPreferences AppPref;
    int selectedpos =10;

    public Adapter_Spinner_Pref_2(Context context, List<String> GenderList, SharedPreferences appPref) {

        mComtext = context;
        Genders = GenderList;
        AppPref = appPref;
    }

    @Override
    public int getCount() {
        return Genders.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(mComtext).inflate(R.layout.item_spinner_pref_2, parent, false);

        TextView button = convertView.findViewById(R.id.tvitemspinner);
        button.setTextColor(mComtext.getResources().getColor(R.color.color_black));

        button.setText(Genders.get(position));

        return convertView;
    }
}
