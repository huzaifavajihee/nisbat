package com.nisbat.app

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.service.controls.ControlsProviderService.TAG
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

val Database_Name = "CSC_Database"
val Table_Country = "Table_Country"
val Table_State = "Table_State"
val Table_City = "Table_City"
val Tb_Co_Id = "Country_Id"
val Tb_Co_Name = "Country_Name"
val Tb_Co_Phonecode = "Country_Ph_Code"
val Tb_Co_ISO = "Country_Code_ISO2"
val Tb_St_Id = "State_Id"
val Tb_St_Name = "State_Name"
val Tb_St_CID = "State_Country_Id"
val Tb_Ct_Id = "City_Id"
val Tb_Ct_Name = "City_Name"
val Tb_Ct_SID = "City_State_Id"


class CSC_Database_Helper(context: Context) : SQLiteOpenHelper(context, Database_Name, null, 1) {

    override fun onCreate(db: SQLiteDatabase?) {

        //Creating First Table
        val QUERY = ("CREATE TABLE IF NOT EXISTS " + Table_Country
                + "(" + Tb_Co_Id + " TEXT,"
                + Tb_Co_Name + " TEXT,"
                + Tb_Co_Phonecode + " TEXT,"
                + Tb_Co_ISO + " TEXT);")
        db!!.execSQL(QUERY)

        val QUERY1 = ("CREATE TABLE IF NOT EXISTS " + Table_State
                + "(" + Tb_St_Id + " TEXT,"
                + Tb_St_Name + " TEXT,"
                + Tb_St_CID + " TEXT);")
        db!!.execSQL(QUERY1)
//
        val QUERY2 = ("CREATE TABLE IF NOT EXISTS " + Table_City
                + "(" + Tb_Ct_Id + " TEXT,"
                + Tb_Ct_Name + " TEXT,"
                + Tb_Ct_SID + " TEXT);")
        db!!.execSQL(QUERY2)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    fun InsertCountry(Country: JSONObject) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(Tb_Co_Id, Country.getString("id"))
        cv.put(Tb_Co_Name, Country.getString("name"))
        cv.put(Tb_Co_ISO, Country.getString("iso2"))
        cv.put(Tb_Co_Phonecode, Country.getString("phonecode"))
        db.insert(Table_Country, null, cv)
        if (db.isOpen) {
            db.close()
        }
    }

    fun InsertStates(State: JSONObject) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(Tb_St_Id, State.getString("id"))
        cv.put(Tb_St_Name, State.getString("name"))
        cv.put(Tb_St_CID, State.getString("country_id"))
        db.insert(Table_State, null, cv)
        Log.d(TAG, "InsertStates: " + State.getString("id"))
        if (db.isOpen) {
            db.close()
        }
    }

    fun InsertCities(City: JSONObject) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(Tb_Ct_Id, City.getString("id"))
        cv.put(Tb_Ct_Name, City.getString("name"))
        cv.put(Tb_Ct_SID, City.getString("state_id"))
        db.insert(Table_City, null, cv)
        Log.d(TAG, "InsertCities: " + City.getString("id"))
        if (db.isOpen) {
            db.close()
        }
    }

    @Throws(JSONException::class)
    fun GetCountry(): JSONArray? {
        val db = this.readableDatabase
        val ArrayCountry = JSONArray()
        val Query =
            "select * from " + Table_Country
        val cursor = db.rawQuery(Query, null) as Cursor
        if (cursor.moveToFirst()) {
            do {
                val JsonObj = JSONObject()
                JsonObj.put(
                    "id",
                    cursor.getString(cursor.getColumnIndex(Tb_Co_Id))
                )
                JsonObj.put(
                    "name",
                    cursor.getString(cursor.getColumnIndex(Tb_Co_Name))
                )
                JsonObj.put(
                    "phonecode",
                    cursor.getString(cursor.getColumnIndex(Tb_Co_Phonecode))
                )
                JsonObj.put("iso2", cursor.getString(cursor.getColumnIndex(Tb_Co_ISO)))
                ArrayCountry.put(JsonObj)
            } while (cursor.moveToNext())
        }
        return ArrayCountry
    }

    fun GetStates(CID: String): JSONArray? {
        val db = this.readableDatabase
        val ArrayState = JSONArray()
        val Query =
            "select * from " + Table_State + " where " + Tb_St_CID + " Like " + CID
        val cursor = db.rawQuery(Query, null) as Cursor
        if (cursor.count > 0) {
            if (cursor.moveToFirst()) {
                do {
                    val JsonObj = JSONObject()
                    JsonObj.put(
                        "id",
                        cursor.getString(cursor.getColumnIndex(Tb_St_Id))
                    )
                    JsonObj.put(
                        "name",
                        cursor.getString(cursor.getColumnIndex(Tb_St_Name))
                    )
                    JsonObj.put(
                        "country_id",
                        cursor.getString(cursor.getColumnIndex(Tb_St_CID))
                    )
                    ArrayState.put(JsonObj)
                } while (cursor.moveToNext())
            }
        }
        return ArrayState
    }

    fun GetCities(SID: String): JSONArray? {
        val db = this.readableDatabase
        val ArrayCity = JSONArray()
        val Query =
            "select * from " + Table_City + " where " + Tb_Ct_SID + " Like " + SID
        val cursor = db.rawQuery(Query, null) as Cursor
        if (cursor.count > 0) {
            if (cursor.moveToFirst()) {
                do {
                    val JsonObj = JSONObject()
                    JsonObj.put(
                        "id",
                        cursor.getString(cursor.getColumnIndex(Tb_Ct_Id))
                    )
                    JsonObj.put(
                        "name",
                        cursor.getString(cursor.getColumnIndex(Tb_Ct_Name))
                    )
                    JsonObj.put(
                        "state_id",
                        cursor.getString(cursor.getColumnIndex(Tb_Ct_SID))
                    )
                    ArrayCity.put(JsonObj)
                } while (cursor.moveToNext())
            }
        }
        return ArrayCity
    }

}
