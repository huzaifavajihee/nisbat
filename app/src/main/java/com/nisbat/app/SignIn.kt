package com.nisbat.app

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.LinearGradient
import android.graphics.Shader
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.text.TextUtils.isDigitsOnly
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.text.isDigitsOnly
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset


class SignIn : AppCompatActivity() {


    var booleanrememberme: Boolean = false
    var stremail: String = ""
    var strpassword: String = ""
    var strforgotemail: String = ""
    var booleanpasswordvisible: Boolean = false
    lateinit var loginPreference: SharedPreferences
    lateinit var remembermePreferences: SharedPreferences
    var FCMToken = ""
    var jsonUserData = JSONObject()
    lateinit var cscDatabaseHelper: CSC_Database_Helper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signin_3_1)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = ContextCompat.getColor(this, R.color.color_white)
        }



        GetFCMToken()
//        var countries=loadJSONFromAsset()
//
//        var jsonArrayCountries=JSONArray(countries)
//        Log.d(" Total countries",jsonArrayCountries.length().toString())
//        cscDatabaseHelper=CSC_Database_Helper(this@SignIn)
//        for (i in 0 until jsonArrayCountries.length())
//        {
//            var jsonobj=jsonArrayCountries.getJSONObject(i)
//            cscDatabaseHelper.InsertCountry(jsonobj)
//        }
//        var states=loadJSONStatesFromAsset()
//        var jsonArrayStates=JSONArray(states)
//        Log.d("",jsonArrayStates.length().toString())
//        cscDatabaseHelper=CSC_Database_Helper(this@SignIn)
//        for (i in 0 until jsonArrayStates.length())
//        {
//            var jsonobj=jsonArrayStates.getJSONObject(i)
//            cscDatabaseHelper.InsertStates(jsonobj)
//        }
//        var citiess=loadJSONCitiesFromAsset()
//        var jsonArrayCities=JSONArray(citiess)
//        Log.d("",jsonArrayCities.length().toString())
//        cscDatabaseHelper=CSC_Database_Helper(this@SignIn)
//        for (i in 0 until jsonArrayCities.length())
//        {
//            var jsonobj=jsonArrayCities.getJSONObject(i)
//            cscDatabaseHelper.InsertCities(jsonobj)
//        }


        loginPreference =
            this.getSharedPreferences(
                "pref_login_user",
                0
            );

        if (loginPreference.contains("User_Data")) {
            jsonUserData = JSONObject(loginPreference.getString("User_Data", ""))
        }

        if (loginPreference.getBoolean("is_loggedin", false)) {
            if (loginPreference.getBoolean("is_info_updated", false)) {
                if (jsonUserData.getString("profile_picture").isNullOrEmpty() ||
                    jsonUserData.getString("profile_picture").equals("abc.jpg")
                ) {
                    val intent = Intent(applicationContext, Edit_Profile::class.java)
                    intent.putExtra("From", "SignIn")
                    intent.putExtra("FragmentName", "Images")
                    startActivity(intent)
                } else {
                    val intent = Intent(applicationContext, HomeScreen::class.java)
                    startActivity(intent)
                }
            } else {
                val intent = Intent(applicationContext, Register_Info::class.java)
                startActivity(intent)
            }
            finish()
        }
        val etemail = findViewById<EditText>(R.id.etsigninemail)
        val etpassword = findViewById<EditText>(R.id.etsigninpassword)
        val ivrememberoutside = findViewById<ImageView>(R.id.ivsigninremembermeoutside)
        val ivrememberinside = findViewById<ImageView>(R.id.ivsigninremembermeinside)
        val tvloginsignup = findViewById<TextView>(R.id.tvloginsignup)
        val btnsignin = findViewById<Button>(R.id.btnlogin)
        val tvforgotpassword = findViewById<TextView>(R.id.tvloginforgotpassword)
        val frameforgotpassword = findViewById<FrameLayout>(R.id.framesigninforgotpassword)
        val etforgotemail = findViewById<EditText>(R.id.etsigninforgotpasswordemail)
        val btnforgotcancel = findViewById<Button>(R.id.btnsigninforgotpasswordcancel)
        val btnforgotok = findViewById<Button>(R.id.btnsigninforgotpasswordok)
        val ivpasswordvisible = findViewById<ImageView>(R.id.ivloginpasswordvisible)
        val framerememberme = findViewById<FrameLayout>(R.id.framesigninrememberme)
        remembermePreferences =
            getSharedPreferences("pref_login_remember_me", Context.MODE_PRIVATE);

        //Showing Values from preferences if remember me is checked
        if (remembermePreferences.getBoolean("remember", false)) {
            ivrememberoutside.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivrememberinside.visibility = View.VISIBLE
            booleanrememberme = true
            stremail = remembermePreferences.getString("remember_email", "").toString()
            strpassword = remembermePreferences.getString("remember_password", "").toString()
            etemail.setText(stremail)
            etpassword.setText(strpassword)
        } else {
            ivrememberoutside.setBackgroundResource(R.drawable.frame_white_7)
            ivrememberinside.visibility = View.INVISIBLE
            booleanrememberme = false
            etemail.setText("")
            etpassword.setText("")
        }

        // Set Text In multicolor
        val tvhometogether = findViewById<TextView>(R.id.tvlogintogether)
        val paint = tvhometogether.paint
        val shader = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            LinearGradient(
                0f, 0f, 0f, tvhometogether.textSize, getColor(R.color.color_orange), getColor(
                    R.color.color_red
                ), Shader.TileMode.CLAMP
            )
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        tvhometogether.paint.shader = shader

        //Forgot Password Clicked
        tvforgotpassword.setOnClickListener {
            frameforgotpassword.visibility = View.VISIBLE
        }

        //Forgot Password Back Clicked
        btnforgotcancel.setOnClickListener {
            frameforgotpassword.visibility = View.INVISIBLE
        }

        //Forgot Password Done Clicked
        btnforgotok.setOnClickListener {
            strforgotemail = etforgotemail.text.toString()
            if (!strforgotemail.equals("") && android.util.Patterns.EMAIL_ADDRESS.matcher(
                    strforgotemail
                ).matches()
            ) {
                val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
                if (isConnected) {
                    SendEmail()
                } else {
                    Toast.makeText(this, "You Are Not Connected To Network", Toast.LENGTH_SHORT)
                        .show()
                }
            } else {
                Toast.makeText(this, "Email should not be blank !", Toast.LENGTH_SHORT).show()
            }
        }

        //signup button clicked
        tvloginsignup.setOnClickListener {
            val intent = Intent(this, NewRegister::class.java)
            startActivity(intent)
        }


        //login button clicked
        btnsignin.setOnClickListener {

            stremail = etemail.text.toString()
            strpassword = etpassword.text.toString()
            if (!stremail.equals("") && !strpassword.equals("")) {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(stremail).matches()) {

                    Sign_In(stremail, strpassword)
                } else if (isDigitsOnly(stremail) && stremail.length == 8) {
                    Sign_In(stremail, strpassword)
                } else {
                    Toast.makeText(
                        this,
                        "Please enter valid email or ITS number !",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            } else {
                Toast.makeText(this, "Please fill all required(*) details !", Toast.LENGTH_SHORT)
                    .show()
            }

        }


        //Remember Checkbox Clicked
        val ivcheckboxout = findViewById<ImageView>(R.id.ivsigninremembermeoutside)
        framerememberme.setOnClickListener {
            if (booleanrememberme) {
                ivrememberoutside.setBackgroundResource(R.drawable.frame_white_7)
                ivrememberinside.visibility = View.INVISIBLE
                booleanrememberme = false
            } else {
                ivrememberoutside.setBackgroundResource(R.drawable.rounded_white_button_7)
                ivrememberinside.visibility = View.VISIBLE
                booleanrememberme = true
            }
        }

        //password visible/invisible
        ivpasswordvisible.setOnClickListener {

            if (!booleanpasswordvisible) {
                ivpasswordvisible.setImageResource(R.drawable.icon_eye_white)
                etpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                booleanpasswordvisible = true

            } else {
                ivpasswordvisible.setImageResource(R.drawable.icon_eye_gray)
                etpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                booleanpasswordvisible = false
            }
        }
    }

    fun loadJSONFromAsset(): String? {
        var json: String? = null
        try {
            val `is`: InputStream = resources.getAssets().open("country.json")
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            val  charset: Charset =Charsets.UTF_8
            `is`.read(buffer)
            `is`.close()
            json=String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    fun loadJSONStatesFromAsset(): String? {
        var json: String? = null
        try {
            val `is`: InputStream = resources.getAssets().open("state.json")
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            val  charset: Charset =Charsets.UTF_8
            `is`.read(buffer)
            `is`.close()
            json=String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    fun loadJSONCitiesFromAsset(): String? {
        var json: String? = null
        try {
            val `is`: InputStream = resources.getAssets().open("city2.json")
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            val  charset: Charset =Charsets.UTF_8
            `is`.read(buffer)
            `is`.close()
            json=String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }


    private fun Sign_In(stremail: String, strpassword: String) {
        var logineditor = loginPreference.edit()
        logineditor.putString("login_email", stremail)
        logineditor.putString("login_password", strpassword)
        logineditor.commit()

        if (booleanrememberme) {
            var editor = remembermePreferences.edit()
            editor.putBoolean("remember", true)
            editor.putString("remember_email", stremail)
            editor.putString("remember_password", strpassword)
            editor.commit()
        } else {
            var editor = remembermePreferences.edit()
            editor.putBoolean("remember", false)
            editor.putString("remember_email", "")
            editor.putString("remember_password", "")
            editor.commit()
        }
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if (isConnected) {
            Signin()
        } else {
            Toast.makeText(this, "You Are Not Connected To Network", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun isNumber(stremail: String): Boolean {
        if (stremail.matches("-?\\d+(\\.\\d+)?".toRegex())) {
            return false
        } else {
            return true
        }
    }

    //Signincicked
    fun Signin() {
        val progressDialog = ProgressDialog(this@SignIn)
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()
        requestParams.add("email", stremail)
        requestParams.add("password", strpassword)
        requestParams.add("fcm_token", FCMToken)

        val asyncHttpClient = AsyncHttpClient()
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "login",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        val loginPreference: SharedPreferences =
                            getSharedPreferences(
                                "pref_login_user",
                                0
                            )
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObjuserdata: JSONObject = response.getJSONObject("data")

                            var jsonaccess_token = response.getJSONObject("access_token")
                            var jsonoriginal = jsonaccess_token.getJSONObject("original")
                            var straccess_token = jsonoriginal.getString("access_token")
                            var strtoken_type = jsonoriginal.getString("token_type")
                            var editor = loginPreference.edit()
                            editor.putString("User_Data", jsonObjuserdata.toString())
                            editor.putString("access_token", straccess_token)
                            editor.putString("token_type", strtoken_type)
                            editor.commit()
                            progressDialog.dismiss()
                            if (jsonObjuserdata.getString("basic_info_status").equals("1")) {
                                if (jsonObjuserdata.getString("profile_picture").isNullOrEmpty()) {
                                    val intent =
                                        Intent(applicationContext, Edit_Profile::class.java)
                                    intent.putExtra("From", "SignIn")
                                    intent.putExtra("FragmentName", "Images")
                                    editor.putBoolean("is_loggedin", true)
                                    editor.putBoolean("is_info_updated", true)
                                    editor.commit()
                                    finish()
                                    startActivity(intent)
                                } else {
                                    val intent = Intent(applicationContext, HomeScreen::class.java)
                                    startActivity(intent)
                                    editor.putBoolean("is_loggedin", true)
                                    editor.putBoolean("is_info_updated", true)
                                    editor.commit()
                                    finish()
                                }
                            } else {
                                //Starting new Activity
                                val intent = Intent(
                                    applicationContext,
                                    Register_Info::class.java
                                )
                                editor.putBoolean("is_loggedin", true)
                                editor.putBoolean("is_info_updated", false)
                                editor.commit()
                                startActivity(intent)
                                finish()
                            }
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                applicationContext,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (status == 423) {
                            ShowAlertDialogue("Your profile is not verified yet. you will get confirmation on your registered email.")
                            progressDialog.dismiss()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                applicationContext,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@SignIn, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@SignIn, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@SignIn, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    fun SendEmail() {
        val frameforgotpassword = findViewById<FrameLayout>(R.id.framesigninforgotpassword)
        val etforgotemail = findViewById<EditText>(R.id.etsigninforgotpasswordemail)
        val progressDialog = ProgressDialog(this@SignIn)
//        progressDialog.setTitle("Forgot Password")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()
        requestParams.add("email", strforgotemail)

        val asyncHttpClient = AsyncHttpClient()
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "forgot-password",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {

                            progressDialog.dismiss()
                            ShowAlertDialogue("An email has been sent to your registered email address. Follow the instruction in the email to reset your password")
                            frameforgotpassword.visibility = View.INVISIBLE
                            etforgotemail.setText("")
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            frameforgotpassword.visibility = View.INVISIBLE
                            Toast.makeText(
                                applicationContext,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                            etforgotemail.setText("")
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@SignIn, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@SignIn, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@SignIn, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    fun ShowAlertDialogue(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)

        builder.setPositiveButton("Ok") { dialog, which ->
//            Toast.makeText(
//                applicationContext,
//                android.R.string.yes, Toast.LENGTH_SHORT
//            ).show()
        }
        builder.show()
    }


    fun GetFCMToken() {
        FirebaseApp.initializeApp(this)
        FCMToken = FirebaseInstanceId.getInstance().getToken().toString()
        Log.d("TAG", "GetFCMToken: " + FCMToken)
    }

}
