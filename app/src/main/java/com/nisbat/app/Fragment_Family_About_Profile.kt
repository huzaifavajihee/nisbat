package com.nisbat.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Family_About_Profile.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Family_About_Profile : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_family_about_profile, container, false)
        val tvfathersstatus = view.findViewById<TextView>(R.id.tvaboutprofilefatherstatus)
        val tvmothersstatus = view.findViewById<TextView>(R.id.tvaboutprofilemotherstatus)
        val tvfamilylocation = view.findViewById<TextView>(R.id.tvaboutprofilefamilylocation)
        val tvnativeplace = view.findViewById<TextView>(R.id.tvaboutprofilenativeplace)
        val tvbrothers = view.findViewById<TextView>(R.id.tvaboutprofilebrothers)
        val tvsisters = view.findViewById<TextView>(R.id.tvaboutprofilesisters)
        val tvfamilytype = view.findViewById<TextView>(R.id.tvaboutprofilefamilytype)
        val tvfamilyvalues = view.findViewById<TextView>(R.id.tvaboutprofilefamilyvalues)
        val tvaffluencelevel = view.findViewById<TextView>(R.id.tvaboutprofileaffluencelevel)

        var UserData = (activity as AboutProfile).jsonUserData

        var fathersstatus = UserData.getString("fathers_status")
        var mothersstatus = UserData.getString("mothers_status")
        var familylocation = UserData.getString("family_location")
        var nativeplace = UserData.getString("native_place")
        var unmarriedbrothers = UserData.getString("unmarried_brothers")
        var marriedbrothers = UserData.getString("married_brothers")
        var unmarriedsisters = UserData.getString("unmarried_sisters")
        var marriedsisters = UserData.getString("married_sisters")
        var familytype = UserData.getString("family_type")
        var familyvalues = UserData.getString("family_lifestyles")
        var affluencelevel = UserData.getString("family_background")
        var brothers = ""
        var sisters = ""
        if (!fathersstatus.isNullOrEmpty()) {
            tvfathersstatus.setText(
                fathersstatus.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!mothersstatus.isNullOrEmpty()) {
            tvmothersstatus.setText(
                mothersstatus.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!familylocation.isNullOrEmpty()) {
            tvfamilylocation.setText(
                familylocation.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!nativeplace.isNullOrEmpty()) {
            tvnativeplace.setText(nativeplace)
        }
        if (!unmarriedbrothers.isNullOrEmpty() && unmarriedbrothers.toInt() > 0) {
            brothers = unmarriedbrothers + " - Unmarried"
        }
        if (!marriedbrothers.isNullOrEmpty() && marriedbrothers.toInt() > 0) {
            if (brothers.isNullOrEmpty()) {
                brothers = marriedbrothers + " - Married"
            } else {
                brothers = brothers + "\n" + marriedbrothers + " - Married"
            }
        }
        if (brothers.isNullOrEmpty()) {
            tvbrothers.setText("No Data Availabele")
        } else {
            tvbrothers.setText(brothers)
        }
        if (!unmarriedsisters.isNullOrEmpty() && unmarriedsisters.toInt() > 0) {
            sisters = unmarriedsisters + " - Unmarried"
        }
        if (!marriedsisters.isNullOrEmpty() && marriedsisters.toInt() > 0) {
            if (sisters.isNullOrEmpty()) {
                sisters = marriedsisters + " - Married"
            } else {
                sisters = sisters + "\n" + marriedsisters + " - Married"
            }
        }
        if (sisters.isNullOrEmpty()) {
            tvsisters.setText("No Data Available")
        } else {
            tvsisters.setText(sisters)
        }

        if (!familytype.isNullOrEmpty()) {
            tvfamilytype.setText(
                familytype.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!familyvalues.isNullOrEmpty()) {
            tvfamilyvalues.setText(
                familyvalues.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!affluencelevel.isNullOrEmpty()) {
            tvaffluencelevel.setText(
                affluencelevel.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }


        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Family_About_Profile.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Family_About_Profile().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}