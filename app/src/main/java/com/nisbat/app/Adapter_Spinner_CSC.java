package com.nisbat.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

class Adapter_Spinner_CSC extends BaseAdapter {

    Context mComtext;
    JSONArray Genders;
    String interestedin;
    SharedPreferences AppPref;
    int selectedpos =10;

    public Adapter_Spinner_CSC(Context context, JSONArray GenderList, SharedPreferences appPref) {

        mComtext = context;
        Genders = GenderList;
        AppPref = appPref;
    }

    @Override
    public int getCount() {
        return Genders.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (mComtext.getClass().equals(NewRegister.class))
        {
            convertView = LayoutInflater.from(mComtext).inflate(R.layout.item_spinner_3, parent, false);
        }
        else
        {
            convertView = LayoutInflater.from(mComtext).inflate(R.layout.item_spinner, parent, false);
        }
        JSONObject obj = null;
        try {
            obj = Genders.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        TextView button = convertView.findViewById(R.id.tvitemspinner);
        button.setTextColor(mComtext.getResources().getColor(R.color.color_black));

        try {
            button.setText(obj.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
