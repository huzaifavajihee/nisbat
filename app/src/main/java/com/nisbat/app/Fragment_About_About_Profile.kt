package com.nisbat.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_About_About_Profile.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_About_About_Profile : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_about_about_profile, container, false)

        val tvprofilecreatedby=view.findViewById<TextView>(R.id.tvaboutprofileprofilecreatedby)
        val tvabout=view.findViewById<TextView>(R.id.tvaboutprofileaboutyourself)
        var about=(activity as AboutProfile).jsonUserData.getString("about_me")
        var tvusername=view.findViewById<TextView>(R.id.tvaboutprofileaboutusername)
        var username=(activity as AboutProfile).jsonUserData.getString("first_name")+" "+(activity as AboutProfile).jsonUserData.getString("last_name")
        var profilecreatedby=(activity as AboutProfile).jsonUserData.getString("profile_created_by")
        tvabout.setText(about)
        tvusername.setText(username)
        tvprofilecreatedby.setText(profilecreatedby)

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_About_About_Profile.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_About_About_Profile().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}