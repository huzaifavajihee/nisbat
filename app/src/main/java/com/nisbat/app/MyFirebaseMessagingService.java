package com.nisbat.app;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Vibrator;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import static android.app.NotificationManager.IMPORTANCE_HIGH;
import static android.content.ContentValues.TAG;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    JSONObject NotificationData = new JSONObject();

    @SuppressLint("WrongThread")
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        RemoteMessage.Notification notification = remoteMessage.getNotification();

        NotificationData = new JSONObject(remoteMessage.getData());
        Boolean foreground = false;
//        Log.e(TAG, "onMessageReceived: " + NotificationData);
//        try {
//            foreground = new foregroundCheck().execute(getApplicationContext()).get();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            try {
//                if (!foreground) {
                ShowNotification(NotificationData);
//                } else {
//                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//                    vibe.vibrate(80);
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {

        }
    }

    private void ShowNotification(JSONObject data) throws JSONException {

        final Intent notifIntent = new Intent(getApplicationContext(), SplashScreen.class);
        notifIntent.putExtra("FragmentName", "Notification");
        notifIntent.putExtra("data", data.toString());
        Log.d(TAG, "ShowNotification: " + data);
        notifIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
        final PendingIntent pIntent = PendingIntent.getActivity(
                getApplicationContext(), 00, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            final NotificationChannel channel;//Creating New Alarm Channel(Required Upon Android Oreo)
            channel = new NotificationChannel("NisbatNotification", "Nisbat", IMPORTANCE_HIGH);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 50, 100, 50, 100, 50});

            mNotificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "NisbatNotification");// If Android Version Is Below Oreo Than It Will Create Notification.
        builder.setSmallIcon(R.drawable.icon_logo);
        builder.setContentTitle(NotificationData.getString("title"));
        builder.setContentText(NotificationData.getString("body"));
        builder.setPriority(Notification.PRIORITY_HIGH);
        builder.setContentIntent(pIntent);
        builder.setAutoCancel(true);
        mNotificationManager.notify(1000, builder.build());
    }
}
