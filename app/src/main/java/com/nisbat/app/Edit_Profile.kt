package com.nisbat.app

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.media.Image
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telecom.Call
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject

class Edit_Profile : AppCompatActivity(), PurchasesUpdatedListener {

    var booleanbasicprofile: Boolean = false
    var booleancareerprofile: Boolean = false
    var booleancontactprofile: Boolean = false
    var booleanfamilyprofile: Boolean = false
    var booleanlocationprofile: Boolean = false
    var booleanyourinfo: Boolean = false
    var booleansetting: Boolean = false
    var booleanphotos: Boolean = false
    var booleanpurchase: Boolean = false
    var booleanNotification: Boolean = false
    var booleansubscriptiondetail: Boolean = false
    var isNotificationLoaded: Boolean = false

    var jsonNotificationData = JSONArray()

    val manager = supportFragmentManager
    lateinit var fragmentEditBasicInfo: Fragment_Edit_Basic_Info
    lateinit var fragmentEditCarrerInfo: Fragment_Edit_Career_Info
    lateinit var fragmentEditLocationInfo: Fragment_Edit_Location_Info
    lateinit var fragmentEditFamilyInfo: Fragment_Edit_Family_Info
    lateinit var fragmentEditContactInfo: Fragment_Edit_Contact_Info
    lateinit var fragmentEditYourInfo: Fragment_Edit_Your_Info
    lateinit var fragmentEditImages: Fragment_Edit_Images
    lateinit var fragmentSubscriptionDetails: Fragment_Subscription_Details
    lateinit var framebgblackfullimage: FrameLayout
    lateinit var framefullimagepopup: FrameLayout
    lateinit var closefullpopup: ImageView
    lateinit var nextfullimage: ImageView
    lateinit var previousfullimage: ImageView
    lateinit var fullimage: ImageView
    lateinit var jsonUserData: JSONObject
    lateinit var sharedPreferencesLogin: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = resources.getColor(R.color.color_white)
        }

        if (!checkPermissionForWriteExtertalStorage()) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
        }

        sharedPreferencesLogin =
            (this@Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPreferencesLogin.getString("User_Data", ""))

        var strFragmentName = intent.getStringExtra("FragmentName")
        var strFrom = intent.getStringExtra("From")

        val tvtitle = findViewById<TextView>(R.id.tveditprofiletitle)
        val btnsave = findViewById<Button>(R.id.btneditprofilesave)
        val ibtnback = findViewById<ImageButton>(R.id.ibtneditprofileback)
        val btnsavecolor = findViewById<Button>(R.id.btneditprofilesavecolor)
        framebgblackfullimage = findViewById(R.id.frame_full_image_gb_black)
        framefullimagepopup = findViewById(R.id.Frame_full_image_popup)
        closefullpopup = findViewById(R.id.full_image_close)
        fullimage = findViewById(R.id.edit_profile_full_image)
        nextfullimage = findViewById(R.id.fullimage_next)
        previousfullimage = findViewById(R.id.fullimage_previous)

        nextfullimage.setOnClickListener {
            var arraysize = fragmentEditImages.jsonImageFullArray.length()
            if (fragmentEditImages.fullimagepos < arraysize - 1) {
                fragmentEditImages.fullimagepos = fragmentEditImages.fullimagepos + 1
                var imageurl =
                    fragmentEditImages.jsonImageFullArray.getJSONObject(fragmentEditImages.fullimagepos)
                        .getString("profile_picture")
                Picasso.get()
                    .load(imageurl)
                    .placeholder(R.drawable.icon_placeholder_add_image)
                    .error(R.drawable.icon_logo_grey)
                    .fit()
                    .centerInside()
                    .into(fullimage)
            }
        }

        previousfullimage.setOnClickListener {
            if (fragmentEditImages.fullimagepos - 1 > -1) {
                fragmentEditImages.fullimagepos = fragmentEditImages.fullimagepos - 1
                var imageurl =
                    fragmentEditImages.jsonImageFullArray.getJSONObject(fragmentEditImages.fullimagepos)
                        .getString("profile_picture")
                Picasso.get()
                    .load(imageurl)
                    .placeholder(R.drawable.icon_placeholder_add_image)
                    .error(R.drawable.icon_logo_grey)
                    .fit()
                    .centerInside()
                    .into(fullimage)
            }
        }

        closefullpopup.setOnClickListener {
            framefullimagepopup.visibility = View.GONE
            framebgblackfullimage.visibility = View.GONE
        }


//        GetAllValuesFromPreferences()

        if (strFragmentName.equals("Basic")) {
            tvtitle.text = "Basic information"
            btnsave.visibility = View.VISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = true
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Basic_Info()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        } else if (strFragmentName.equals("Career")) {
            tvtitle.text = "Career and Education"
            btnsave.visibility = View.VISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = true
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Career_Info()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        } else if (strFragmentName.equals("Location")) {
            tvtitle.text = "Location"
            btnsave.visibility = View.VISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = true
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Location_Info()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        } else if (strFragmentName.equals("Family")) {
            tvtitle.text = "Family information"
            btnsave.visibility = View.VISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = true
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Family_Info()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        } else if (strFragmentName.equals("Contact")) {
            tvtitle.text = "Contact information"
            btnsave.visibility = View.VISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = true
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Contact_Info()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        } else if (strFragmentName.equals("About YourSelf")) {
            tvtitle.text = "About Yourself"
            btnsave.visibility = View.VISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = true
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Your_Info()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        } else if (strFragmentName.equals("Settings")) {
            tvtitle.text = "Settings"
            btnsave.visibility = View.INVISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = true
            booleanphotos = false
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Settings()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        } else if (strFragmentName.equals("Images")) {
            tvtitle.text = "Photos"
            if (strFrom != null) {
                if (strFrom.equals("Profile")) {
                    ibtnback.visibility = View.VISIBLE
                } else if (strFrom.equals("RegisterInfo") || strFrom.equals("SignIn")) {
                    ibtnback.visibility = View.INVISIBLE
                }
            }
            btnsave.visibility = View.INVISIBLE
            btnsavecolor.visibility = View.VISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = true
            booleanpurchase = false

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Edit_Images()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        } else if (strFragmentName.equals("Purchase")) {
            tvtitle.text = "Subscription"
            btnsave.visibility = View.INVISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = true

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Subscription()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        } else if (strFragmentName.equals("Notification")) {
            tvtitle.text = "Notification"
            btnsave.visibility = View.INVISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false
            booleanNotification = true

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Notification()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        } else if (strFragmentName.equals("Subscription_Detail")) {
            tvtitle.text = "Subscription details"
            btnsave.visibility = View.INVISIBLE
            btnsavecolor.visibility = View.INVISIBLE

            booleanbasicprofile = false
            booleancareerprofile = false
            booleanlocationprofile = false
            booleanfamilyprofile = false
            booleancontactprofile = false
            booleanyourinfo = false
            booleansetting = false
            booleanphotos = false
            booleanpurchase = false
            booleanNotification = false
            booleansubscriptiondetail = true

            val transaction = manager.beginTransaction()
            val fragment = Fragment_Subscription_Details()
            transaction.replace(R.id.frameeditprofilefragment, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        ibtnback.setOnClickListener {
            if (strFrom.equals("Notification")) {
                var intent = Intent(this@Edit_Profile, HomeScreen::class.java)
                startActivity(intent)
            } else if (strFragmentName.equals("Notification")) {
                finish()
            } else if (strFragmentName.equals("Images")) {
                if (fragmentEditImages.imagecount > 0) {
                    finish()
                    fragmentEditImages.isopened = false
                } else {
                    Toast.makeText(
                        this@Edit_Profile,
                        "Please upload at least one photo to proceed",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                finish()
            }

        }
        btnsavecolor.setOnClickListener {
            if (strFrom != null) {
                if (sharedPreferencesLogin.getBoolean("is_loggedin", false)) {
                    if (strFrom.equals("Profile")) {
                        if (fragmentEditImages.imagecount > 0) {
                            finish()
                        } else {
                            Toast.makeText(
                                this@Edit_Profile,
                                "Please upload at least one photo to proceed",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else if (strFrom.equals("RegisterInfo") || strFrom.equals("SignIn")) {
                        if (fragmentEditImages.imagecount > 0) {
                            val intent = Intent(applicationContext, HomeScreen::class.java)
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                this@Edit_Profile,
                                "Please upload at least one photo to proceed",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }

        btnsave.setOnClickListener {
            if (booleanbasicprofile) {

                if (!fragmentEditBasicInfo.strprofilecreatedby.isNullOrEmpty() && !fragmentEditBasicInfo.strdateofbirth.isNullOrEmpty()
                    && !fragmentEditBasicInfo.strmaritalstatus.isNullOrEmpty() && !fragmentEditBasicInfo.strheight.isNullOrEmpty()
                    && !fragmentEditBasicInfo.strdisability.isNullOrEmpty() && !fragmentEditBasicInfo.strhealthproblem.isNullOrEmpty()
                ) {
                    jsonUserData.put(
                        "profile_created_by",
                        fragmentEditBasicInfo.strprofilecreatedby.replace("/", "_")
                    )
                    jsonUserData.put("dob", fragmentEditBasicInfo.tvdob.text.toString())
                    jsonUserData.put(
                        "marital_status",
                        fragmentEditBasicInfo.strmaritalstatus.replace(" ", "_")
                    )
                    jsonUserData.put("height", fragmentEditBasicInfo.strheight)
                    jsonUserData.put(
                        "body_weight_kg",
                        fragmentEditBasicInfo.etweight.text.toString()
                    )
                    jsonUserData.put(
                        "any_disability",
                        fragmentEditBasicInfo.strdisability.replace(" ", "_")
                    )
                    jsonUserData.put(
                        "health_problem",
                        fragmentEditBasicInfo.strhealthproblem.replace(" ", "_")
                    )
                    jsonUserData.put("blood_group", fragmentEditBasicInfo.strbloodgroup)
                    jsonUserData.put(
                        "whats_your_diet",
                        fragmentEditBasicInfo.strdiet.replace(" ", "_")
                    )
//                jsonUserData.put("languages", sharedPreferencesLogin.getString("language", ""))
                    CallAPI()
                } else {
                    if (fragmentEditBasicInfo.strprofilecreatedby.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"profile created by\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditBasicInfo.strmaritalstatus.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Marital status\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditBasicInfo.strhealthproblem.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Health problem\" (If you have any)",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditBasicInfo.strdisability.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Disability\" (If you have any)",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditBasicInfo.strdateofbirth.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Date of birth\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditBasicInfo.strheight.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Height\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else if (booleancareerprofile) {
                if (!fragmentEditCarrerInfo.strqualification.isNullOrEmpty()) {

                    jsonUserData.put(
                        "qualification",
                        fragmentEditCarrerInfo.strqualification
                    )
                    jsonUserData.put(
                        "college_name",
                        fragmentEditCarrerInfo.etcollegename.text.toString()
                    )
                    jsonUserData.put(
                        "occupation",
                        fragmentEditCarrerInfo.etoccupation.text.toString()
                    )
                    jsonUserData.put("annual_income", fragmentEditCarrerInfo.strincome)
                    CallAPI()
                } else {
                    if (fragmentEditCarrerInfo.strqualification.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Qualification\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditCarrerInfo.strincome.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Income\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else if (booleanlocationprofile) {

                if (!fragmentEditLocationInfo.strcountry.isNullOrEmpty() && !fragmentEditLocationInfo.strstate.isNullOrEmpty()
                    && !fragmentEditLocationInfo.strcity.isNullOrEmpty()
                ) {
                    jsonUserData.put("country", fragmentEditLocationInfo.strcountry)
                    jsonUserData.put("state", fragmentEditLocationInfo.strstate)
                    jsonUserData.put("city", fragmentEditLocationInfo.strcity)
                    jsonUserData.put(
                        "residency_status",
                        fragmentEditLocationInfo.strresidencystatus.replace(" ", "_")
                    )
                    jsonUserData.put("grew_up_in", fragmentEditLocationInfo.strgrewin)
                    CallAPI()
                } else {
                    if (fragmentEditLocationInfo.strcountry.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"Country\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditLocationInfo.strstate.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"State\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditLocationInfo.strcity.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Please select \"City\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            } else if (booleanfamilyprofile) {

                jsonUserData.put(
                    "fathers_status",
                    fragmentEditFamilyInfo.strfathersstatus.replace(" ", "_")
                )
                jsonUserData.put(
                    "mothers_status",
                    fragmentEditFamilyInfo.strmotherstatus.replace(" ", "_")
                )
                jsonUserData.put(
                    "family_location",
                    fragmentEditFamilyInfo.etfamilylocation.text.toString()
                )
                jsonUserData.put(
                    "native_place",
                    fragmentEditFamilyInfo.etnativeplace.text.toString()
                )
                jsonUserData.put(
                    "married_brothers",
                    fragmentEditFamilyInfo.etnoofmarriedbrothers.text.toString()
                )
                jsonUserData.put(
                    "unmarried_brothers",
                    fragmentEditFamilyInfo.etnoofunmarriedbrothers.text.toString()
                )
                jsonUserData.put(
                    "married_sisters",
                    fragmentEditFamilyInfo.etnoofmarriedsisters.text.toString()
                )
                jsonUserData.put(
                    "unmarried_sisters",
                    fragmentEditFamilyInfo.etnoofunmarriedsisters.text.toString()
                )
                jsonUserData.put(
                    "family_type",
                    fragmentEditFamilyInfo.strfamilytype.replace(" ", "_")
                )
                jsonUserData.put(
                    "family_lifestyles",
                    fragmentEditFamilyInfo.strfamilyvalues.replace(" ", "_")
                )
                jsonUserData.put(
                    "family_background",
                    fragmentEditFamilyInfo.straffluencelevel.replace(" ", "_")
                )
                CallAPI()
            } else if (booleancontactprofile) {
                var mobilenumber = fragmentEditContactInfo.etcontactnumber.text.toString()
                var email = fragmentEditContactInfo.tvemail.text.toString()
                if (!email.isNullOrEmpty()
                    && !mobilenumber.isNullOrEmpty()
                ) {
                    jsonUserData.put("email", fragmentEditContactInfo.tvemail.text.toString())
                    jsonUserData.put(
                        "mobile_no",
                        fragmentEditContactInfo.etcontactnumber.text.toString()
                    )
                    jsonUserData.put(
                        "whats_app_no",
                        fragmentEditContactInfo.etwhatsappnumber.text.toString()
                    )
                    jsonUserData.put(
                        "instagram_id",
                        fragmentEditContactInfo.etinstagramprofile.text.toString()
                    )
                    jsonUserData.put(
                        "facebook_id",
                        fragmentEditContactInfo.etfacebooklink.text.toString()
                    )
                    jsonUserData.put(
                        "linkedin_profile",
                        fragmentEditContactInfo.etlinkedinprofile.text.toString()
                    )
                    jsonUserData.put("phone_code", fragmentEditContactInfo.strphonecode)
                    CallAPI()
                } else {
                    if (fragmentEditContactInfo.stremail.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Email should not be blank",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (fragmentEditContactInfo.strphone.isNullOrEmpty()) {
                        Toast.makeText(
                            this@Edit_Profile,
                            "Phone number should not be blank",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else if (booleanyourinfo) {

                var aboutme = fragmentEditYourInfo.etyourinfo.text.toString()
                if (!aboutme.isNullOrEmpty()) {
                    jsonUserData.put("about_me", fragmentEditYourInfo.etyourinfo.text.toString())
                    CallAPI()
                } else {
                    Toast.makeText(
                        this,
                        "Please write something about yourself",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            } else if (booleanphotos) {
            }

        }
    }

//    private fun GetAllValuesFromPreferences() {
//        jsonObjMaster.put(
//            "its_card_number",
//            sharedPreferencesLogin.getString("its_card_number", "")
//        )
//        jsonObjMaster.put("profile_for", sharedPreferencesLogin.getString("profile_for", ""))
//        jsonObjMaster.put(
//            "profile_created_by",
//            sharedPreferencesLogin.getString("profile_created_by", "")
//        )
//        jsonObjMaster.put("first_name", sharedPreferencesLogin.getString("first_name", ""))
//        jsonObjMaster.put("last_name", sharedPreferencesLogin.getString("last_name", ""))
//        jsonObjMaster.put("gender", sharedPreferencesLogin.getString("gender", ""))
//        jsonObjMaster.put("dob", sharedPreferencesLogin.getString("dob", ""))
//        jsonObjMaster.put(
//            "marital_status",
//            sharedPreferencesLogin.getString("marital_status", "")
//        )
//        jsonObjMaster.put("height", sharedPreferencesLogin.getString("height", ""))
//        jsonObjMaster.put(
//            "body_weight_kg",
//            sharedPreferencesLogin.getString("body_weight_kg", "")
//        )
//        jsonObjMaster.put(
//            "any_disability",
//            sharedPreferencesLogin.getString("any_disability", "")
//        )
//        jsonObjMaster.put(
//            "health_problem",
//            sharedPreferencesLogin.getString("health_problem", "")
//        )
//        jsonObjMaster.put("blood_group", sharedPreferencesLogin.getString("blood_group", ""))
//        jsonObjMaster.put(
//            "whats_your_diet",
//            sharedPreferencesLogin.getString("whats_your_diet", "")
//        )
//        jsonObjMaster.put("language", sharedPreferencesLogin.getString("languages", ""))
//        jsonObjMaster.put(
//            "qualification",
//            sharedPreferencesLogin.getString("qualification", "")
//        )
//        jsonObjMaster.put("college_name", sharedPreferencesLogin.getString("college_name", ""))
//        jsonObjMaster.put("occupation", sharedPreferencesLogin.getString("occupation", ""))
//        jsonObjMaster.put(
//            "annual_income",
//            sharedPreferencesLogin.getString("annual_income", "")
//        )
//        jsonObjMaster.put("country", sharedPreferencesLogin.getString("country", ""))
//        jsonObjMaster.put("state", sharedPreferencesLogin.getString("state", ""))
//        jsonObjMaster.put("city", sharedPreferencesLogin.getString("city", ""))
//        jsonObjMaster.put(
//            "residency_status",
//            sharedPreferencesLogin.getString("residency_status", "")
//        )
//        jsonObjMaster.put("grew_up_in", sharedPreferencesLogin.getString("grew_up_in", ""))
//        jsonObjMaster.put(
//            "fathers_status",
//            sharedPreferencesLogin.getString("fathers_status", "")
//        )
//        jsonObjMaster.put(
//            "mothers_status",
//            sharedPreferencesLogin.getString("mothers_status", "")
//        )
//        jsonObjMaster.put(
//            "family_location",
//            sharedPreferencesLogin.getString("family_location", "")
//        )
//        jsonObjMaster.put("native_place", sharedPreferencesLogin.getString("native_place", ""))
//        if(!sharedPreferencesLogin.getString("married_brothers","").equals("null"))
//        {
//            jsonObjMaster.put(
//                "married_brothers",
//                sharedPreferencesLogin.getString("married_brothers", "")
//            )
//        }else{
//            jsonObjMaster.put(
//                "married_brothers",
//                 "0")
//        }
//        if(!sharedPreferencesLogin.getString("unmarried_brothers","").equals("null"))
//        {
//            jsonObjMaster.put(
//                "unmarried_brothers",
//                sharedPreferencesLogin.getString("unmarried_brothers", "")
//            )
//        }else{
//            jsonObjMaster.put(
//                "unmarried_brothers",
//                "0")
//        }
////        jsonObjMaster.put(
////            "unmarried_brothers",
////            sharedPreferencesLogin.getString("unmarried_brothers", "")
////        )
//        if(!sharedPreferencesLogin.getString("married_sisters","").equals("null"))
//        {
//            jsonObjMaster.put(
//                "married_sisters",
//                sharedPreferencesLogin.getString("married_sisters", "")
//            )
//        }else{
//            jsonObjMaster.put(
//                "married_sisters",
//                "0")
//        }
//        if(!sharedPreferencesLogin.getString("unmarried_sisters","").equals("null"))
//        {
//            jsonObjMaster.put(
//                "unmarried_sisters",
//                sharedPreferencesLogin.getString("unmarried_sisters", "")
//            )
//        }else{
//            jsonObjMaster.put(
//                "unmarried_sisters",
//                "0")
//
//        }
//
//        jsonObjMaster.put("family_type", sharedPreferencesLogin.getString("family_type", ""))
//        jsonObjMaster.put(
//            "family_lifestyles",
//            sharedPreferencesLogin.getString("family_lifestyles", "")
//        )
//        jsonObjMaster.put(
//            "family_background",
//            sharedPreferencesLogin.getString("family_background", "")
//        )
//        jsonObjMaster.put("email", sharedPreferencesLogin.getString("email", ""))
//        jsonObjMaster.put("mobile_no", sharedPreferencesLogin.getString("mobile_no", ""))
//        jsonObjMaster.put("phone_code", sharedPreferencesLogin.getString("phone_code", ""))
//        jsonObjMaster.put("whats_app_no", sharedPreferencesLogin.getString("whats_app_no", ""))
//        jsonObjMaster.put("instagram_id", sharedPreferencesLogin.getString("instagram_id", ""))
//        jsonObjMaster.put("facebook_id", sharedPreferencesLogin.getString("facebook_id", ""))
//        jsonObjMaster.put(
//            "linkedin_profile",
//            sharedPreferencesLogin.getString("linkedin_profile", "")
//        )
//        jsonObjMaster.put("about_me", sharedPreferencesLogin.getString("about_me", ""))
//    }

    override fun onBackPressed() {

        if (framefullimagepopup.visibility == View.VISIBLE) {
            framebgblackfullimage.visibility = View.GONE
        } else {
            finish()
        }
    }

    fun CallAPI() {
        val progressDialog = ProgressDialog(this@Edit_Profile)
//        progressDialog.setTitle("Update")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )

        requestParams.add("profile_for", jsonUserData.getString("profile_for"))
        requestParams.add(
            "profile_created_by",
            jsonUserData.getString("profile_created_by")
        )
        requestParams.add("first_name", jsonUserData.getString("first_name"))
        requestParams.add("last_name", jsonUserData.getString("last_name"))
        requestParams.add("gender", jsonUserData.getString("gender"))
        requestParams.add("dob", jsonUserData.getString("dob"))
        requestParams.add(
            "marital_status",
            jsonUserData.getString("marital_status")
        )
        requestParams.add("height", jsonUserData.getString("height"))
        requestParams.add(
            "body_weight_kg",
            jsonUserData.getString("body_weight_kg")
        )
        requestParams.add(
            "any_disability",
            jsonUserData.getString("any_disability")
        )
        requestParams.add(
            "health_problem",
            jsonUserData.getString("health_problem")
        )
        requestParams.add("blood_group", jsonUserData.getString("blood_group"))
        requestParams.add(
            "whats_your_diet",
            jsonUserData.getString("whats_your_diet")
        )
        requestParams.add("language", sharedPreferencesLogin.getString("languages", ""))
        requestParams.add(
            "qualification",
            jsonUserData.getString("qualification")
        )
        requestParams.add("college_name", jsonUserData.getString("college_name"))
        requestParams.add("occupation", jsonUserData.getString("occupation"))
        requestParams.add(
            "annual_income",
            jsonUserData.getString("annual_income")
        )
        requestParams.add("country", jsonUserData.getString("country"))
        requestParams.add("state", jsonUserData.getString("state"))
        requestParams.add("city", jsonUserData.getString("city"))
        requestParams.add(
            "residency_status",
            jsonUserData.getString("residency_status")
        )
        requestParams.add("grew_up_in", jsonUserData.getString("grew_up_in"))
        requestParams.add(
            "fathers_status",
            jsonUserData.getString("fathers_status")
        )
        requestParams.add(
            "mothers_status",
            jsonUserData.getString("mothers_status")
        )
        requestParams.add(
            "family_location",
            jsonUserData.getString("family_location")
        )
        requestParams.add("native_place", jsonUserData.getString("native_place"))
        requestParams.add(
            "married_brothers",
            jsonUserData.getString("married_brothers")
        )
        requestParams.add(
            "unmarried_brothers",
            jsonUserData.getString("unmarried_brothers")
        )
        requestParams.add(
            "married_sisters",
            jsonUserData.getString("married_sisters")
        )
        requestParams.add(
            "unmarried_sisters",
            jsonUserData.getString("unmarried_sisters")
        )
        requestParams.add("family_type", jsonUserData.getString("family_type"))
        requestParams.add(
            "family_lifestyles",
            jsonUserData.getString("family_lifestyles")
        )
        requestParams.add(
            "family_background",
            jsonUserData.getString("family_background")
        )
        requestParams.add("email", jsonUserData.getString("email"))
        requestParams.add("mobile_no", jsonUserData.getString("mobile_no"))
        requestParams.add("phone_code", jsonUserData.getString("phone_code"))
        requestParams.add("whats_app_no", jsonUserData.getString("whats_app_no"))
        requestParams.add("instagram_id", jsonUserData.getString("instagram_id"))
        requestParams.add("facebook_id", jsonUserData.getString("facebook_id"))
        requestParams.add(
            "linkedin_profile",
            jsonUserData.getString("linkedin_profile")
        )
        requestParams.add("about_me", jsonUserData.getString("about_me"))

        Log.d("TAG", "CallAPI: " + requestParams)
        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/store-basic-info",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObjuserdata: JSONObject = response.getJSONObject("data")
                            jsonUserData = JSONObject(jsonObjuserdata.toString())
//                            println(jsonUserData)
                            var editor = sharedPreferencesLogin.edit()
                            editor.putString("User_Data", jsonUserData.toString())
                            editor.commit()
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@Edit_Profile),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                            finish()

                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@Edit_Profile),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                (this@Edit_Profile),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@Edit_Profile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(this@Edit_Profile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(this@Edit_Profile, "Internal Server Error !", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    fun checkPermissionForWriteExtertalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result: Int = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    override fun onPurchasesUpdated(p0: BillingResult, p1: MutableList<Purchase>?) {
        TODO("Not yet implemented")
    }
}
