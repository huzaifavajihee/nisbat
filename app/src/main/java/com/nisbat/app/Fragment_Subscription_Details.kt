package com.nisbat.app

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ShareActionProvider
import android.widget.TextView
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Subscription_Details.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Subscription_Details : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var tvname: TextView
    lateinit var tvsdate: TextView
    lateinit var tvedate: TextView
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        var view = inflater.inflate(R.layout.fragment_subscription_details, container, false)

        tvname = view.findViewById(R.id.tvaboutprofileplanname)
        tvsdate = view.findViewById(R.id.tvaboutprofileplanstartdate)
        tvedate = view.findViewById(R.id.tvaboutprofileplanenddate)

        if (jsonUserData.getString("plan_name").equals("one_month")) {
            tvname.text = "1 Month Plan"
        } else if (jsonUserData.getString("plan_name").equals("three_months")) {
            tvname.text = "3 Month Plan"
        } else if (jsonUserData.getString("plan_name").equals("six_months")) {
            tvname.text = "6 Month Plan"
        } else if (jsonUserData.getString("plan_name").equals("per_year")) {
            tvname.text = "12 Month Plan"
        }

        tvsdate.text = jsonUserData.getString("plan_start_date")
        tvedate.text = jsonUserData.getString("plan_end_date")

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Subscription_Details.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Subscription_Details().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}