package com.nisbat.app

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.FirebaseApp
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*


class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        FirebaseApp.initializeApp(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.statusBarColor = ContextCompat.getColor(this, R.color.color_white)
        }
    }

    fun createDatabaseFromDatabaseFile() {
        var progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading Data...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val mInput: InputStream = getAssets().open("Database.db")
        val outFileName: File? = this.getDatabasePath("CSC_Database")
        val mOutput: OutputStream = FileOutputStream(outFileName)
        val mBuffer = ByteArray(2024)
        var mLength: Int
        while (mInput.read(mBuffer).also { mLength = it } > 0) {
            mOutput.write(mBuffer, 0, mLength)
        }
        mOutput.flush()
        mOutput.close()
        mInput.close()

        val loginPreference =
            this.getSharedPreferences(
                "pref_login_user",
                0
            );
        val editor = loginPreference.edit()
        editor.putString("isDBCreated", "true")
        editor.commit()
        progressDialog.dismiss()
    }

    override fun onResume() {
        super.onResume()
        Log.d("", "onResume: " + intent.getStringExtra("data"))
        if (intent.getStringExtra("data") != null) {
            try {
                Log.d("TAG", "onResume: " + intent.getStringExtra("data"))
                var Data = JSONObject(intent.getStringExtra("data"))
                if (Data.has("type") && Data.getString("type").equals("notification")) {
                    var intent = Intent(this@SplashScreen, Edit_Profile::class.java)
                    intent.putExtra("FragmentName", "Notification")
                    intent.putExtra("From", "Notification")
                    startActivity(intent)
                } else {
                    val loginPreference = this.getSharedPreferences(
                        "pref_login_user",
                        0
                    );

                    if (!loginPreference.getString("isDBCreated", "").equals("true")) {
                        createDatabaseFromDatabaseFile()
                    }
                    val background: Thread = object : Thread() {
                        override fun run() {
                            try {
                                // Thread will sleep for 5 seconds
                                sleep(3000)

                                // After 5 seconds redirect to another intent
                                val i = Intent(this@SplashScreen, SignIn::class.java)
                                startActivity(i)

                                //Remove activity
                                finish()


                            } catch (e: Exception) {
                            }
                        }
                    }
                    background.start()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            // Set Text In multicolor
//            val tvhometogether = findViewById<TextView>(R.id.tvappname)
//            val shader = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//                LinearGradient(
//                    0f, 0f, 0f, tvhometogether.textSize, getColor(R.color.color_orange), getColor(
//                        R.color.color_red
//                    ), Shader.TileMode.CLAMP
//                )
//            } else {
//                TODO("VERSION.SDK_INT < M")
//            }
//            tvhometogether.paint.shader = shader

            val loginPreference = this.getSharedPreferences(
                "pref_login_user",
                0
            );

            if (!loginPreference.getString("isDBCreated", "").equals("true")) {
                createDatabaseFromDatabaseFile()
            }
            val background: Thread = object : Thread() {
                override fun run() {
                    try {
                        // Thread will sleep for 5 seconds
                        sleep(3000)

                        // After 5 seconds redirect to another intent
                        val i = Intent(this@SplashScreen, SignIn::class.java)
                        startActivity(i)

                        //Remove activity
                        finish()


                    } catch (e: Exception) {
                    }
                }
            }
            background.start()
        }
    }
//    }
}
