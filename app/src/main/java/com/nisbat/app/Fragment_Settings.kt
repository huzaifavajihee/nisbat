package com.nisbat.app

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import java.net.URI


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Settings.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Settings : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        val llrateus = view.findViewById<LinearLayout>(R.id.llsettingrateus)
        val llcontactus = view.findViewById<LinearLayout>(R.id.llsettingcontactus)
        val llaboutus=view.findViewById<LinearLayout>(R.id.llsettingaboutus)
        val lltandc=view.findViewById<LinearLayout>(R.id.llsettingtandc)
        // Inflate the layout for this fragment

        llrateus.setOnClickListener {
            SettingRateusClicked()
        }

        llcontactus.setOnClickListener {
            sendEmail()
        }

        llaboutus.setOnClickListener {
            OpenUrl("https://uniqueinfosolution.com/demo/matrimonial/aboutus")
        }

        lltandc.setOnClickListener {
            OpenUrl("https://uniqueinfosolution.com/demo/matrimonial/terms-and-condition")
        }
        return view
    }

    private fun OpenUrl(s: String) {

        var i=Intent(Intent.ACTION_VIEW)
        i.setData(Uri.parse(s))
        startActivity(i)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Settings.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Settings().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun SettingRateusClicked() {
        val appPackageName: String =
            (activity as Edit_Profile).packageName// getPackageName() from Context or Activity object

        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }

    private fun sendEmail() {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
//        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        var pm = (activity as Edit_Profile).packageManager
        var match = pm.queryIntentActivities(mIntent, 0)
        var best: ResolveInfo? = null
        for (info in match) if (info.activityInfo.packageName.endsWith(".gm") ||
            info.activityInfo.name.toLowerCase().contains("gmail")
        ) best = info
        if (best != null)
            mIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name)
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("info.alitetecknolabs@gmail.com"))
        //put the Subject in the intent
//        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
//        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(mIntent)
        } catch (e: Exception) {
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            Toast.makeText(activity as Edit_Profile, e.message, Toast.LENGTH_LONG).show()
        }

    }
}