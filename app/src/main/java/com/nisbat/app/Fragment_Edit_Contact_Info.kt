package com.nisbat.app

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Contact_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Contact_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var stremail: String = ""
    var strphone: String = ""
    var strwhatsappnumber: String = ""
    var strinstagram: String = ""
    var strfacebooklink: String = ""
    var strlinkedin: String = ""
    var strphonecode: String = ""

    lateinit var tvemail: TextView
    lateinit var etcontactnumber: EditText
    lateinit var etwhatsappnumber: EditText
    lateinit var etinstagramprofile: EditText
    lateinit var etfacebooklink: EditText
    lateinit var etlinkedinprofile: EditText
    lateinit var spinnercountrycode: Spinner

    var jsonArrayCountry: JSONArray = JSONArray()

    val strarrayCCode = ArrayList<String>()
    var jsonArrayCCode = JSONArray()
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_contact_info, container, false)
        // Inflate the layout for this fragment

        (activity as Edit_Profile).fragmentEditContactInfo = this@Fragment_Edit_Contact_Info

        etcontactnumber = view.findViewById<EditText>(R.id.eteditContactinfocontactnumber)
        tvemail = view.findViewById<TextView>(R.id.tveditcontactinfoemail)
        etwhatsappnumber = view.findViewById<EditText>(R.id.eteditcontactinfowhatsappnumber)
        etinstagramprofile = view.findViewById<EditText>(R.id.eteditcontactinfoinstagramusername)
        etfacebooklink = view.findViewById<EditText>(R.id.eteditcontactinfofacebookprofilelink)
        etlinkedinprofile = view.findViewById<EditText>(R.id.eteditcontactinfolinkedinprofile)
        spinnercountrycode = view.findViewById<Spinner>(R.id.spinnereditcontactinfocountrycode)

        val progressDialog = ProgressDialog(activity as Edit_Profile)
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
//        GetPhoneCode()
        Thread {
            // DO your work here
            // get the data
            jsonArrayCountry = JSONArray(loadCountryJSONFromAsset())
            (activity as Edit_Profile).runOnUiThread(Runnable { // update UI
                var locale = resources.configuration.locale.displayCountry
                var currentcountrycodeposition: Int = 0
                for (i in 0 until jsonArrayCountry.length()) {
                    var jsonobj = jsonArrayCountry.getJSONObject(i)
                    var code = jsonobj.getString("phonecode")
                    var countryname = jsonobj.getString("name")
                    var phonecode = code.split("-")

                    var finalcode =
                        "+" + phonecode.get(0).replace("+", "") + " " + countryname
                    if (!strarrayCCode.contains(finalcode)) {
                        if (!finalcode.equals("+")) {
                            strarrayCCode.add(finalcode)
                        }
                    }
                    var temp = jsonUserData.getString("phone_code")
                    var ccode = phonecode[0].replace("+", "")
                    if (temp.equals(ccode)) {
                        strphonecode = phonecode[0].replace("+", "")
                        currentcountrycodeposition = i
                    }
                }
                if (spinnercountrycode != null) {

                    val adapter =
                        Adapter_CountryCode(
                            activity as Edit_Profile,
                            strarrayCCode,
                            null
                        )
                    spinnercountrycode.adapter = adapter
                    spinnercountrycode.setSelection(currentcountrycodeposition)
                }
                progressDialog.dismiss()
            })
        }.start()

        if (!jsonUserData.getString("email").equals("")) {
            tvemail.text = jsonUserData.getString("email")
            tvemail.isEnabled = false
            stremail = tvemail.text.toString()
        }
        if (!jsonUserData.getString("mobile_no").equals("")) {
            etcontactnumber.setText(jsonUserData.getString("mobile_no"))
            strphone = etcontactnumber.text.toString()
        }
        if (!jsonUserData.getString("whats_app_no").equals("")) {
            etwhatsappnumber.setText(
                jsonUserData.getString("whats_app_no").toString()
            )
            strwhatsappnumber = etwhatsappnumber.text.toString()
        }
        if (!jsonUserData.getString("instagram_id").equals("")) {
            etinstagramprofile.setText(jsonUserData.getString("instagram_id"))
            strinstagram = etinstagramprofile.text.toString()
        }
        if (!jsonUserData.getString("facebook_id").equals("")) {
            etfacebooklink.setText(jsonUserData.getString("facebook_id"))
            strfacebooklink = tvemail.text.toString()
        }
        if (!jsonUserData.getString("linkedin_profile").equals("")) {
            etlinkedinprofile.setText(jsonUserData.getString("linkedin_profile"))
            strlinkedin = etlinkedinprofile.text.toString()
        }

        spinnercountrycode.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                var code: List<String> = strarrayCCode.get(position).split(" ")
                var strcountrycode1 = strarrayCCode.get(position).replace("+", "").split(" ")
                strphonecode = strcountrycode1[0]
                val textView = view?.findViewById<TextView>(R.id.tvspinnercountrycode)
                if (textView != null) {
                    textView.text = "+" + strphonecode
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }


        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Contact_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Contact_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun GetPhoneCode() {
        val progressDialog = ProgressDialog(activity as Edit_Profile)
//        progressDialog.setTitle("Update")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val requestParams = RequestParams()

        val asyncHttpClient = AsyncHttpClient()
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "get-country-phone-code",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            strarrayCCode.clear()
                            jsonArrayCCode = response.getJSONArray("data")
                            var locale = resources.configuration.locale.displayCountry
                            var currentcountrycodeposition: Int = 0
                            for (i in 0 until jsonArrayCCode.length()) {
                                var jsonobj = jsonArrayCCode.getJSONObject(i)
                                var code = jsonobj.getString("phonecode")
                                var countryname = jsonobj.getString("name")
                                var phonecode = code.split("-")

                                var finalcode =
                                    "+" + phonecode.get(0).replace("+", "") + "   " + countryname
                                if (!strarrayCCode.contains(finalcode)) {
                                    if (!finalcode.equals("+")) {
                                        strarrayCCode.add(finalcode)
                                    }
                                }
                                var temp = jsonUserData.getString("phone_code")
                                var ccode = phonecode[0].replace("+", "")
                                if (temp.equals(ccode)) {
                                    strphonecode = phonecode[0].replace("+", "")
                                    currentcountrycodeposition = i
                                }
                            }
//                            Log.d("TAG", "onSuccess: ")
                            if (spinnercountrycode != null) {

                                val adapterGender =
                                    Adapter_CountryCode(
                                        activity as Edit_Profile,
                                        strarrayCCode,
                                        null
                                    )
                                val adapter = ArrayAdapter(
                                    activity as Edit_Profile,
                                    R.layout.item_spinner_countrycode,
                                    strarrayCCode
                                )
                                spinnercountrycode.adapter = adapterGender
                                spinnercountrycode.setSelection(currentcountrycodeposition)
                            }
                            progressDialog.dismiss()
//                            Toast.makeText(
//                                (this@NewRegister),
//                                response.getString("message"),
//                                Toast.LENGTH_SHORT
//                            ).show()


                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                activity as Edit_Profile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                activity as Edit_Profile,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText((activity as Edit_Profile),"Internal Server Error !",Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(activity as Edit_Profile,"Internal Server Error !",Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(activity as Edit_Profile,"Internal Server Error !",Toast.LENGTH_SHORT).show()
                }
            })
    }
    fun loadCountryJSONFromAsset(): String? {
        var json: String? = null
        json = try {
            val IS: InputStream = (activity as Edit_Profile).assets.open("country.json")
            val size = IS.available()
            val buffer = ByteArray(size)
            IS.read(buffer)
            IS.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
}