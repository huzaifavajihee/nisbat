package com.nisbat.app

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BlurMaskFilter
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Contact_About_Profile.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Contact_About_Profile : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var loginPreferences: SharedPreferences
    lateinit var jsonUserData: JSONObject
    var isActiveNow = false
    lateinit var blurbg1: FrameLayout
    lateinit var blurbg2: FrameLayout
    lateinit var btnupgrade: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        loginPreferences =
            (activity as AboutProfile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(loginPreferences.getString("User_Data", ""))

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_contact_about_profile, container, false)
        val tvnumber = view.findViewById<TextView>(R.id.tvaboutprofilecontactnumber)
        val tvemail = view.findViewById<TextView>(R.id.tvaboutprofileemail)
        val tvfacebook = view.findViewById<TextView>(R.id.tvaboutprofilecontactfacebook)
        val tvinstagram = view.findViewById<TextView>(R.id.tvaboutprofilecontactinstagram)
        val tvwhatsapp = view.findViewById<TextView>(R.id.tvaboutprofilecontactwhatsapp)
        val tvlinkedin = view.findViewById<TextView>(R.id.tvaboutprofilecontactlinkedin)
        blurbg1 = view.findViewById<FrameLayout>(R.id.Blur_Detail_BG)
        blurbg2 = view.findViewById<FrameLayout>(R.id.Blur_Social_BG)
        btnupgrade = view.findViewById<Button>(R.id.BtnUpgradetoPro)

//        Blurry.with(activity as AboutProfile)
//            .radius(100)
//            .sampling(20)
//            .color(Color.argb(66,255,255,0))
//            .async()
//            .animate(500)
//            .onto(blurbg1)
//        Blurry.with(activity as AboutProfile)
//            .radius(100)
//            .sampling(8)
//            .color(Color.argb(66,255,255,0))
//            .async()
//            .animate(500)
//            .onto(blurbg2)


        if (jsonUserData.has("is_pro") && jsonUserData.getString("is_pro").equals("true")) {
            val date = Date()

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            var target_date = ""
            if (jsonUserData.has("plan_end_date") && !jsonUserData.getString("plan_end_date")
                    .isNullOrEmpty()
            ) {
                target_date = jsonUserData.getString("plan_end_date")
            }
            val targetdate = sdf.parse(target_date) as Date
            if (targetdate.compareTo(date) >= 0) {
                isActiveNow = true
            } else {
                isActiveNow = false
            }
            if (!isActiveNow) {
                val drawable = resources.getDrawable(R.drawable.blur_bg)
                val bitmap = (drawable as BitmapDrawable).bitmap
                blurbg1.visibility = View.VISIBLE
                blurbg2.visibility = View.VISIBLE
                btnupgrade.visibility = View.VISIBLE
            }

//            Blurry.with(activity as AboutProfile)
//                .radius(25)
//                .sampling(2)
//                .onto(blurbg1)
//            Blurry.with(activity as AboutProfile)
//                .radius(25)
//                .sampling(2)
//                .onto(blurbg2)

        }

        var jsonObject = (activity as AboutProfile).jsonUserData
        var number = "+" + jsonObject.getString("phone_code") + " " +
                jsonObject.getString("mobile_no")
        var email = jsonObject.getString("email")
        if (!jsonObject.getString("facebook_id").isNullOrEmpty()) {
            tvfacebook.setText(jsonObject.getString("facebook_id"))
        }
        if (!jsonObject.getString("instagram_id").isNullOrEmpty()) {
            tvinstagram.setText(jsonObject.getString("instagram_id"))
        }
        if (!jsonObject.getString("whats_app_no").isNullOrEmpty()) {
            tvwhatsapp.setText(jsonObject.getString("whats_app_no"))
        }
        if (!jsonObject.getString("linkedin_profile").isNullOrEmpty()) {
            tvlinkedin.setText(jsonObject.getString("linkedin_profile"))
        }


        tvnumber.setText(number)
        tvemail.setText(email)

        btnupgrade.setOnClickListener {

            val intent = Intent((activity as AboutProfile), Edit_Profile::class.java)
            intent.putExtra("FragmentName", "Purchase")
            intent.putExtra("From", "Profile")
            startActivity(intent)
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Contact_About_Profile.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Contact_About_Profile().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val drawable=resources.getDrawable(R.drawable.blur_bg)
//        val bitmap=(drawable as BitmapDrawable).bitmap
//        val ll = view.findViewById<LinearLayout>(R.id.llDetail)
//        val blur =view.findViewById<FrameLayout>(R.id.blurView)
//        BlurBuilder.blur(activity, bitmap,blur);
    }

    override fun onResume() {
        super.onResume()

        if (jsonUserData.has("is_pro") && jsonUserData.getString("is_pro").equals("true")) {
            val date = Date()

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            var target_date = ""
            if (jsonUserData.has("plan_end_date") && !jsonUserData.getString("plan_end_date")
                    .isNullOrEmpty()
            ) {
                target_date = jsonUserData.getString("plan_end_date")
            }
            val targetdate = sdf.parse(target_date) as Date
            if (targetdate.compareTo(date) >= 0) {
                isActiveNow = true
            } else {
                isActiveNow = false
            }
            if (isActiveNow) {
                val drawable = resources.getDrawable(R.drawable.blur_bg)
                val bitmap = (drawable as BitmapDrawable).bitmap
                blurbg1.visibility = View.GONE
                blurbg2.visibility = View.GONE
                btnupgrade.visibility = View.GONE
            }
        }
    }
}

