package com.nisbat.app

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.Constraints
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Register_Family_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Register_Family_Info : Fragment() {

    var strfathersstatus: String = ""
    var strmotherstatus: String = ""
    var strfamilylocation: String = ""
    var strnativeplace: String = ""
    var strnoofmarriedbrothers: String = ""
    var strnoofunmarriedbrothers: String = ""
    var strnoofmarriedsisters: String = ""
    var strnoofunmarriedsisters: String = ""
    var strfamilytype: String = ""
    var strfamilyvalues: String = ""
    var straffluencelevel: String = ""

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Register_Info).getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            (activity as Register_Info).getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_family_info, container, false)
        // Inflate the layout for this fragment

        val btnnext = view.findViewById<Button>(R.id.btnfamilyinfonext)
        val btnback = view.findViewById<Button>(R.id.btnfamilyinfoback)
        val spinnerfathersstatus = view.findViewById<Spinner>(R.id.spinnerregisterfatherstatus)
        val spinnermothersstatus = view.findViewById<Spinner>(R.id.spinnerregistermotherstatus)
        val etfamilylocation = view.findViewById<EditText>(R.id.etregisterfamilylocation)
        val etnativeplace = view.findViewById<EditText>(R.id.etregisternativeplace)
        val etnoofmarriedbrothers = view.findViewById<EditText>(R.id.etregistermarriedbrothers)
        val etnoofunmarriedbrothers = view.findViewById<EditText>(R.id.etregisternotmarriedbrothers)
        val etnoofmarriedsisters = view.findViewById<EditText>(R.id.etregistermarriedsisters)
        val etnoofunmarriedsisters = view.findViewById<EditText>(R.id.etregisternotmarriedsisters)
        val spinnerfamilytype = view.findViewById<Spinner>(R.id.spinnerregisterfamilytype)
        val spinnerfamilyvalues = view.findViewById<Spinner>(R.id.spinnerregisterfamilyvalues)
        val spinneraffluencelevel = view.findViewById<Spinner>(R.id.spinnerregisteraffluencelevel)


        if (!sharedprefencesReginfo.getString("family_location", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("family_location", "")
            etfamilylocation.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("native_place", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("native_place", "")
            etnativeplace.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("married_sisters", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("married_sisters", "")
            etnoofmarriedsisters.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("married_brothers", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("married_brothers", "")
            etnoofmarriedbrothers.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("unmarried_sisters", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("unmarried_sisters", "")
            etnoofunmarriedsisters.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("unmarried_brothers", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("unmarried_brothers", "")
            etnoofunmarriedbrothers.setText(tmpstr)
        }
        //set value in spinner
        if (spinnerfathersstatus != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.FatherStatus).toMutableList(), null
            )
            spinnerfathersstatus.adapter = adapter
            if (!sharedprefencesReginfo.getString("fathers_status", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("fathers_status", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.FatherStatus).indexOf(tmpstr)
                spinnerfathersstatus.setSelection(pos)
                strfathersstatus = resources.getStringArray(R.array.FatherStatus).get(pos)
            }
        }
        if (spinnermothersstatus != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.MothersStatus).toMutableList(), null
            )
            spinnermothersstatus.adapter = adapter
            if (!sharedprefencesReginfo.getString("mothers_status", "").isNullOrEmpty()) {
                var tmpstr = sharedprefencesReginfo.getString("mothers_status", "")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.MothersStatus).indexOf(tmpstr)
                spinnermothersstatus.setSelection(pos)
                strmotherstatus = resources.getStringArray(R.array.MothersStatus).get(pos)
            }
        }

        if (spinnerfamilytype != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.FamilyType).toMutableList(), null
            )
            spinnerfamilytype.adapter = adapter
            if (!sharedprefencesReginfo.getString("family_type", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("family_type", "")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.FamilyType).indexOf(tmpstr)
                spinnerfamilytype.setSelection(pos)
                strfamilytype = resources.getStringArray(R.array.FamilyType).get(pos)
            }
        }
        if (spinnerfamilyvalues != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.FamilyValues).toMutableList(), null
            )
            spinnerfamilyvalues.adapter = adapter
            if (!sharedprefencesReginfo.getString("family_lifestyles", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("family_lifestyles", "")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.FamilyValues).indexOf(tmpstr)
                spinnerfamilyvalues.setSelection(pos)
                strfamilyvalues = resources.getStringArray(R.array.FamilyValues).get(pos)
            }
        }
        if (spinneraffluencelevel != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.AffluenceLevel).toMutableList(), null
            )
            spinneraffluencelevel.adapter = adapter
            if (!sharedprefencesReginfo.getString("family_background", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("family_background", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }

                var pos = resources.getStringArray(R.array.AffluenceLevel).indexOf(tmpstr)
                spinneraffluencelevel.setSelection(pos)
                straffluencelevel = resources.getStringArray(R.array.AffluenceLevel).get(pos)
            }
        }

        //spinner item click
        spinnerfathersstatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strfathersstatus = resources.getStringArray(R.array.FatherStatus).get(position)
                } else {
                    strfathersstatus = ""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnermothersstatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strmotherstatus = resources.getStringArray(R.array.MothersStatus).get(position)
                } else {
                    strmotherstatus = ""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerfamilytype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strfamilytype = resources.getStringArray(R.array.FamilyType).get(position)
                } else {
                    strfamilytype = ""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerfamilyvalues.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (position != 0) {
                    strfamilyvalues = resources.getStringArray(R.array.FamilyValues).get(position)
                } else {
                    strfamilyvalues = ""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinneraffluencelevel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    straffluencelevel =
                        resources.getStringArray(R.array.AffluenceLevel).get(position)
                } else {
                    straffluencelevel = ""
                }

                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        //button back clicked
        btnback.setOnClickListener {
            editorReginfo.putString("fathers_status", strfathersstatus)
            editorReginfo.putString("mothers_status", strmotherstatus)
            editorReginfo.putString("family_location", strfamilylocation)
            editorReginfo.putString("native_place", strnativeplace)
            editorReginfo.putString("married_brothers", strnoofmarriedbrothers)
            editorReginfo.putString("unmarried_brothers", strnoofunmarriedbrothers)
            editorReginfo.putString("married_sisters", strnoofmarriedsisters)
            editorReginfo.putString("unmarried_sisters", strnoofunmarriedsisters)
            editorReginfo.putString("family_type", strfamilytype)
            editorReginfo.putString("family_lifestyles", strfamilyvalues)
            editorReginfo.putString("family_background", straffluencelevel)
            editorReginfo.commit()
            (activity as Register_Info).ShowLocationInfo()
        }

        //button next clicked
        btnnext.setOnClickListener {

            strfamilylocation = etfamilylocation.text.toString()
            strnativeplace = etnativeplace.text.toString()
            strnoofmarriedbrothers = etnoofmarriedbrothers.text.toString()
            strnoofunmarriedbrothers = etnoofunmarriedbrothers.text.toString()
            strnoofmarriedsisters = etnoofmarriedsisters.text.toString()
            strnoofunmarriedsisters = etnoofunmarriedsisters.text.toString()

//            callAPI()
            editorReginfo.putString("fathers_status", strfathersstatus.replace(" ", "_"))
            editorReginfo.putString("mothers_status", strmotherstatus.replace(" ", "_"))
            editorReginfo.putString("family_location", strfamilylocation)
            editorReginfo.putString("native_place", strnativeplace)
            editorReginfo.putString("married_brothers", strnoofmarriedbrothers)
            editorReginfo.putString("unmarried_brothers", strnoofunmarriedbrothers)
            editorReginfo.putString("married_sisters", strnoofmarriedsisters)
            editorReginfo.putString("unmarried_sisters", strnoofunmarriedsisters)
            editorReginfo.putString("family_type", strfamilytype.replace(" ", "_"))
            editorReginfo.putString("family_lifestyles", strfamilyvalues.replace(" ", "_"))
            editorReginfo.putString("family_background", straffluencelevel.replace(" ", "_"))
            editorReginfo.commit()


            (activity as Register_Info).ShowContactInfo()
        }
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Register_Family_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Register_Family_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}