package com.nisbat.app

import android.app.Notification
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.NotificationCompat
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONArray
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Home.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Home : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var jsonUserData: JSONObject
    lateinit var sharedPreferencesLogin: SharedPreferences


    lateinit var framenodata: LinearLayout
    lateinit var listMymatches: ListView
    lateinit var pulltorefresh: SwipeRefreshLayout
    lateinit var adapter: Adapter_MyMatches
    lateinit var btnmymatches: Button
    lateinit var btnrecentlyvisitors: Button
    var isloadingfirsttime = true
    lateinit var tvnodata: TextView
    lateinit var ivprofilepic: ImageView
    lateinit var ivcircleprofilepic: CircleImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPreferencesLogin =
            activity!!.getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPreferencesLogin.getString("User_Data", ""))
        if (!sharedPreferencesLogin.getString("recentely_visitors", "").isNullOrEmpty()) {
            (activity as HomeScreen).jsonRecentVisitors =
                JSONArray(sharedPreferencesLogin.getString("recentely_visitors", ""))
        }

//        if (!(activity as HomeScreen).isMatchesLoaded || (activity as HomeScreen).preferreces.equals(
//                "Preference Success"
//            )
//        ) {
//            GetMatches()
//        } else {
//            if ((activity as HomeScreen).jsonMatchesData.length() > 0) {
//                val adapter = Adapter_MyMatches(
//                    activity!!,
//                    (activity as HomeScreen).jsonMatchesData,
//                    this@Fragment_Home
//                )
//                listMymatches.adapter = adapter
//                framenodata.visibility = View.GONE
//                listMymatches.visibility = View.VISIBLE
//            } else {
//                framenodata.visibility = View.VISIBLE
//                listMymatches.visibility = View.GONE
//            }
//        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_home, container, false)
        // Inflate the layout for this fragment
        btnmymatches = view.findViewById<Button>(R.id.btnhomemymatches)
        btnrecentlyvisitors = view.findViewById<Button>(R.id.btnhomerecentlyvisitors)
        val tvsetpreferences = view.findViewById<TextView>(R.id.tvhomesetepreferences)
        val tvhomeusername = view.findViewById<TextView>(R.id.tvhomeusername)
        ivprofilepic = view.findViewById<ImageView>(R.id.ivhomeprofileimage)
        pulltorefresh = view.findViewById<SwipeRefreshLayout>(R.id.pulltorefreshmatches)
        framenodata = view.findViewById(R.id.framenodataplaceholder)
        listMymatches = view.findViewById(R.id.lvhomemymatches)
        tvnodata = view.findViewById(R.id.HomeTvNoData)
        ivcircleprofilepic = view.findViewById(R.id.ivprofilepic)

        tvhomeusername.text = "Hi, " + jsonUserData.getString("first_name")
        if (!jsonUserData.getString("profile_picture").isNullOrEmpty()) {
            if (jsonUserData.getString("gender").equals("male")) {
                Picasso.get()
                    .load(jsonUserData.getString("profile_picture"))
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_male)
                    .error(R.drawable.placeholder_male)
                    .transform(
                        CircleTransform(
                            this.resources.getDimension(
                                R.dimen._18sdp
                            )
                                .toInt(), 0
                        )
                    )
                    .into(ivcircleprofilepic)
            } else {
                Picasso.get()
                    .load(jsonUserData.getString("profile_picture"))
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_female)
                    .error(R.drawable.placeholder_female)
                    .transform(
                        CircleTransform(
                            this.resources.getDimension(
                                R.dimen._18sdp
                            )
                                .toInt(), 0
                        )
                    )
                    .into(ivcircleprofilepic)
            }
        }

        btnmymatches.isSelected = true

        if (!(activity as HomeScreen).isMatchesLoaded || (activity as HomeScreen).preferreces.equals(
                "Preference Success"
            )
        ) {
            GetMatches()
        } else {
            if ((activity as HomeScreen).jsonMatchesData.length() > 0) {
                val adapter = Adapter_MyMatches(
                    activity!!,
                    (activity as HomeScreen).jsonMatchesData,
                    this@Fragment_Home
                )
                listMymatches.adapter = adapter
                framenodata.visibility = View.GONE
                listMymatches.visibility = View.VISIBLE
            } else {
                framenodata.visibility = View.VISIBLE
                listMymatches.visibility = View.GONE
            }
        }

        //button click
        btnmymatches.setOnClickListener {
            btnmymatches.isSelected = true
            btnrecentlyvisitors.isSelected = false
            if ((activity as HomeScreen).jsonMatchesData.length() > 0) {
                adapter = Adapter_MyMatches(
                    activity!!,
                    (activity as HomeScreen).jsonMatchesData,
                    this@Fragment_Home
                )

                listMymatches.adapter = adapter
                framenodata.visibility = View.GONE
                listMymatches.visibility = View.VISIBLE
            } else {
                framenodata.visibility = View.VISIBLE
                listMymatches.visibility = View.GONE
                tvnodata.text = "No Matches"

            }
        }
        btnrecentlyvisitors.setOnClickListener {
            btnmymatches.isSelected = false
            btnrecentlyvisitors.isSelected = true
            if ((activity as HomeScreen).jsonRecentVisitors.length() > 0) {
                adapter = Adapter_MyMatches(
                    activity!!,
                    (activity as HomeScreen).jsonRecentVisitors,
                    this@Fragment_Home
                )
                listMymatches.adapter = adapter
                framenodata.visibility = View.GONE
                listMymatches.visibility = View.VISIBLE
            } else {
                framenodata.visibility = View.VISIBLE
                tvnodata.text = "No Recent Visitors"
                listMymatches.visibility = View.GONE
            }
        }

        tvsetpreferences.setOnClickListener {
            val intent = Intent((activity as HomeScreen), Preferences::class.java)
            startActivity(intent)
        }


        pulltorefresh.setOnRefreshListener(object : OnRefreshListener {
            override fun onRefresh() {
                if (btnmymatches.isSelected == true) {
//                    isloadingfirsttime = false
                    GetMatches()
                } else {
                    GetRecentlyVisitors()
                }
            }
        })

        return view
    }

    fun GetMatches() {
        val progressDialog = ProgressDialog(activity)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        if (!(activity as HomeScreen).isMatchesLoaded) {
            progressDialog.show()
        }

        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )

        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/my-matches",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        if (!(activity as HomeScreen).isMatchesLoaded) {
                            progressDialog.dismiss()
                        }
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            if (!response.isNull("data")) {

                                (activity as HomeScreen).jsonMatchesData =
                                    response.getJSONArray("data")
                                if ((activity as HomeScreen).jsonMatchesData.length() > 0) {
                                    adapter = Adapter_MyMatches(
                                        activity!!,
                                        (activity as HomeScreen).jsonMatchesData,
                                        this@Fragment_Home
                                    )
                                    listMymatches.adapter = adapter
                                    framenodata.visibility = View.GONE
                                    listMymatches.visibility = View.VISIBLE
                                    GetRecentlyVisitors()
                                } else {
                                    framenodata.visibility = View.VISIBLE
                                    tvnodata.setText("No Matches")
                                    listMymatches.visibility = View.GONE
                                    GetRecentlyVisitors()
                                }
                                (activity as HomeScreen).isMatchesLoaded = true
                                if (pulltorefresh.isRefreshing) {
                                    pulltorefresh.isRefreshing = false
                                }
                            }
                        } else if (status == 401) {
                            if (pulltorefresh.isRefreshing) {
                                pulltorefresh.isRefreshing = false
                            }
                            progressDialog.dismiss()
//                            Toast.makeText(
//                                activity,
//                                response.getString("message"),
//                                Toast.LENGTH_SHORT
//                            ).show()
                            GetRecentlyVisitors()

                        } else {
                            progressDialog.dismiss()
                            if (pulltorefresh.isRefreshing) {
                                pulltorefresh.isRefreshing = false
                            }
//                            Toast.makeText(
//                                activity,
//                                response.getString("message"),
//                                Toast.LENGTH_SHORT
//                            ).show()
                            if (pulltorefresh.isRefreshing) {
                                pulltorefresh.isRefreshing = false
                            }
                            GetRecentlyVisitors()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as HomeScreen,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as HomeScreen,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    fun GetRecentlyVisitors() {
//        val progressDialog = ProgressDialog(activity)
//        progressDialog.setTitle("Loading")
//        progressDialog.setMessage("Please Wait...")
//        progressDialog.setCancelable(false)
//        if (isloadingfirsttime) {
//            progressDialog.show()
//        }

        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )

        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/get-recently-visitors",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
//                        if (isloadingfirsttime) {
//                            progressDialog.dismiss()
//                        }
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            if (!response.isNull("data")) {

                                (activity as HomeScreen).jsonRecentVisitors =
                                    response.getJSONArray("data")
                                if (pulltorefresh.isRefreshing) {
                                    if (btnrecentlyvisitors.isSelected) {
                                        if ((activity as HomeScreen).jsonRecentVisitors.length() > 0) {
                                            adapter = Adapter_MyMatches(
                                                activity!!,
                                                (activity as HomeScreen).jsonRecentVisitors,
                                                this@Fragment_Home
                                            )


                                            listMymatches.adapter = adapter
                                            framenodata.visibility = View.GONE
                                            listMymatches.visibility = View.VISIBLE
                                        } else {
                                            framenodata.visibility = View.VISIBLE
                                            tvnodata.setText("No Recently Visitors")
                                            listMymatches.visibility = View.GONE
                                        }
                                    }
//                                (activity as HomeScreen).isMatchesLoaded = true

                                    pulltorefresh.isRefreshing = false
                                }
                            }
                        } else if (status == 401) {
                            if (pulltorefresh.isRefreshing) {
                                pulltorefresh.isRefreshing = false
                            }
//                            progressDialog.dismiss()
//                            Toast.makeText(
//                                activity,
//                                response.getString("message"),
//                                Toast.LENGTH_SHORT
//                            ).show()

                        } else {
//                            progressDialog.dismiss()
                            if (pulltorefresh.isRefreshing) {
                                pulltorefresh.isRefreshing = false
                            }
//                            Toast.makeText(
//                                activity,
//                                response.getString("message"),
//                                Toast.LENGTH_SHORT
//                            ).show()
                            if (pulltorefresh.isRefreshing) {
                                pulltorefresh.isRefreshing = false
                            }
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
//                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
//                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as HomeScreen,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
//                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as HomeScreen,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Home.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Home().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun ListItemCLicked(pos: Int) {
        val intent = Intent(
            (activity as HomeScreen),
            AboutProfile::class.java
        )
        var tempobj = JSONObject()
        if (btnmymatches.isSelected == true) {
            tempobj = (activity as HomeScreen).jsonMatchesData.getJSONObject(pos)
        } else if (btnrecentlyvisitors.isSelected == true) {
            tempobj = (activity as HomeScreen).jsonRecentVisitors.getJSONObject(pos)
        }
//        if (btnmymatches.isSelected == true) {
//            var available = false
//            if ((activity as HomeScreen).jsonRecentVisitors.length() > 0) {
////                var ids = arrayOf(String())
//                var currentid = tempobj.getString("id")
//                for (i in 0 until (activity as HomeScreen).jsonRecentVisitors.length()) {
//                    var obj = (activity as HomeScreen).jsonRecentVisitors.getJSONObject(i)
//                    if (currentid.equals(obj.getString("id"))) {
//                        available = true
//                    }
//                }
//                if (!available) {
//                    (activity as HomeScreen).jsonRecentVisitors.put(tempobj)
//                }
//            } else {
//                (activity as HomeScreen).jsonRecentVisitors.put(tempobj)
//            }
//            var editor = sharedPreferencesLogin.edit()
//            editor.putString(
//                "recentely_visitors",
//                (activity as HomeScreen).jsonRecentVisitors.toString()
//            )
//            editor.commit()
//        }

        intent.putExtra("Data", tempobj.toString())
        startActivity(intent)
    }

    private fun ReportUser(
        its: String
    ) {
        val progressDialog = ProgressDialog(context)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            its
        )
        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                " " + access_token
        )
        val handle = asyncHttpClient.post(
            resources.getString(R.string.Api2) + "user/report",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {

                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                }
            })
    }

    override fun onResume() {
        super.onResume()

        sharedPreferencesLogin =
            activity!!.getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPreferencesLogin.getString("User_Data", ""))


        if (!jsonUserData.getString("profile_picture").isNullOrEmpty()) {
            if (jsonUserData.getString("gender").equals("male")) {
                Picasso.get()
                    .load(jsonUserData.getString("profile_picture"))
                    .fit()
                    .centerCrop()
                    .placeholder(this.resources.getDrawable(R.drawable.icon_logo_grey))
                    .error(this.resources.getDrawable(R.drawable.icon_logo_grey))
                    .transform(
                        CircleTransform(
                            this.resources.getDimension(
                                R.dimen._18sdp
                            )
                                .toInt(), 0
                        )
                    )
                    .into(ivprofilepic)
            } else {
                Picasso.get()
                    .load(jsonUserData.getString("profile_picture"))
                    .fit()
                    .centerCrop()
                    .placeholder(this.resources.getDrawable(R.drawable.icon_logo_grey))
                    .error(this.resources.getDrawable(R.drawable.icon_logo_grey))
                    .transform(
                        CircleTransform(
                            this.resources.getDimension(
                                R.dimen._18sdp
                            )
                                .toInt(), 0
                        )
                    )
                    .into(ivprofilepic)
            }
        }
    }
}
