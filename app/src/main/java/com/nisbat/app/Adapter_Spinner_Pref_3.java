package com.nisbat.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

class Adapter_Spinner_Pref_3 extends BaseAdapter {

    Context mComtext;
    JSONArray data;
    String interestedin;
    SharedPreferences AppPref;
    int selectedpos =10;

    public Adapter_Spinner_Pref_3(Context context, JSONArray jsonarray, SharedPreferences appPref) {

        mComtext = context;
        data =jsonarray;
        AppPref = appPref;
    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(mComtext).inflate(R.layout.item_spinner_pref, parent, false);

        TextView button = convertView.findViewById(R.id.tvitemspinner);
        button.setTextColor(mComtext.getResources().getColor(R.color.color_black));

        try {
            button.setText(data.getJSONObject(position).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
