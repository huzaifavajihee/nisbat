package com.nisbat.app

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Family_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Family_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var strfathersstatus: String = ""
    var strmotherstatus: String = ""
    var strfamilytype: String = ""
    var strfamilyvalues: String = ""
    var straffluencelevel: String = ""

    lateinit var etfamilylocation: EditText
    lateinit var etnativeplace: EditText
    lateinit var etnoofmarriedbrothers: EditText
    lateinit var etnoofunmarriedbrothers: EditText
    lateinit var etnoofmarriedsisters: EditText
    lateinit var etnoofunmarriedsisters: EditText

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_family_info, container, false)
        // Inflate the layout for this fragment

        val spinnerfathersstatus = view.findViewById<Spinner>(R.id.spinnereditfatherstatus)
        val spinnermothersstatus = view.findViewById<Spinner>(R.id.spinnereditmotherstatus)
        etfamilylocation = view.findViewById<EditText>(R.id.eteditfamilylocation)
        etnativeplace = view.findViewById<EditText>(R.id.eteditnativeplace)
        etnoofmarriedbrothers = view.findViewById<EditText>(R.id.eteditnoofmarriedbrothers)
        etnoofunmarriedbrothers = view.findViewById<EditText>(R.id.eteditnoofunmarriedbrothers)
        etnoofmarriedsisters = view.findViewById<EditText>(R.id.eteditnoofmarriedsisters)
        etnoofunmarriedsisters = view.findViewById<EditText>(R.id.eteditnoofunmarriedsisters)
        val spinnerfamilytype = view.findViewById<Spinner>(R.id.spinnereditfamilytype)
        val spinnerfamilyvalues = view.findViewById<Spinner>(R.id.spinnereditfamilyvalues)
        val spinneraffluencelevel = view.findViewById<Spinner>(R.id.spinnereditaffluencelevel)

        (activity as Edit_Profile).fragmentEditFamilyInfo = this@Fragment_Edit_Family_Info
        //set value in spinner
        if (spinnerfathersstatus != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                 resources.getStringArray(R.array.FatherStatus).toMutableList(),null
            )
            spinnerfathersstatus.adapter = adapter
            var tmpstr = jsonUserData.getString("fathers_status").replace("_", " ")
            if (!tmpstr.isNullOrEmpty()) {
                tmpstr = tmpstr.split(" ").joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.FatherStatus).indexOf(tmpstr)
                spinnerfathersstatus.setSelection(pos)
                strfathersstatus = resources.getStringArray(R.array.FatherStatus).get(pos)
            }
            else
            {
                spinnerfathersstatus.setSelection(0)
            }

        }
        if (spinnermothersstatus != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                 resources.getStringArray(R.array.MothersStatus).toMutableList(),null
            )
            spinnermothersstatus.adapter = adapter
            var tmpstr = jsonUserData.getString("mothers_status").replace("_", " ")
            if (!tmpstr.isNullOrEmpty()) {
                tmpstr = tmpstr.split(" ").joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.MothersStatus).indexOf(tmpstr)
                spinnermothersstatus.setSelection(pos)
                strmotherstatus = resources.getStringArray(R.array.MothersStatus).get(pos)
            }
        }

        if (spinnerfamilytype != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                 resources.getStringArray(R.array.FamilyType).toMutableList(),null
            )
            spinnerfamilytype.adapter = adapter
            var tmpstr = jsonUserData.getString("family_type").replace("_", " ")
            if (!tmpstr.isNullOrEmpty()) {
                tmpstr = tmpstr.split(" ").joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.FamilyType).indexOf(tmpstr)
                spinnerfamilytype.setSelection(pos)
                strfamilytype = resources.getStringArray(R.array.FamilyType).get(pos)
            }
        }
        if (spinnerfamilyvalues != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                 resources.getStringArray(R.array.FamilyValues).toMutableList(),null
            )
            spinnerfamilyvalues.adapter = adapter
            var tmpstr = jsonUserData.getString("family_lifestyles").replace("_", " ")
            if (!tmpstr.isNullOrEmpty()) {
                tmpstr = tmpstr.split(" ").joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.FamilyValues).indexOf(tmpstr)
                spinnerfamilyvalues.setSelection(pos)
                strfamilyvalues = resources.getStringArray(R.array.FamilyValues).get(pos)
            }
        }
        if (spinneraffluencelevel != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.AffluenceLevel).toMutableList(),null
            )
            spinneraffluencelevel.adapter = adapter
            var tmpstr = jsonUserData.getString("family_background").replace("_", " ")
            if (!tmpstr.isNullOrEmpty()) {
                tmpstr = tmpstr.split(" ").joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.AffluenceLevel).indexOf(tmpstr)
                spinneraffluencelevel.setSelection(pos)
                straffluencelevel = resources.getStringArray(R.array.AffluenceLevel).get(pos)
            }
        }
        val tempstr = jsonUserData.getString("family_location")
        etfamilylocation.setText(tempstr)

        val tempstr1 = jsonUserData.getString("native_place")
        etnativeplace.setText(tempstr1)


        val tempstr2 = jsonUserData.getString("married_brothers")
        if (!tempstr2.isNullOrEmpty()) {
            etnoofmarriedbrothers.setText(tempstr2)
        } else {
            etnoofmarriedbrothers.setText("0")
        }

        val tempstr4 = jsonUserData.getString("unmarried_brothers")
        if (!tempstr4.isNullOrEmpty()) {
            etnoofunmarriedbrothers.setText(tempstr4)
        } else {
            etnoofunmarriedbrothers.setText("0")
        }

        val tempstr5 = jsonUserData.getString("married_sisters")
        if (!tempstr5.isNullOrEmpty()) {
            etnoofmarriedsisters.setText(tempstr5)
        } else {
            etnoofmarriedsisters.setText("0")
        }


        val tempstr6 = jsonUserData.getString("unmarried_sisters")
        if (!tempstr6.isNullOrEmpty()) {
            etnoofunmarriedsisters.setText(tempstr6)
        } else {
            etnoofunmarriedsisters.setText("0")
        }

        //spinner item click
        spinnerfathersstatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strfathersstatus = resources.getStringArray(R.array.FatherStatus).get(position)
                } else {
                    strfathersstatus = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnermothersstatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strmotherstatus = resources.getStringArray(R.array.MothersStatus).get(position)
                } else {
                    strmotherstatus = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerfamilytype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strfamilytype = resources.getStringArray(R.array.FamilyType).get(position)
                } else {
                    strfamilytype = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerfamilyvalues.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strfamilyvalues = resources.getStringArray(R.array.FamilyValues).get(position)
                } else {
                    strfamilyvalues = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinneraffluencelevel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    straffluencelevel =
                        resources.getStringArray(R.array.AffluenceLevel).get(position)
                } else {
                    straffluencelevel = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Family_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Family_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
