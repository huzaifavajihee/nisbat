package com.nisbat.app

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.Constraints.TAG
import androidx.core.view.get
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.text.SimpleDateFormat

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Register_Basic_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Register_Basic_Info : Fragment() {


    var booleanMaleChecked: Boolean = true
    var booleanfemaleChecked: Boolean = false
    var strprofillecretedby: String = ""
    var strmaritulstatus: String = ""
    var strheight: String = ""
    var strweight: String = ""
    var strhealthproblem: String = ""
    var strdisability: String = ""
    var strbloodgroup: String = ""
    var strdiet: String = ""
    var strlanguage: String = ""
    var strgender: String = ""
    var strdob: String = ""

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor

    lateinit var spinnerprofilefor: Spinner
    lateinit var spinnermaritulstatus: Spinner
    lateinit var spinnerheight: Spinner
    lateinit var spinnerdisbility: Spinner
    lateinit var spinnerhealthproblem: Spinner
    lateinit var spinnerdiet: Spinner
    lateinit var spinnerbloodgroup: Spinner
    lateinit var spinnerlanguage: Spinner

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Register_Info).getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            (activity as Register_Info).getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_basic_info, container, false)
        // Inflate the layout for this fragment

        val btnnext = view.findViewById<Button>(R.id.btnbasicinfonext)
        val ivmalecheckboxout = view.findViewById<ImageView>(R.id.ivregisterbasicinfomaleoutside)
        val ivfemalecheckboxout =
            view.findViewById<ImageView>(R.id.ivregisterbasicinfofemaleoutside)
        val ivmalecheckinside = view.findViewById<ImageView>(R.id.ivregisterbasicinfomaleinside)
        val ivfemalecheckinside = view.findViewById<ImageView>(R.id.ivregisterbasicinfofemaleinside)
        val tvdateofbirth = view.findViewById<TextView>(R.id.tvregisterbasicinfodateofbirth)
        spinnerprofilefor = view.findViewById<Spinner>(R.id.spinnerregisterprofilecreatedby)
        spinnermaritulstatus = view.findViewById<Spinner>(R.id.spinnerregistermaritalstatus)
        spinnerheight = view.findViewById<Spinner>(R.id.spinnerregisterheight)
        spinnerdisbility = view.findViewById<Spinner>(R.id.spinnerregisterdisability)
        spinnerhealthproblem = view.findViewById<Spinner>(R.id.spinnerregisterhealthproblem)
        spinnerdiet = view.findViewById<Spinner>(R.id.spinnerregisterdiet)
        spinnerbloodgroup = view.findViewById<Spinner>(R.id.spinnerregisterbloodgroup)
        spinnerlanguage = view.findViewById<Spinner>(R.id.spinnerregisterlanguage)
        val etweight = view.findViewById<EditText>(R.id.etregisterbodyweight)

        // Setting Values In Spinners
        if (spinnerprofilefor != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.ProfieCreatedBy).toMutableList(), null
            )
            spinnerprofilefor.adapter = adapter

            if (!sharedprefencesReginfo.getString("profile_created_by", "").isNullOrEmpty()) {
                var tmpstrprofilecreatedby =
                    sharedprefencesReginfo.getString("profile_created_by", "")?.replace("_", " ")
                if (tmpstrprofilecreatedby.equals("parent guardian")) {
                    tmpstrprofilecreatedby = "parent/Guardian"
                }
                tmpstrprofilecreatedby =
                    tmpstrprofilecreatedby?.split(" ")?.joinToString { it.capitalize() }
                var pos =
                    resources.getStringArray(R.array.ProfieCreatedBy)
                        .indexOf(tmpstrprofilecreatedby)
                spinnerprofilefor.setSelection(pos)
                strprofillecretedby = resources.getStringArray(R.array.ProfieCreatedBy).get(pos)
            }
        }
        if (spinnermaritulstatus != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.MaritulStatus).toMutableList(), null
            )
            spinnermaritulstatus.adapter = adapter

            if (!sharedprefencesReginfo.getString("marital_status", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("marital_status", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.MaritulStatus).indexOf(tmpstr)
                spinnermaritulstatus.setSelection(pos)
                strmaritulstatus = resources.getStringArray(R.array.MaritulStatus).get(pos)
            }
        }
        if (spinnerheight != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.Height).toMutableList(), null
            )
            spinnerheight.adapter = adapter

            if (!sharedprefencesReginfo.getString("height", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("height", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.Height).indexOf(tmpstr)
                spinnerheight.setSelection(pos)
                strheight = resources.getStringArray(R.array.Height).get(pos)
            }
        }
        if (spinnerdisbility != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.Disbilities).toMutableList(), null
            )
            spinnerdisbility.adapter = adapter

            if (!sharedprefencesReginfo.getString("any_disability", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("any_disability", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.Disbilities).indexOf(tmpstr)
                spinnerdisbility.setSelection(pos)
                strdisability = resources.getStringArray(R.array.Disbilities).get(pos)
            }
        }
        if (spinnerhealthproblem != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.HealthProblems).toMutableList(), null
            )
            spinnerhealthproblem.adapter = adapter

            if (!sharedprefencesReginfo.getString("health_problem", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("health_problem", "")?.replace("_", " ")
                        ?.toLowerCase()
                for (i in 0 until resources.getStringArray(R.array.HealthProblems).size) {
                    var tmp = resources.getStringArray(R.array.HealthProblems).get(i).toLowerCase()
                    if (tmpstr.equals(tmp)) {
                        spinnerhealthproblem.setSelection(i)
                        strhealthproblem = resources.getStringArray(R.array.AnnualIncome).get(i)
                    }
                }
            }
        }
        if (spinnerbloodgroup != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.BloodGroups).toMutableList(), null
            )
            spinnerbloodgroup.adapter = adapter

            if (!sharedprefencesReginfo.getString("blood_group", "").isNullOrEmpty()) {
                var tmpstr = sharedprefencesReginfo.getString("blood_group", "")?.replace("_", " ")
                    ?.toLowerCase()
                if (tmpstr.equals("a positive")) {
                    tmpstr = "a+"
                } else if (tmpstr.equals("a negative")) {
                    tmpstr = "a-"
                } else if (tmpstr.equals("b positive")) {
                    tmpstr = "b+"
                } else if (tmpstr.equals("b negative")) {
                    tmpstr = "b-"
                } else if (tmpstr.equals("o positive")) {
                    tmpstr = "o+"
                } else if (tmpstr.equals("o negative")) {
                    tmpstr = "o-"
                } else if (tmpstr.equals("ab positive")) {
                    tmpstr = "ab+"
                } else if (tmpstr.equals("ab negative")) {
                    tmpstr = "ab-"
                }
                for (i in 0 until resources.getStringArray(R.array.BloodGroups).size) {
                    var tmp = resources.getStringArray(R.array.BloodGroups).get(i).toLowerCase()
                    if (tmpstr.equals(tmp)) {
                        spinnerbloodgroup.setSelection(i)
                        strbloodgroup = resources.getStringArray(R.array.AnnualIncome).get(i)
                    }
                }
            }
        }
        if (spinnerdiet != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.Diet).toMutableList(), null
            )
            spinnerdiet.adapter = adapter

            if (!sharedprefencesReginfo.getString("whats_your_diet", "").isNullOrEmpty()) {
                var tmpstr =
                    sharedprefencesReginfo.getString("whats_your_diet", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.Diet).indexOf(tmpstr)
                spinnerdiet.setSelection(pos)
                strdiet = resources.getStringArray(R.array.Diet).get(pos)
            }
        }
        if (spinnerlanguage != null) {
            val adapter = Adapter_Spinner(
                (activity as Register_Info),
                resources.getStringArray(R.array.Languages).toMutableList(), null
            )
            spinnerlanguage.adapter = adapter

            if (!sharedprefencesReginfo.getString("languages", "").isNullOrEmpty()) {
                var tmpstr = sharedprefencesReginfo.getString("languages", "")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.Languages).indexOf(tmpstr)
                spinnerlanguage.setSelection(pos)
                strlanguage = resources.getStringArray(R.array.Diet).get(pos)
            }
        }

        if (!sharedprefencesReginfo.getString("body_weight_kg", "").isNullOrEmpty()) {
            etweight.setText(sharedprefencesReginfo.getString("body_weight_kg", ""))
        }

        //spinner item click
        spinnerprofilefor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strprofillecretedby =
                        resources.getStringArray(R.array.ProfieCreatedBy).get(position)

                    editorReginfo.putString("profile_created_by", strprofillecretedby)
                    editorReginfo.commit()
                } else {
                    strprofillecretedby = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")

            }
        }
        spinnermaritulstatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strmaritulstatus = resources.getStringArray(R.array.MaritulStatus).get(position)

                    editorReginfo.putString("marital_status", strmaritulstatus)
                    editorReginfo.commit()
                } else {
                    strmaritulstatus = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerheight.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strheight = resources.getStringArray(R.array.Height).get(position)

                    editorReginfo.putString("height", strheight)
                    editorReginfo.commit()
                } else {
                    strheight = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerdisbility.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (position != 0) {
                    strdisability = resources.getStringArray(R.array.Disbilities).get(position)

                    editorReginfo.putString("any_disability", strdisability)
                    editorReginfo.commit()
                } else {
                    strdisability = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerhealthproblem.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (position != 0) {
                    strhealthproblem =
                        resources.getStringArray(R.array.HealthProblems).get(position)

                    editorReginfo.putString("health_problem", strhealthproblem)
                    editorReginfo.commit()
                } else {
                    strhealthproblem = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerbloodgroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strbloodgroup = resources.getStringArray(R.array.BloodGroups).get(position)

                    editorReginfo.putString("blood_group", strbloodgroup)
                    editorReginfo.commit()
                } else {
                    strbloodgroup = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerdiet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strdiet = resources.getStringArray(R.array.Diet).get(position)

                    editorReginfo.putString("whats_your_diet", strdiet)
                    editorReginfo.commit()
                } else {
                    strdiet = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerlanguage.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strlanguage = resources.getStringArray(R.array.Languages).get(position)

                    editorReginfo.putString("languages", strlanguage)
                    editorReginfo.commit()
                } else {
                    strlanguage = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        if (jsonUserData.getString("gender").equals("male")) {
            ivmalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivmalecheckinside.visibility = View.VISIBLE
            ivfemalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivfemalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = true
            booleanfemaleChecked = false
            strgender = "male"
        } else if (jsonUserData.getString("gender").equals("female")) {
            ivfemalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivfemalecheckinside.visibility = View.VISIBLE
            ivmalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivmalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = true
            booleanfemaleChecked = false
            strgender = "female"
        } else {
            ivmalecheckboxout.setBackgroundResource(R.drawable.rounded_white_button_7)
            ivmalecheckinside.visibility = View.VISIBLE
            ivfemalecheckboxout.setBackgroundResource(R.drawable.frame_white_7)
            ivfemalecheckinside.visibility = View.INVISIBLE
            booleanMaleChecked = true
            booleanfemaleChecked = false
            strgender = "male"
        }

        if (!jsonUserData.getString("dob").equals("") ||
            !jsonUserData.getString("dob").equals("Date of birth *")
        ) {
            val tempstr1 = jsonUserData.getString("dob")
            strdob = tempstr1.toString()
            var dateformat = SimpleDateFormat("yyyy-MM-dd")
            var date = dateformat.parse(strdob)
            dateformat = SimpleDateFormat("dd-MM-yyyy")
            strdob = dateformat.format(date)
            tvdateofbirth.setText(strdob)
        }

        //button next clicked
        btnnext.setOnClickListener {

            strweight = etweight.text.toString()

            if (booleanMaleChecked || booleanfemaleChecked) {
                if (!strprofillecretedby.isNullOrEmpty() && !strmaritulstatus.isNullOrEmpty() && !strdob.isNullOrEmpty()
                    && !strheight.isNullOrEmpty() && !strdisability.isNullOrEmpty() && !strhealthproblem.isNullOrEmpty()
                ) {
                    if (strbloodgroup.equals("A+")) {
                        strbloodgroup = "a_positive"
                    } else if (strbloodgroup.equals("A-")) {
                        strbloodgroup = "a_negative"
                    } else if (strbloodgroup.equals("B+")) {
                        strbloodgroup = "b_positive"
                    } else if (strbloodgroup.equals("B-")) {
                        strbloodgroup = "b_negative"
                    } else if (strbloodgroup.equals("O-")) {
                        strbloodgroup = "o_negative"
                    } else if (strbloodgroup.equals("O+")) {
                        strbloodgroup = "o_positive"
                    } else if (strbloodgroup.equals("AB-")) {
                        strbloodgroup = "ab_negative"
                    } else if (strbloodgroup.equals("AB+")) {
                        strbloodgroup = "ab_positive"
                    }
                    editorReginfo.putString("blood_group", strprofillecretedby)
                    editorReginfo.commit()
//                callAPI()

                    editorReginfo.putString(
                        "profile_created_by",
                        strprofillecretedby.replace("/", "_")
                    )
                    editorReginfo.putString("gender", strgender)
                    editorReginfo.putString("dob", strdob)
                    editorReginfo.putString("marital_status", strmaritulstatus.replace(" ", "_"))
                    editorReginfo.putString("height", strheight)
                    editorReginfo.putString("body_weight_kg", strweight)
                    editorReginfo.putString("any_disability", strdisability.replace(" ", "_"))
                    editorReginfo.putString("health_problem", strhealthproblem.replace(" ", "_"))
                    editorReginfo.putString("blood_group", strbloodgroup)
                    editorReginfo.putString("whats_your_diet", strdiet.replace(" ", "_"))
                    editorReginfo.putString("languages", strlanguage)
                    editorReginfo.commit()

                    (activity as Register_Info).ShowCareerInfo()
                } else {
                    if (strprofillecretedby.isNullOrEmpty()) {
                        Toast.makeText(
                            (activity as Register_Info),
                            "Please select \"Profile created by\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (strmaritulstatus.isNullOrEmpty()) {
                        Toast.makeText(
                            (activity as Register_Info),
                            "Please select \"Marital status\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (strhealthproblem.isNullOrEmpty()) {
                        Toast.makeText(
                            (activity as Register_Info),
                            "Please select \"Health problem\" (If you have any)",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (strdisability.isNullOrEmpty()) {
                        Toast.makeText(
                            (activity as Register_Info),
                            "Please select \"Disability\" (If you have any)",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (strdob.isNullOrEmpty()) {
                        Toast.makeText(
                            (activity as Register_Info),
                            "Please select your \"Date of birth\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (strheight.isNullOrEmpty()) {
                        Toast.makeText(
                            (activity as Register_Info),
                            "Please select your \"Height\"",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else {
                if (!booleanfemaleChecked && !booleanMaleChecked) {
                    Toast.makeText(
                        (activity as Register_Info),
                        "Please select \"Gender\"",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Register_Basic_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Register_Basic_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onResume() {
        super.onResume()
    }
}
