package com.nisbat.app

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.Constraints
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Register_Your_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Register_Your_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var strinfo: String = ""
    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Register_Info).getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            (activity as Register_Info).getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_your_info, container, false)

        val btnsubmit = view.findViewById<Button>(R.id.btnyourinfosubmit)
        val btnback = view.findViewById<Button>(R.id.btnyourinfoback)
        val etinfo = view.findViewById<EditText>(R.id.etregisteryourinfoaboutyourself)

        if (!sharedprefencesReginfo.getString("about_me", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("about_me", "")
            etinfo.setText(tmpstr)
        }

        btnsubmit.setOnClickListener {
            strinfo = etinfo.text.toString()
            if (!strinfo.isNullOrEmpty()) {
                editorReginfo.putString("about_me", strinfo)
                editorReginfo.commit()
                val cm =
                    (activity as Register_Info).getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
                if (isConnected) {
                    (activity as Register_Info).FinalSubmitButtonClicked()
                } else {
                    Toast.makeText(
                        activity as Register_Info,
                        "You Are Not Connected To Network",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    (activity as Register_Info),
                    "Please write something about yourself",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        btnback.setOnClickListener {
            editorReginfo.putString("about_me", strinfo)
            editorReginfo.commit()
            (activity as Register_Info).ShowContactInfo()
        }
        // Inflate the layout for this fragment
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Register_Your_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Register_Your_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}