package com.nisbat.app

import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.SharedPreferences
import android.graphics.ColorSpace.Model
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.billingclient.api.*
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Subscription.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Subscription : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var billingClient: BillingClient? = null
    private var skuDetails: SkuDetails? = null
    lateinit var listofitems: List<SkuDetails>

    lateinit var listView: ListView

    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_subscription, container, false)

        listView = view.findViewById(R.id.listsubscription)
        progressDialog = ProgressDialog((activity as Edit_Profile))
//        progressDialog.setTitle("Please Wait...")
        progressDialog.setMessage("Getting Plans...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        // Inflate the layout for this fragment

        billingClient = BillingClient.newBuilder(activity as Edit_Profile)
            .setListener(purchaseUpdateListener)
            .enablePendingPurchases()
            .build()

        billingClient!!.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {

                    // The BillingClient is ready. You can query purchases here.
                    querySkuDetails()
                    val purchases = billingClient!!.queryPurchases(BillingClient.SkuType.SUBS)
                        .purchasesList

                    handleItemAlreadyPurchase(purchases)
                }
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        })
        return view
    }

    private fun handleItemAlreadyPurchase(purchases: List<Purchase>?) {

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Subscription.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Subscription().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private val purchaseUpdateListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            Log.v("TAG_INAPP", "billingResult responseCode : ${billingResult.responseCode}")

            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                for (purchase in purchases) {
// handlePurchase(purchase)
                    handleConsumedPurchases(purchase)
                }
            } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
// Handle an error caused by a user cancelling the purchase flow.
            } else {
// Handle any other error codes.
            }
        }

    private fun handleConsumedPurchases(purchase: Purchase) {
        Log.d("TAG_INAPP", "handleConsumablePurchasesAsync foreach it is $purchase")
        val params =
            ConsumeParams.newBuilder().setPurchaseToken(purchase.purchaseToken).build()
        billingClient?.consumeAsync(params) { billingResult, purchaseToken ->
            when (billingResult.responseCode) {
                BillingClient.BillingResponseCode.OK -> {
// Update the appropriate tables/databases to grant user the items
                    Log.d(
                        "TAG_INAPP",
                        " Update the appropriate tables/databases to grant user the items"
                    )
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Payment Successfull.",
                        Toast.LENGTH_SHORT
                    ).show()
                    CallPurchaseAPI()
                }
                else -> {
                    Log.w("TAG_INAPP", billingResult.debugMessage)
                    Toast.makeText(
                        (activity as Edit_Profile),
                        billingResult.debugMessage,
                        Toast.LENGTH_SHORT
                    ).show()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "handleConsumedPurchases Method else Block",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
    }
    private fun CallPurchaseAPI() {

        val progressDialog=ProgressDialog(activity as Edit_Profile)
        progressDialog.setMessage("Please Wait...")
//        progressDialog.setTitle("Payment...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        var loginPreferences: SharedPreferences = (activity as Edit_Profile).getSharedPreferences(
            "pref_login_user",
            0
        )
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))

        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            User_Data.getString("its_card_number")
        )
        requestParams.add("plan_name", "three_months")
        requestParams.add("months", "3")

        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            (activity as Edit_Profile).resources.getString(R.string.Api2) + "user/update-plan",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {

                            var jsonObject = JSONObject(response.getString("data"))
                            User_Data.put("is_pro", jsonObject.getString("is_pro"))
                            User_Data.put(
                                "plan_start_date",
                                jsonObject.getString("plan_start_date")
                            )
                            User_Data.put("plan_end_date", jsonObject.getString("plan_end_date"))
                            User_Data.put("plan_name", jsonObject.getString("plan_name"))

                            var editor = loginPreferences.edit()
                            editor.putString("User_Data", User_Data.toString())
                            editor.commit()
                            progressDialog.dismiss()

                            (activity as Edit_Profile).finish()
                        } else {
//
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
//                    progressDialog.dismiss()
                    Toast.makeText(
                        context,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressDialog.dismiss()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
//                    progressDialog.dismiss()
                    Toast.makeText(
                        context,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressDialog.dismiss()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
//                    progressDialog.dismiss()
                    Toast.makeText(
                        context,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressDialog.dismiss()
                }
            })
    }


    private fun querySkuDetails() {
        val skulist = listOf(
            "nisbat_12_months",
            "nisbat_6_months",
            "nisbat_3_month",
            "nisbat_1_months",
            "test_subscription"
        )
        val params = SkuDetailsParams.newBuilder()
            .setSkusList(skulist)
            .setType(BillingClient.SkuType.SUBS)
            .build()
//        withContext(Dispatchers.IO) {
        billingClient!!.querySkuDetailsAsync(params) { billingResult, skuDetailsList ->
            // Process the result.
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                Log.d(TAG, "querySkuDetails:::   " + skuDetailsList)

                if (skuDetailsList != null) {
                    listofitems = skuDetailsList
                    Collections.sort(listofitems, object : Comparator<SkuDetails> {
//                        fun compare(lhs: Model, rhs: Model): Int {
//                            return lhs.getName().compareTo(rhs.getName())
//                        }

                        override fun compare(o1: SkuDetails?, o2: SkuDetails?): Int {
                            return o1?.priceAmountMicros?.compareTo(o2?.priceAmountMicros!!)!!
                        }
                    })
                    UpdateUI(listofitems)
                } else {
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "No Subscription Plans Found ! ",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }

        }
//        }
    }

    private fun UpdateUI(skuDetailsList: List<SkuDetails>?) {

        val adapter =
            Adapter_Subscription((activity as Edit_Profile), skuDetailsList, billingClient)

        listView.adapter = adapter

        progressDialog.dismiss()

    }


}
