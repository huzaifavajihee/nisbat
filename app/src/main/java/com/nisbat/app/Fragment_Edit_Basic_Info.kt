package com.nisbat.app

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.JsonWriter
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory.TAG
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Edit_Basic_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Edit_Basic_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var strprofilecreatedby: String = ""
    var strbloodgroup: String = ""
    var strdateofbirth: String = ""
    var strdiet: String = ""
    var strdisability: String = ""
    var strhealthproblem: String = ""
    var strheight: String = ""
    var strmaritalstatus: String = ""
    var strweight: String = ""

    lateinit var etweight: EditText
    lateinit var tvdob: TextView

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        sharedPrefencesLogin =
            (activity as Edit_Profile).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_basic_info, container, false)
        // Inflate the layout for this fragment

        val spinnercreatedby = view.findViewById<Spinner>(R.id.spinnereditprofilecreatedby)
        tvdob = view.findViewById<TextView>(R.id.tveditbasicinfodateofbirth) as TextView
        val spinnermaritalstatus = view.findViewById<Spinner>(R.id.spinnereditmaritalstatus)
        val spinnerheight = view.findViewById<Spinner>(R.id.spinnereditheight)
        etweight = view.findViewById<EditText>(R.id.eteditbodyweight) as EditText
        val spinnerdisability = view.findViewById<Spinner>(R.id.spinnereditdisability)
        val spinnerhealthproblem = view.findViewById<Spinner>(R.id.spinneredithealthproblem)
        val spinnerbloodgroup = view.findViewById<Spinner>(R.id.spinnereditbloodgroup)
        val spinnerdiet = view.findViewById<Spinner>(R.id.spinnereditdiet)
        val lineardob = view.findViewById<FrameLayout>(R.id.lineareditbasicinfodateofbirth)
        val framedatepicker = view.findViewById<FrameLayout>(R.id.frameeditinfodatepicker)
        val btndpok = view.findViewById<Button>(R.id.btneditinfodatepickerok)
        val btndpcancel = view.findViewById<Button>(R.id.btneditinfodatepickercancel)
        val dp = view.findViewById<DatePicker>(R.id.datepickereditinfo)

        (activity as Edit_Profile).fragmentEditBasicInfo = this@Fragment_Edit_Basic_Info


        // setting Values in layout from preferences
        if (spinnercreatedby != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.ProfieCreatedBy).toMutableList(),
                null
            )
            spinnercreatedby.adapter = adapter
            var tmpstrprofilecreatedby =
                jsonUserData.getString("profile_created_by")?.replace("_", " ")
            if (tmpstrprofilecreatedby.equals("parent guardian")) {
                tmpstrprofilecreatedby = "parent/Guardian"
            }
            tmpstrprofilecreatedby =
                tmpstrprofilecreatedby?.split(" ")?.joinToString { it.capitalize() }
            var pos =
                resources.getStringArray(R.array.ProfieCreatedBy).indexOf(tmpstrprofilecreatedby)
            spinnercreatedby.setSelection(pos)
            strprofilecreatedby = resources.getStringArray(R.array.ProfieCreatedBy).get(pos)
        }

        if (spinnermaritalstatus != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.MaritulStatus).toMutableList(), null
            )
            spinnermaritalstatus.adapter = adapter
            var tmpstr = jsonUserData.getString("marital_status")?.replace("_", " ")
            tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
            var pos = resources.getStringArray(R.array.MaritulStatus).indexOf(tmpstr)
            spinnermaritalstatus.setSelection(pos)
            strmaritalstatus = resources.getStringArray(R.array.MaritulStatus).get(pos)
        }

        if (spinnerheight != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.Height).toMutableList(), null
            )
            spinnerheight.adapter = adapter
            var tmpstr =
                jsonUserData.getString("height")?.replace("_", " ")
            tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
            var pos = resources.getStringArray(R.array.Height).indexOf(tmpstr)
            spinnerheight.setSelection(pos)
            strheight = resources.getStringArray(R.array.Height).get(pos)
        }

        val tempstr = jsonUserData.getString("body_weight_kg")
        etweight?.setText(tempstr)
        strweight = tempstr.toString()

        val tempstr1 = jsonUserData.getString("dob")
        strdateofbirth = tempstr1.toString()
        var datesplit = strdateofbirth.split("-")
        var dateformat = SimpleDateFormat("yyyy-MM-dd")
        var date = dateformat.parse(strdateofbirth)
        dp.updateDate(datesplit[0].toInt(), datesplit[1].toInt(), datesplit[2].toInt())
        dateformat = SimpleDateFormat("dd-MM-yyyy")
        strdateofbirth = dateformat.format(date)
        tvdob.setText(strdateofbirth)

        if (spinnerdisability != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.Disbilities).toMutableList(), null
            )
            spinnerdisability.adapter = adapter
            var tmpstr = jsonUserData.getString("any_disability")?.replace("_", " ")
            tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
            var pos = resources.getStringArray(R.array.Disbilities).indexOf(tmpstr)
            spinnerdisability.setSelection(pos)
            strdisability = resources.getStringArray(R.array.Disbilities).get(pos)
        }

        if (spinnerhealthproblem != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.HealthProblems).toMutableList(), null
            )
            spinnerhealthproblem.adapter = adapter
            var tmpstr = jsonUserData.getString("health_problem")?.replace("_", " ")?.toLowerCase()
            for (i in 0 until resources.getStringArray(R.array.HealthProblems).size) {
                var tmp = resources.getStringArray(R.array.HealthProblems).get(i).toLowerCase()
                if (tmpstr.equals(tmp)) {
                    spinnerhealthproblem.setSelection(i)
                    strhealthproblem = resources.getStringArray(R.array.AnnualIncome).get(i)
                }
            }
        }

        if (spinnerbloodgroup != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.BloodGroups).toMutableList(), null
            )
            spinnerbloodgroup.adapter = adapter
            var tmpstr = jsonUserData.getString("blood_group")?.replace("_", " ")?.toLowerCase()
            if (!tmpstr.isNullOrEmpty()) {
                if (tmpstr.equals("a positive")) {
                    tmpstr = "a+"
                } else if (tmpstr.equals("a negative")) {
                    tmpstr = "a-"
                } else if (tmpstr.equals("b positive")) {
                    tmpstr = "b+"
                } else if (tmpstr.equals("b negative")) {
                    tmpstr = "b-"
                } else if (tmpstr.equals("o positive")) {
                    tmpstr = "o+"
                } else if (tmpstr.equals("o negative")) {
                    tmpstr = "o-"
                } else if (tmpstr.equals("ab positive")) {
                    tmpstr = "ab+"
                } else if (tmpstr.equals("ab negative")) {
                    tmpstr = "ab-"
                }
                for (i in 0 until resources.getStringArray(R.array.BloodGroups).size) {
                    var tmp = resources.getStringArray(R.array.BloodGroups).get(i).toLowerCase()
                    if (tmpstr.equals(tmp)) {
                        spinnerbloodgroup.setSelection(i)
                        strbloodgroup = resources.getStringArray(R.array.AnnualIncome).get(i)
                    }
                }
            }
        }

        if (spinnerdiet != null) {
            val adapter = Adapter_Spinner(
                (activity as Edit_Profile),
                resources.getStringArray(R.array.Diet).toMutableList(), null
            )
            spinnerdiet.adapter = adapter
            if (!jsonUserData.getString("whats_your_diet").isNullOrEmpty()) {
                var tmpstr = jsonUserData.getString("whats_your_diet")?.replace("_", " ")
                tmpstr = tmpstr?.split(" ")?.joinToString(" ") { it.capitalize() }
                var pos = resources.getStringArray(R.array.Diet).indexOf(tmpstr)
                spinnerdiet.setSelection(pos)
                strdiet = resources.getStringArray(R.array.Diet).get(pos)
            }
        }

        //item Click Events
        lineardob.setOnClickListener {
            framedatepicker.visibility = View.VISIBLE

        }
        btndpcancel.setOnClickListener {
            framedatepicker.visibility = View.INVISIBLE
        }
        btndpok.setOnClickListener {
            framedatepicker.visibility = View.INVISIBLE
            val day: Int = dp.getDayOfMonth()
            val month: Int = dp.getMonth()
            val year: Int = dp.getYear()
            val calendar: Calendar = Calendar.getInstance()
            calendar.set(year, month, day)

            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val formatedDate: String = sdf.format(calendar.getTime())

            if (getAge(year, month, day) >= 18) {
                tvdob.setText(formatedDate)
                strdateofbirth = formatedDate
                framedatepicker.visibility = View.INVISIBLE
                tvdob.text = strdateofbirth
            } else {
                Toast.makeText((activity as Edit_Profile), "Age must be 18+", Toast.LENGTH_SHORT)
                    .show()
                strdateofbirth = ""
                tvdob.setText(strdateofbirth)
            }
        }

        spinnercreatedby.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strprofilecreatedby =
                        resources.getStringArray(R.array.ProfieCreatedBy).get(position)
                } else {
                    strprofilecreatedby = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnermaritalstatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strmaritalstatus = resources.getStringArray(R.array.MaritulStatus).get(position)
                } else {
                    strmaritalstatus = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerheight.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strheight = resources.getStringArray(R.array.Height).get(position)
                } else {
                    strheight = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerdisability.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strdisability = resources.getStringArray(R.array.Disbilities).get(position)
                } else {
                    strdisability = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerhealthproblem.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strhealthproblem =
                        resources.getStringArray(R.array.HealthProblems).get(position)
                } else {
                    strhealthproblem = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerbloodgroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strbloodgroup = resources.getStringArray(R.array.BloodGroups).get(position)
                } else {
                    strbloodgroup = ""
                }
                if (strbloodgroup.equals("A+")) {
                    strbloodgroup = "a_positive"
                } else if (strbloodgroup.equals("A-")) {
                    strbloodgroup = "a_negative"
                } else if (strbloodgroup.equals("B+")) {
                    strbloodgroup = "b_positive"
                } else if (strbloodgroup.equals("B-")) {
                    strbloodgroup = "b_negative"
                } else if (strbloodgroup.equals("O-")) {
                    strbloodgroup = "o_negative"
                } else if (strbloodgroup.equals("O+")) {
                    strbloodgroup = "o_positive"
                } else if (strbloodgroup.equals("AB-")) {
                    strbloodgroup = "ab_negative"
                } else if (strbloodgroup.equals("AB+")) {
                    strbloodgroup = "ab_positive"
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        spinnerdiet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    strdiet = resources.getStringArray(R.array.Diet).get(position)
                } else {
                    strdiet = ""
                }
                val textView = view?.findViewById<TextView>(R.id.tvitemspinner)
                if (textView != null) {
                    textView.setTextColor(resources.getColor(R.color.color_white))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Edit_Basic_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Edit_Basic_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun getAge(_year: Int, _month: Int, _day: Int): Int {
        val cal = GregorianCalendar()
        val y: Int
        val m: Int
        val d: Int
        var a: Int
        y = cal[Calendar.YEAR]
        m = cal[Calendar.MONTH]
        d = cal[Calendar.DAY_OF_MONTH]
        cal[_year, _month] = _day
        a = y - cal[Calendar.YEAR]
        if (m < cal[Calendar.MONTH]
            || m == cal[Calendar.MONTH] && d < cal[Calendar.DAY_OF_MONTH]
        ) {
            --a
        }
        require(a >= 0) { "Age < 0" }
        return a
    }
}
