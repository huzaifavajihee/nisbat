package com.nisbat.app

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject


class Adapter_MyMatches(
    val context: Context,
    val datasource: JSONArray,
    val fragment_Home: Fragment_Home,

    ) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    //lateinit var btnmenu:ImageButton

    override fun getCount(): Int {
        //TODO("Not yet implemented")
        return datasource.length()

    }

    override fun getItem(p0: Int): Any {
        //TODO("Not yet implemented")
        return datasource.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        //TODO("Not yet implemented")
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))

        val view = inflater.inflate(R.layout.item_list_home_mymatches, p2, false)
        val imageView = view.findViewById<ImageView>(R.id.ivmymatcheslistitem)
        val tvnameage = view.findViewById<TextView>(R.id.tvmymatchelistitemnameandage)
        val tvprofession = view.findViewById<TextView>(R.id.tvmymatchelistitemprofession)
        val tvaddress = view.findViewById<TextView>(R.id.tvmymatchelistitemaddress)
        val btnconnectnow = view.findViewById<Button>(R.id.btnmymatchesconnectnow)
        val btnmenu = view.findViewById<ImageView>(R.id.ibtnmymatchesimagemenu)
        val tvimagecount = view.findViewById<TextView>(R.id.tvmatchitemimagecount)

        var tempObj = datasource.getJSONObject(p0)
        var name = tempObj.getString("first_name") + " " + tempObj.getString("last_name")
        var age = ""
        if (tempObj.has("age")) {
            age = tempObj.getString("age")
        }
        var profession = ""
        if (tempObj.has("qualification")) {
            tempObj.getString("qualification")
        }
        var address = ""
//        if (!tempObj.getString("family_location").isNullOrEmpty()) {
//            address = address + tempObj.getString("family_location")
//        }
        if (tempObj.has("image_count") && !tempObj.getString("image_count").isNullOrEmpty()) {
            tvimagecount.setText(tempObj.getString("image_count"))
        } else {
            tvimagecount.setText("0")
        }
        if (tempObj.has("city") && !tempObj.getString("city").isNullOrEmpty()) {
            if (!address.isNullOrEmpty()) {
                address = address + ", " + tempObj.getString("city")
            } else {
                address = address + tempObj.getString("city")
            }
        }
        if (tempObj.has("state") && !tempObj.getString("state").isNullOrEmpty()) {
            if (!address.isNullOrEmpty()) {
                address = address + ", " + tempObj.getString("state")
            } else {
                address = address + tempObj.getString("state")
            }
        }
        if (tempObj.has("country") && !tempObj.getString("country").isNullOrEmpty()) {
            if (!address.isNullOrEmpty()) {
                address = address + ", " + tempObj.getString("country")
            } else {
                address = address + tempObj.getString("country")
            }
        }
        if (tempObj.has("occupation") && !tempObj.getString("occupation").isNullOrEmpty()) {
            tvprofession.setText(tempObj.getString("occupation"))
        }
        tvaddress.setText(address)
        tvnameage.setText(name + ", " + age)
//        tvprofession.setText(profession)
        tvaddress.setText(address)

        if (tempObj.has("profile_picture") && !tempObj.getString("profile_picture")
                .isNullOrEmpty()
        ) {
            var imageurl=tempObj.getString("profile_picture")
            imageurl=imageurl.replace("http:","https:")
            if (User_Data.getString("gender").equals("male")) {
                Picasso.get()
                    .load(imageurl)
                    .placeholder(R.drawable.placeholder_female)
                    .error(R.drawable.placeholder_female)
                    .transform(
                        CircleTransform(
                            context.getResources().getDimension(R.dimen._12sdp).toInt(),
                            0
                        )
                    )
                    .fit()
                    .centerCrop()
                    .into(imageView)
            } else {
                Picasso.get()
                    .load(imageurl)
                    .placeholder(R.drawable.placeholder_male)
                    .error(R.drawable.placeholder_male)
                    .transform(
                        CircleTransform(
                            context.getResources().getDimension(R.dimen._12sdp).toInt(),
                            0
                        )
                    )
                    .fit()
                    .centerCrop()
                    .into(imageView)
            }

        } else {
            if (User_Data.getString("gender").equals("male")) {

//                imageView.setBackgroundResource(R.drawable.placeholder_female)
                Picasso.get()
                    .load(R.drawable.placeholder_female)
//                    .placeholder(R.drawable.placeholder_female)
//                    .error(R.drawable.placeholder_female)
                    .transform(
                        CircleTransform(
                            context.getResources().getDimension(R.dimen._12sdp).toInt(),
                            0
                        )
                    )
                    .fit()
                    .centerCrop()
                    .into(imageView)
            } else {
//                imageView.setBackgroundResource(R.drawable.placeholder_male)
                Picasso.get()
                    .load(R.drawable.placeholder_male)
//                    .placeholder(R.drawable.placeholder_male)
//                    .error(R.drawable.placeholder_male)
                    .transform(
                        CircleTransform(
                            context.getResources().getDimension(R.dimen._12sdp).toInt(),
                            0
                        )
                    )
                    .fit()
                    .centerCrop()
                    .into(imageView)
            }
        }

        imageView.setOnClickListener {
            fragment_Home.ListItemCLicked(p0)
        }


        btnconnectnow.setOnClickListener {
            CallConnectNow(datasource.getJSONObject(p0))
        }

        btnmenu.setOnClickListener {
            val popup = PopupMenu(context, btnmenu)
            val inflater = popup.menuInflater
            inflater.inflate(R.menu.popupmenu, popup.menu)
            popup.show()

            popup.setOnMenuItemClickListener {
                if (it.itemId.equals(R.id.ReportMenuItem)) {
                    if (fragment_Home.btnrecentlyvisitors.isSelected) {
                        ReportUser(tempObj.getString("viewer_id"))
                    } else {
                        ReportUser(tempObj.getString("id"))
                    }
                }
                return@setOnMenuItemClickListener true
            }
        }


        return view
    }

    private fun CallConnectNow(jsonObject: JSONObject?) {
        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var User_Data: JSONObject = JSONObject(loginPreferences.getString("User_Data", ""))
        var oppItsNumber = jsonObject?.getString("its_card_number")
        var ownItsNumber = User_Data.getString("its_card_number")
        var type = "pending"
        var actionType = "connect_now"
        val progressDialog = ProgressDialog(context)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "receiver_its_card_number",
            oppItsNumber
        )
        requestParams.add("sender_its_card_number", ownItsNumber)
        requestParams.add("type", type)
        requestParams.add("action_type", actionType)

        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            context.getString(R.string.Api2) + "user/activity-type-update",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            if (fragment_Home.btnrecentlyvisitors.isSelected) {
                                fragment_Home.GetRecentlyVisitors()
                            } else if (fragment_Home.btnmymatches.isSelected) {
                                fragment_Home.GetMatches()
                            }
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun ReportUser(
        targetId: String
    ) {

        var loginPreferences: SharedPreferences = context.getSharedPreferences("pref_login_user", 0)
        var id = fragment_Home.jsonUserData.getString("id")
        val progressDialog = ProgressDialog(context)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val requestParams = RequestParams()
        requestParams.add(
            "parent_id",
            id
        )
        requestParams.add(
            "target_id",
            targetId
        )
        requestParams.add(
            "text",
            "Sorry For That !"
        )
        val asyncHttpClient = AsyncHttpClient()

        var token_type = loginPreferences.getString("token_type", "")
        var access_token = loginPreferences.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            context.getString(R.string.Api2) + "user/report-user",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        progressDialog.dismiss()
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(context, "Internal Server Error !", Toast.LENGTH_SHORT).show()
                }
            })
    }

}
