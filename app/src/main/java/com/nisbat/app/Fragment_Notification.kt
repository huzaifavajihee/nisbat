package com.nisbat.app

import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.FrameLayout
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.fragment_notification.*
import org.json.JSONArray
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Notification.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Notification : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var jsonUserData: JSONObject
    lateinit var sharedPreferencesLogin: SharedPreferences

    var isloadingfirsttime = true
    lateinit var adapter: Adapter_Notification
    lateinit var lvnotifications:ListView
    lateinit var framenodata:FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPreferencesLogin =
            activity!!.getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPreferencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view=inflater.inflate(R.layout.fragment_notification, container, false)
        // Inflate the layout for this fragment

        lvnotifications=view.findViewById<ListView>(R.id.Lvnotifications)
        framenodata=view.findViewById(R.id.framenonotifications)

        if (!(activity as Edit_Profile).isNotificationLoaded) {
            GetNotifications()
        }

        lvnotifications.setOnItemClickListener(object :AdapterView.OnItemClickListener{
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var tempobj=(activity as Edit_Profile).jsonNotificationData.getJSONObject(position)
                val intent = Intent(
                    (activity as Edit_Profile),
                    AboutProfile::class.java
                )
                intent.putExtra("Data", tempobj.toString())
                startActivity(intent)
            }

        })
        return view
    }

    private fun GetNotifications() {
        val progressDialog = ProgressDialog(activity)
//        progressDialog.setTitle("Loading")
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        if (isloadingfirsttime) {
            progressDialog.show()
        }

        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )

        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPreferencesLogin.getString("token_type", "")
        var access_token = sharedPreferencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/get-notification",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {
                        if (isloadingfirsttime) {
                            progressDialog.dismiss()
                        }
                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            if (!response.isNull("data")) {

                                (activity as Edit_Profile).jsonNotificationData =
                                    response.getJSONArray("data")
                                if ((activity as Edit_Profile).jsonNotificationData.length() > 0) {
                                    if (isloadingfirsttime) {
                                        adapter = Adapter_Notification(
                                            activity!!,
                                            (activity as Edit_Profile).jsonNotificationData,
                                        )
                                        isloadingfirsttime = false
                                    } else {
                                        adapter.notifyDataSetChanged()
                                    }

                                    lvnotifications.adapter = adapter
                                    framenodata.visibility = View.GONE
                                    lvnotifications.visibility = View.VISIBLE
                                } else {
                                    framenodata.visibility = View.VISIBLE
                                    lvnotifications.visibility = View.GONE
                                }
                                (activity as Edit_Profile).isNotificationLoaded = true
                            }
                        } else if (status == 401) {
                            progressDialog.dismiss()
                            Toast.makeText(
                                activity,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()

                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(
                                activity,
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        (activity as Edit_Profile),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as Edit_Profile,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    progressDialog.dismiss()
                    Toast.makeText(
                        activity as Edit_Profile,
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Notification.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Notification().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}