package com.nisbat.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.json.JSONObject
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_About_Profile_Basic_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_About_Profile_Basic_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var UserData: JSONObject = JSONObject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_basic_info_about_profile, container, false)

        val tvageheight = view.findViewById<TextView>(R.id.tvaboutprofilebasicageheight)
        val tvmaritalstatus = view.findViewById<TextView>(R.id.tvaboutprofilebasicmaritulstatus)
        val tvlocation = view.findViewById<TextView>(R.id.tvaboutprofilebasiclocation)
        val tvprofession = view.findViewById<TextView>(R.id.tvaboutprofilebasicprofession)
        val tvfood = view.findViewById<TextView>(R.id.tvaboutprofilebasicfood)
        val tvlanguage = view.findViewById<TextView>(R.id.tvaboutprofilebasiclanguages)

        UserData = (activity as AboutProfile).jsonUserData

        var fullheight = UserData.getString("height").split("-")
        var height = fullheight[0].replace("in", "").replace("ft", "\'").replace(" ", "")
        var dob = UserData.getString("dob").split("-")
        var age = getAge(dob[0].toInt(), dob[1].toInt(), dob[2].toInt())
        var maritalstatus = UserData.getString("marital_status")
        var address = "Lives in "
        if (!UserData.getString("family_location").isNullOrEmpty()) {
            address = address + UserData.getString("family_location")
        }
        if (!UserData.getString("city").isNullOrEmpty()) {
            if (!address.isNullOrEmpty()) {
                address = address + ", " + UserData.getString("city")
            } else {
                address = address + UserData.getString("city")
            }
        }
        if (!UserData.getString("state").isNullOrEmpty()) {
            if (!address.isNullOrEmpty()) {
                address = address + ", " + UserData.getString("state")
            } else {
                address = address + UserData.getString("state")
            }
        }
        if (!UserData.getString("country").isNullOrEmpty()) {
            if (!address.isNullOrEmpty()) {
                address = address + ", " + UserData.getString("country")
            } else {
                address = address + UserData.getString("country")
            }
        }
        if(!address.equals("Lives in")) {
            tvlocation.setText(address)
        }

        var occupation = UserData.getString("occupation")
        var food = UserData.getString("whats_your_diet")
        var language = UserData.getString("language")

        tvageheight.setText(age.toString() + " Yrs," + height)
        if (!food.isNullOrEmpty()) {
            tvfood.setText(food.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!language.isNullOrEmpty()) {
            tvlanguage.setText(
                language.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        tvlocation.setText(address)
        if (!maritalstatus.isNullOrEmpty()) {
            tvmaritalstatus.setText(
                maritalstatus.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }
        if (!occupation.isNullOrEmpty()) {
            tvprofession.setText(
                occupation.replace("_", " ").split(" ")?.joinToString(" ") { it.capitalize() })
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_About_Profile_Basic_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_About_Profile_Basic_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun getAge(_year: Int, _month: Int, _day: Int): Int {
        val cal = GregorianCalendar()
        val y: Int
        val m: Int
        val d: Int
        var a: Int
        y = cal[Calendar.YEAR]
        m = cal[Calendar.MONTH]
        d = cal[Calendar.DAY_OF_MONTH]
        cal[_year, _month] = _day
        a = y - cal[Calendar.YEAR]
        if (m < cal[Calendar.MONTH]
            || m == cal[Calendar.MONTH] && d < cal[Calendar.DAY_OF_MONTH]
        ) {
            --a
        }
        require(a >= 0) { "Age < 0" }
        return a
    }
}