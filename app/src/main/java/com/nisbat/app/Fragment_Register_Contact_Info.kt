package com.nisbat.app

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.Constraints
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Register_Contact_Info.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Register_Contact_Info : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var stremail: String = ""
    var strphone: String = ""
    var strwhatsappnumber: String = ""
    var strinstagram: String = ""
    var strfacebooklink: String = ""
    var strlinkedin: String = ""
    var strcountrycode = ""

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var sharedprefencesReginfo: SharedPreferences
    lateinit var jsonUserData: JSONObject
    lateinit var editorReginfo: SharedPreferences.Editor


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as Register_Info).getSharedPreferences("pref_login_user", 0)

        sharedprefencesReginfo =
            (activity as Register_Info).getSharedPreferences("pref_reg_info", 0)

        editorReginfo = sharedprefencesReginfo.edit()

        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_contact_info, container, false)

        val btnnext = view.findViewById<Button>(R.id.btncontactinfonext)
        val btnback = view.findViewById<Button>(R.id.btncontactinfoback)
        val tvcontactnumber = view.findViewById<TextView>(R.id.tvregisterContactinfocontactnumber)
        val tvemail = view.findViewById<TextView>(R.id.tvregistercontactinfoemail)
        val etwhatsappnumber = view.findViewById<EditText>(R.id.etregistercontactinfowhatsappnumber)
        val etinstagramprofile =
            view.findViewById<EditText>(R.id.etregistercontactinfoinstagramusername)
        val etfacebooklink =
            view.findViewById<EditText>(R.id.etregistercontactinfofacebookprofilelink)
        val etlinkedinprofile =
            view.findViewById<EditText>(R.id.etregistercontactinfolinkedinprofile)
        val tvcountrycode = view.findViewById<TextView>(R.id.tvregistercontactinfocountrycode)

        if (!jsonUserData.getString("email").isNullOrEmpty()) {
            tvemail.text = jsonUserData.getString("email")
            stremail = tvemail.text.toString()
        }
        if (!jsonUserData.getString("mobile_no").isNullOrEmpty()) {
            tvcontactnumber.text = jsonUserData.getString("mobile_no")
            strphone = tvcontactnumber.text.toString()
        }
        if (!jsonUserData.getString("phone_code").isNullOrEmpty()) {
            tvcountrycode.text = "+" + jsonUserData.getString("phone_code")
            strcountrycode = (tvcountrycode.text as String).replace("+", "").toString()
        }

        if (!sharedprefencesReginfo.getString("whats_app_no", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("whats_app_no", "")
            etwhatsappnumber.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("instagram_id", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("instagram_id", "")
            etinstagramprofile.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("facebook_id", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("facebook_id", "")
            etfacebooklink.setText(tmpstr)
        }
        if (!sharedprefencesReginfo.getString("linkedin_profile", "").isNullOrEmpty()) {
            var tmpstr =
                sharedprefencesReginfo.getString("linkedin_profile", "")
            etlinkedinprofile.setText(tmpstr)
        }


        btnback.setOnClickListener {
            editorReginfo.putString("email", stremail)
            editorReginfo.putString("mobile_no", strphone)
            editorReginfo.putString("phone_code", strcountrycode)
            editorReginfo.putString("whats_app_no", strwhatsappnumber)
            editorReginfo.putString("instagram_id", strinstagram)
            editorReginfo.putString("facebook_id", strfacebooklink)
            editorReginfo.putString("linkedin_profile", strlinkedin)
            editorReginfo.commit()
            (activity as Register_Info).ShowFamilyInfo()
        }
        btnnext.setOnClickListener {

            strwhatsappnumber = etwhatsappnumber.text.toString()
            strinstagram = etinstagramprofile.text.toString()
            strfacebooklink = etfacebooklink.text.toString()
            strlinkedin = etlinkedinprofile.text.toString()
            strphone=tvcontactnumber.text.toString()
            stremail=tvemail.text.toString()

            if (!stremail.isNullOrEmpty() && !strphone.isNullOrEmpty()) {


                editorReginfo.putString("email", stremail)
                editorReginfo.putString("mobile_no", strphone)
                editorReginfo.putString("phone_code", strcountrycode)
                editorReginfo.putString("whats_app_no", strwhatsappnumber)
                editorReginfo.putString("instagram_id", strinstagram)
                editorReginfo.putString("facebook_id", strfacebooklink)
                editorReginfo.putString("linkedin_profile", strlinkedin)
                editorReginfo.commit()

//            Log.d(Constraints.TAG, "Contact_Info_Pref : " + sharedPreferencesReg_Info.toString())

                (activity as Register_Info).ShowYourInfo()
            }
            else
            {
                if(stremail.isNullOrEmpty())
                {Toast.makeText(
                    (activity as Register_Info),
                    "Email should not be blank",
                    Toast.LENGTH_SHORT
                ).show()}else if (strphone.isNullOrEmpty())
                {Toast.makeText(
                    (activity as Register_Info),
                    "Phone number should not be blank",
                    Toast.LENGTH_SHORT
                ).show()}
            }
        }
        // Inflate the layout for this fragment
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Register_Contact_Info.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Register_Contact_Info().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}