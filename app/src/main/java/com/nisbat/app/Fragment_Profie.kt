package com.nisbat.app

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.fragment_profie.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment_Profie.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment_Profie : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var sharedPrefencesLogin: SharedPreferences
    lateinit var jsonUserData: JSONObject

    lateinit var ivprofiledp: ImageView
    var isActiveNow = false
    lateinit var btnbuypro: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPrefencesLogin =
            (activity as HomeScreen).getSharedPreferences("pref_login_user", 0)
        jsonUserData = JSONObject(sharedPrefencesLogin.getString("User_Data", ""))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_profie, container, false)
        val btnlogout = view.findViewById<Button>(R.id.btnprofilelogout)
        val ibtneditimages = view.findViewById<ImageView>(R.id.ibtnprofileimageedit)
        btnbuypro = view.findViewById<Button>(R.id.btnprofileaccounttypebuypro)
        val framebasicinfo = view.findViewById<LinearLayout>(R.id.frameprofilebasicinfo)
        val framecareerinfo = view.findViewById<LinearLayout>(R.id.frameprofilecareerinfo)
        val framelocationinfo = view.findViewById<LinearLayout>(R.id.frameprofilelocationinfo)
        val framecontactinfo = view.findViewById<LinearLayout>(R.id.frameprofilecontactinfo)
        val frameaboutyourself = view.findViewById<LinearLayout>(R.id.frameprofileaboutyourself)
        val framesettings = view.findViewById<LinearLayout>(R.id.frameprofilesetting)
        val framefamilyinfo = view.findViewById<LinearLayout>(R.id.frameprofilefamilyinfo)
        ivprofiledp = view.findViewById<ImageView>(R.id.ivprofiledp)
        val tvfirstname = view.findViewById<TextView>(R.id.tvprofilefirstname)
        val tvlastname = view.findViewById<TextView>(R.id.tvprofilelastname)
        val frameaccountype = view.findViewById<LinearLayout>(R.id.frameprofileaccounttype)



        if (!jsonUserData.getString("first_name").equals("")) {
            var fn = jsonUserData.getString("first_name")
            tvfirstname.text = fn
        }
        if (!jsonUserData.getString("last_name").equals("")) {
            var ln = jsonUserData.getString("last_name")
            tvlastname.text = ln
        }


        //Button Clicks
        btnlogout.setOnClickListener {

            AlertDialog.Builder(activity as HomeScreen)
//                .setTitle("Confirm?")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton(
                    "Confirm"
                ) { dialog, id ->
                    try {
                        var editor: SharedPreferences.Editor
                        val intent: Intent = Intent(activity as HomeScreen, SignIn::class.java)
                        val sharedPreferencesLogin: SharedPreferences =
                            (activity as HomeScreen).getSharedPreferences("pref_login_user", 0)
                        editor = sharedPreferencesLogin.edit()
                        editor.clear()
                        editor.apply()

                        val sharedPreferencesReginfo: SharedPreferences =
                            (activity as HomeScreen).getSharedPreferences("pref_reg_info", 0)
                        editor = sharedPreferencesReginfo.edit()
                        editor.clear()
                        editor.apply()
                        startActivity(intent)
                        (activity as HomeScreen).finish()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    dialog.cancel()
                }
                .setNegativeButton(
                    "Cancel"
                ) { dialog, id -> dialog.cancel() }.show()


        }
        ibtneditimages.setOnClickListener {
            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false

            (activity as HomeScreen).OpenEditProfile()
        }
        btnbuypro.setOnClickListener {
            if (jsonUserData.has("is_pro") && jsonUserData.getString("is_pro").equals("true")) {
                (activity as HomeScreen).booleanbasicprofile = false
                (activity as HomeScreen).booleancareerprofile = false
                (activity as HomeScreen).booleancontactprofile = false
                (activity as HomeScreen).booleanfamilyprofile = false
                (activity as HomeScreen).booleanlocationprofile = false
                (activity as HomeScreen).booleanyourinfo = false
                (activity as HomeScreen).booleansettings = false
                (activity as HomeScreen).booleanpurchase = false
                (activity as HomeScreen).booleannotification = false
                (activity as HomeScreen).booleanaccounttype = true

                (activity as HomeScreen).OpenEditProfile()
            } else {
                (activity as HomeScreen).booleanbasicprofile = false
                (activity as HomeScreen).booleancareerprofile = false
                (activity as HomeScreen).booleancontactprofile = false
                (activity as HomeScreen).booleanfamilyprofile = false
                (activity as HomeScreen).booleanlocationprofile = false
                (activity as HomeScreen).booleanyourinfo = false
                (activity as HomeScreen).booleansettings = false
                (activity as HomeScreen).booleanpurchase = true
                (activity as HomeScreen).booleannotification = false

                (activity as HomeScreen).OpenEditProfile()
            }
        }
        frameaccountype.setOnClickListener {
            if (jsonUserData.has("is_pro") && jsonUserData.getString("is_pro").equals("true")) {
                (activity as HomeScreen).booleanbasicprofile = false
                (activity as HomeScreen).booleancareerprofile = false
                (activity as HomeScreen).booleancontactprofile = false
                (activity as HomeScreen).booleanfamilyprofile = false
                (activity as HomeScreen).booleanlocationprofile = false
                (activity as HomeScreen).booleanyourinfo = false
                (activity as HomeScreen).booleansettings = false
                (activity as HomeScreen).booleanpurchase = false
                (activity as HomeScreen).booleannotification = false
                (activity as HomeScreen).booleanaccounttype = true

                (activity as HomeScreen).OpenEditProfile()
            } else {
                (activity as HomeScreen).booleanbasicprofile = false
                (activity as HomeScreen).booleancareerprofile = false
                (activity as HomeScreen).booleancontactprofile = false
                (activity as HomeScreen).booleanfamilyprofile = false
                (activity as HomeScreen).booleanlocationprofile = false
                (activity as HomeScreen).booleanyourinfo = false
                (activity as HomeScreen).booleansettings = false
                (activity as HomeScreen).booleanpurchase = true
                (activity as HomeScreen).booleannotification = false

                (activity as HomeScreen).OpenEditProfile()
            }


        }
        framebasicinfo.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = true
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }
        framecareerinfo.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = true
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }
        framelocationinfo.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = true
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }
        framecontactinfo.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = true
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }
        frameaboutyourself.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = true
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }
        framesettings.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = false
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = true
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }
        framefamilyinfo.setOnClickListener {

            (activity as HomeScreen).booleanbasicprofile = false
            (activity as HomeScreen).booleancareerprofile = false
            (activity as HomeScreen).booleancontactprofile = false
            (activity as HomeScreen).booleanfamilyprofile = true
            (activity as HomeScreen).booleanlocationprofile = false
            (activity as HomeScreen).booleanyourinfo = false
            (activity as HomeScreen).booleansettings = false
            (activity as HomeScreen).booleanpurchase = false
            (activity as HomeScreen).booleannotification = false
            (activity as HomeScreen).booleanaccounttype = false

            (activity as HomeScreen).OpenEditProfile()
        }


        // Inflate the layout for this fragment
        return view
    }

    override fun onResume() {
        super.onResume()
        var imageurl = ""
        if (sharedPrefencesLogin.getString("profile_picture", "").isNullOrEmpty()) {
            if (!jsonUserData.getString("profile_picture").isNullOrEmpty()) {
                var editor = sharedPrefencesLogin.edit()
                editor.putString("profile_picture", jsonUserData.getString("profile_picture"))
                editor.commit()
                imageurl = jsonUserData.getString("profile_picture")
            }
        } else {
            imageurl = sharedPrefencesLogin.getString("user_profile_picture", "").toString()
        }
        if (imageurl.isEmpty() || imageurl.equals("abc.jpg")) {
            GetUserphotos()
        } else {
            if (jsonUserData.getString("gender").equals("female")) {
                Picasso.get()
                    .load(imageurl)
                    .placeholder(R.drawable.placeholder_female)
                    .error(R.drawable.placeholder_female)
                    .transform(
                        CircleTransform(
                            (activity as HomeScreen).resources.getDimension(
                                R.dimen._12sdp
                            )
                                .toInt(), 0
                        )
                    )
                    .fit()
                    .centerCrop()
                    .into(ivprofiledp)
            } else {
                Picasso.get()
                    .load(imageurl)
                    .placeholder(R.drawable.placeholder_male)
                    .error(R.drawable.placeholder_male)
                    .transform(
                        CircleTransform(
                            (activity as HomeScreen).resources.getDimension(
                                R.dimen._12sdp
                            )
                                .toInt(), 0
                        )
                    )
                    .fit()
                    .centerCrop()
                    .into(ivprofiledp)
            }

        }

        if (jsonUserData.has("is_pro") && jsonUserData.getString("is_pro").equals("true")) {
            val date = Date()

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            var target_date = ""
            if (jsonUserData.has("plan_end_date") && !jsonUserData.getString("plan_end_date")
                    .isNullOrEmpty()
            ) {
                target_date = jsonUserData.getString("plan_end_date")
            }
            val targetdate = sdf.parse(target_date) as Date
            if (targetdate.compareTo(date) >= 0) {
                isActiveNow = true
            } else {
                isActiveNow = false
            }
            if (isActiveNow) {
                btnbuypro.text = "Pro"
            }
        }
    }

    fun GetUserphotos() {
        val requestParams = RequestParams()
        requestParams.add(
            "its_card_number",
            jsonUserData.getString("its_card_number")
        )

        requestParams.add("status", "1")
        val asyncHttpClient = AsyncHttpClient()

        var token_type = sharedPrefencesLogin.getString("token_type", "")
        var access_token = sharedPrefencesLogin.getString("access_token", "")

        asyncHttpClient.addHeader(
            "Authorization", token_type +
                    " " + access_token
        )
        val handle = asyncHttpClient.post(
            getString(R.string.Api2) + "user/get-user-profile",
            requestParams,
            object : JsonHttpResponseHandler() {
                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    response: JSONObject
                ) {
                    super.onSuccess(statusCode, headers, response)
                    if (statusCode == 200) {

                        var status: Int = response.getString("status").toInt()
                        if (status == 200) {
                            var jsonObjuserdata: JSONObject = response.getJSONObject("data")
                            var jsonArrayPhotos = JSONArray()
                            if (jsonObjuserdata.getString("profilePicture").isNullOrEmpty()) {
                                var editor: SharedPreferences.Editor = sharedPrefencesLogin.edit()
                                editor.putString("profile_picture", "abc.jpg")
                                editor.commit()
                                if (jsonUserData.getString("gender").equals("male")) {
                                    ivprofiledp.setBackgroundResource(R.drawable.placeholder_female)

//                                    Picasso.get()
//                                        .load(R.drawable.placeholder_female)
//                                        .placeholder(R.drawable.placeholder_female)
//                                        .error(R.drawable.placeholder_female)
//                                        .transform(
//                                            CircleTransform(
//                                                (activity as HomeScreen).resources.getDimension(
//                                                    R.dimen._20sdp
//                                                )
//                                                    .toInt(), 0
//                                            )
//                                        )
//                                        .fit()
//                                        .centerCrop()
//                                        .into(ivprofiledp)
                                } else {
                                    ivprofiledp.setBackgroundResource(R.drawable.placeholder_male)
//                                    Picasso.get()
//                                        .load(R.drawable.placeholder_male)
//                                        .placeholder(R.drawable.placeholder_male)
//                                        .error(R.drawable.placeholder_male)
//                                        .transform(
//                                            CircleTransform(
//                                                (activity as HomeScreen).resources.getDimension(
//                                                    R.dimen._20sdp
//                                                )
//                                                    .toInt(), 0
//                                            )
//                                        )
//                                        .fit()
//                                        .centerCrop()
//                                        .into(ivprofiledp)
                                }


                            } else {
                                jsonArrayPhotos =
                                    JSONArray(jsonObjuserdata.getString("profilePicture"))
                                var jsonObjTemp = jsonArrayPhotos.getJSONObject(0)
                                var imageurl = jsonObjTemp.getString("profile_picture")
                                imageurl=imageurl.replace("http:","https:")
                                var editor: SharedPreferences.Editor = sharedPrefencesLogin.edit()
                                editor.putString("profile_picture", imageurl)
                                editor.commit()
                                if (jsonUserData.getString("gender").equals("male")) {
                                    Picasso.get()
                                        .load(imageurl)
                                        .placeholder(R.drawable.placeholder_male)
                                        .error(R.drawable.placeholder_male)
                                        .transform(
                                            CircleTransform(
                                                (activity as HomeScreen).resources.getDimension(
                                                    R.dimen._20sdp
                                                )
                                                    .toInt(), 0
                                            )
                                        )
                                        .fit()
                                        .centerCrop()
                                        .into(ivprofiledp)
                                } else {
                                    Picasso.get()
                                        .load(imageurl)
                                        .placeholder(R.drawable.placeholder_female)
                                        .error(R.drawable.placeholder_female)
                                        .transform(
                                            CircleTransform(
                                                (activity as HomeScreen).resources.getDimension(
                                                    R.dimen._20sdp
                                                )
                                                    .toInt(), 0
                                            )
                                        )
                                        .fit()
                                        .centerCrop()
                                        .into(ivprofiledp)
                                }
                            }

                        } else if (status == 401) {

                            Toast.makeText(
                                (activity as HomeScreen),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {

                            Toast.makeText(
                                (activity as HomeScreen),
                                response.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    throwable: Throwable,
                    errorResponse: JSONObject
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    throwable: Throwable?,
                    errorResponse: JSONArray?
                ) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseString: String?,
                    throwable: Throwable?
                ) {
                    super.onFailure(statusCode, headers, responseString, throwable)
                    Toast.makeText(
                        (activity as HomeScreen),
                        "Internal Server Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment_Profie.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment_Profie().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
